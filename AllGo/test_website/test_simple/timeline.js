
//data
/*	
		var items = [	{"lane": 4, "id":"1", "start": 6},
            			{"lane": 0, "id":"2", "start": 5 },
						{"lane": 1, "id":"5", "start": 3},
						{'lane': 2, "id":"2", 'start': 4},
						{'lane': 2, "id":"7", 'start': '4'},
						{'lane': 2, "id":"2", 'start': '4'}
					];
*/

var marginTop = 20,
	marginRight = 25,
	marginBottom = 15,
	marginLeft = 120,
	padding = 2,
	laneHeight = 20,
	width = 630 - marginRight - marginLeft;


function display() {

    d3.select(".chart").remove(); 
    
    var sequences = document.getElementById("values").value.split("\n");
    var items =[];
    var maxitem=0;
    var sl = sequences.length;
    for(i=0;i<sl;i++) {
        var itemset = sequences[i].split(" ").map(function(d){return +d});
        
        for(j=3;j<(3+itemset[2]);j++) {
            items.push( {'lane': itemset[0], 'id': itemset[j],'start': itemset[1]} );
            //console.log( {'lane': itemset[0], 'id': itemset[j],'start': itemset[1]} );
            if( maxitem<itemset[j] ) maxitem=itemset[j];
        }
    }

    var laneLength = d3.max( items, function(d) {
                    return d.lane;
                }) + 1;
                
    var lanesDuration = d3.max( items, function(d) {
                    return d.start;
                }) + 1;

    var lanesObjects = [];
    for (i=0; i<laneLength; i++) {
        lanesObjects.push({'id': i});
    } 

	var mainHeight = laneHeight*laneLength;
	var height = mainHeight + marginTop + marginBottom ;

    // Scales
    var x1 = d3.scaleLinear()
			    .domain([0, lanesDuration])
			    .range([0, width]);

    var y1 = d3.scaleLinear()
			    .domain([0, laneLength])
			    .range([padding, mainHeight]);

    var radius = laneHeight/4;

    var chart = d3.select(".infochart")
			        .append("svg")
			        .attr("width", width + marginRight + marginLeft )
			        .attr("height", height + marginTop + marginBottom )
			        .attr("class", "chart");

    chart.append("defs").append("clipPath")
	    .attr("id", "clip")
	    .append("rect")
	    .attr("width", width)
	    .attr("height" , mainHeight + 2*marginBottom);

    var main = chart.append("g")
		        .attr("transform", "translate(" + marginLeft + "," + marginTop + ")" )
		        .attr("width", width)
		        .attr("height", mainHeight)
		        .attr("class", "main");

    // main lanes and text
    main.append("g").selectAll("line")
	    .data(lanesObjects)
	    .enter().append("line")
	    .attr("x1", 0)
	    .attr("y1", function(d,i) { return y1(i); })
	    .attr("x2", width)
	    .attr("y2", function(d,i) { return y1(i); } )
	    .attr("class", "mainLines");

    // affichage des ID de sequences
    main.append("g").selectAll(".laneText")
	    .data(lanesObjects)
	    .enter()
	    .append("text")
	    .text(function(d, i) { return "S"+i;} )
	    .attr("x", -marginRight)
	    .attr("y", function(d, i) { return y1(i + .5); } )
	    .attr("dy", ".5ex")
	    .attr("text-anchor", "end")
	    .attr("class", "laneText");

    var x1DateAxis = d3.axisBottom(x1)
                        .ticks(lanesDuration);

    main.append('g')
		    .attr('transform', 'translate(0,' + mainHeight  + ')')
		    .attr('class', 'axis');	

    var circleHolder = main.append("g")
					.attr("clip-path", "url(#clip)");	

    var colorScale = d3.scaleSequential()
      .domain([0,255])
      .interpolator(d3.interpolateRainbow);

    main.select('.axis').call(x1DateAxis);
  
	/* Build the circles for the main box */
	var circles = circleHolder.selectAll("circle")
	    .data(items)
	    .enter()
	    .append("circle")
	    .attr("fill", function(d) { console.log((255*d.id)/maxitem); return colorScale( (255*d.id)/maxitem); } )
	    .attr("cx", function(d) { return x1(d.start); } )
	    .attr("cy", function(d) { return y1(d.lane) + radius + laneHeight/4; } )
	    //.attr("cy", function(d) { return y1(d.lane) + radius+ Math.random()*laneHeight/3; } ) //add jitter
	    .attr("r", function(d) { return radius } );
}
