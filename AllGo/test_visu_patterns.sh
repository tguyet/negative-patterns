var data =  {"patterns":[
{"pattern":[{"ids":[5],"neg":false},{"ids":[1,2],"neg":true},{"ids":[4],"neg":false}]},
{"pattern":[{"ids":[5],"neg":false},{"ids":[1,3],"neg":true},{"ids":[4],"neg":false}]},
{"pattern":[{"ids":[5],"neg":false},{"ids":[1,4],"neg":true},{"ids":[4],"neg":false}]},
{"pattern":[{"ids":[5],"neg":false},{"ids":[1,5],"neg":true},{"ids":[4],"neg":false}]},
{"pattern":[{"ids":[5],"neg":false},{"ids":[1],"neg":true},{"ids":[4],"neg":false}]},
{"pattern":[{"ids":[5],"neg":false},{"ids":[2,3],"neg":true},{"ids":[4],"neg":false}]},
{"pattern":[{"ids":[5],"neg":false},{"ids":[2,4],"neg":true},{"ids":[4],"neg":false}]},
{"pattern":[{"ids":[5],"neg":false},{"ids":[2,5],"neg":true},{"ids":[4],"neg":false}]},
{"pattern":[{"ids":[5],"neg":false},{"ids":[2],"neg":true},{"ids":[4],"neg":false}]},
{"pattern":[{"ids":[5],"neg":false},{"ids":[3,4],"neg":true},{"ids":[4],"neg":false}]},
{"pattern":[{"ids":[5],"neg":false},{"ids":[3,5],"neg":true},{"ids":[4],"neg":false}]},
{"pattern":[{"ids":[5],"neg":false},{"ids":[3],"neg":true},{"ids":[4],"neg":false}]},
{"pattern":[{"ids":[5],"neg":false},{"ids":[4,5],"neg":true},{"ids":[4],"neg":false}]},
{"pattern":[{"ids":[5],"neg":false},{"ids":[4],"neg":true},{"ids":[4],"neg":false}]},
{"pattern":[{"ids":[5],"neg":false},{"ids":[5],"neg":true},{"ids":[4],"neg":false}]},
{"pattern":[{"ids":[5],"neg":false},{"ids":[4],"neg":false}]},
{"pattern":[{"ids":[5],"neg":false}]},
{"pattern":[]}
]};

 var c10 = d3.scale.category10();
 var svg = d3.select("body")
   .append("svg")
   .attr("width", 1200)
   .attr("height", 800);

 var drag = d3.behavior.drag()
   .on("drag", function(d, i) {
     d.x += d3.event.dx
     d.y += d3.event.dy
     d3.select(this).attr("cx", d.x).attr("cy", d.y);
     links.each(function(l, li) {
       if (l.source == i) {
         d3.select(this).attr("x1", d.x).attr("y1", d.y);
       } else if (l.target == i) {
         d3.select(this).attr("x2", d.x).attr("y2", d.y);
       }
     });
   });

 var links = svg.selectAll("link")
   .data(data.links)
   .enter()
   .append("line")
   .attr("class", "link")
   .attr("x1", function(l) {
     var sourceNode = data.nodes.filter(function(d, i) {
       return i == l.source
     })[0];
     d3.select(this).attr("y1", sourceNode.y);
     return sourceNode.x
   })
   .attr("x2", function(l) {
     var targetNode = data.nodes.filter(function(d, i) {
       return i == l.target
     })[0];
     d3.select(this).attr("y2", targetNode.y);
     return targetNode.x
   })
   .attr("fill", "none")
   .attr("stroke", "white");

 var nodes = svg.selectAll("node")
   .data(data.nodes)
   .enter()
   .append("circle")
   .attr("class", "node")
   .attr("cx", function(d) {
     return d.x
   })
   .attr("cy", function(d) {
     return d.y
   })
   .attr("r", 15)
   .attr("fill", function(d, i) {
     return c10(i);
   })
   .call(drag);
