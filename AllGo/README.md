## TENSE patterns -- TEmporal Negative Sequential patterns

TENSE is an algorithm to extract negative sequential patterns from dataset of sequences. A negative sequential pattern is a pattern negative itemsets, ie specifying absence of event.
Negative temporal patterns add numerical temporal constraints about durations between successive events.

The TENSE algorithm combines the mining of negative temporal patterns (NegPSpan) with a density clustering algorithm (MAFIA algorithm). It enables to add maxgap constraints.

Our software enables to evaluate alternative configurations of TENSE pattern extraction:

* possibility to use eNSP instead of NegPSpan to extract the negative patterns
* different configurations of NegPSpan algorithms (different semantic of negative patterns)

### Input format

The algorithm process dataset of sequences. Possible file extensions are `.dat` or `.txt`.

The input format of a dataset is is the IBM format. Each line of the file represents an itemset. The line give, in that order, the sequence id, the itemset timestamp, the size of the itemset and the set of the items.

The following example illustrates the encoding of the dataset (see *embeddings* example):

* 1 3 2 5 4
* 1 (2 3) 5 4
* 1 2 5 4
* 1 5 4

    0 1 1 1  
    0 2 1 3  
    0 3 1 2  
    0 4 1 5  
    0 5 1 4  
    1 1 1 1  
    1 2 2 2 3   
    1 3 1 5  
    1 4 1 4  
    2 1 1 1  
    2 2 1 2  
    2 3 1 5  
    2 4 1 4  
    3 1 1 1  
    3 2 1 5  
    3 3 1 4

### Parameters

* `cneg`: use NegPSpan (eNSP otherwise)
* `f`: minimum support (number of transactions), for eNSP it is the minimum support for positive patterns
* `n`: minimum support of negative patterns for eNSP algorithms
* `mg`: max gap constraint
* MAFIA parameters
  * `MW`: maximum number of windows
  * `mW`: minimum number of windows 
  * `alpha`: density threshold for dense unit
  * `beta`: merging threshold

### Limitations
This online version prevents from too long processes or too heavy memory requirements (for fair use of our servers). The following additional setting can not by modified:

* maximum pattern length is 4
* maximum size of negative itemsets is 2


### References
<dl>
    <dt>PrefixSpan: Mining Sequential Patterns Efficiently by Prefix-Projected Pattern Growth</dt>
    <dd>Jian Pei, Jiawei Han, Behzad Mortazavi-asl, Helen Pinto, Qiming Chen, Umeshwar Dayal and Mei-chun Hsu IEEE Computer Society, 2001, pages 215</dd>
    <dt>e-NSP: Efficient negative sequential pattern mining</dt>
    <dd>Longbing Cao, Xiangjun Dong, Zhigang Zheng, Artificial Intelligence, 2016, 235:156–182</dd>
    <dt>Mining non-redundant time-gap sequential patterns</dt>
    <dd>Yen, S.-J. and Y.-S. Lee, Applied intelligence 39(4), 2013, 727–738</dd>
</dl>

