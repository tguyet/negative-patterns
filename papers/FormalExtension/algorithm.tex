%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Algorithm \NegPSpan} \label{sec:censp}

In this section, we introduce algorithm \NegPSpan~ for mining NSPs from a sequence database under maxgap and maxspan constraints and under a weak absence semantics with $\nsubseteq\myeq\not\sqsubseteq$ for itemset inclusion. As stated in proposition \ref{prop:sqsubset_eqembeddings}, no matter the embedding strategy, they are equivalent under strict itemset inclusion. 
Let $\mathcal{L}^-$ denote \mco{the set of itemsets that can be built from frequent items}{seem not clear: to be reformulated}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{algorithm}[bp]
\footnotesize
%\small

\LinesNumbered

\SetKwInOut{Input}{input}
\SetKwData{break}{break}
\SetKwComment{Comment}{//}{}

\SetKwFunction{FRec}{\NegPSpan}
\SetKwFunction{match}{Match}
\SetKwFunction{output}{OutputPattern}
\SetKwProg{Fn}{Function}{:}{}
\SetKwFunction{PositiveComposition}{PositiveComposition}
\SetKwFunction{PositiveSequence}{PositiveSequence}
\SetKwFunction{NegativeSequence}{NegativeExtension}
\SetKwFunction{NegativeComposition}{NegativeComposition}

\Input{$\mathcal{S}$: set of sequences, $p$: current pattern, $\sigma$: minimum support threshold,  $occs$: list of occurrences, $\mathcal{I}^f$: set of frequent items, $\theta$: maxgap, $\tau$: maxspan}
	\BlankLine
    \Fn{\FRec{$\mathcal{S}$, $\sigma$, $p$, $occs$, $\mathcal{I}^f$, $\theta$, $\tau$}}{
		\Comment{Support evaluation of pattern $p$}
        \If{$|occs|\geq \sigma$} {
        	\output{$p$, $occs$}\;
        }\Else {
        	\KwRet\;
        }
        
        \Comment{Positive itemset composition}
        \PositiveComposition{$\mathcal{S}$, $\sigma$, $p$, $occs$, $\mathcal{I}^f$, $\theta$, $\tau$}\;
        
        \Comment{Positive sequential extension}
        \PositiveSequence{$\mathcal{S}$, $\sigma$, $p$, $occs$, $\mathcal{I}^f$, $\theta$, $\tau$}\;
        
        \If{$|p| \geq 2$} {
        	\Comment{Negative sequential extension}
			\NegativeSequence{$\mathcal{S}$, $\sigma$, $p$, $occs$, $\mathcal{I}^f$, $\theta$, $\tau$}\;
        }
  	}  	
\caption{\NegPSpan: recursive function for negative sequential pattern extraction}
\label{algo:CeNSP-Rec}
\end{algorithm}

\subsection{Main algorithm}
\NegPSpan~ is based on algorithm PrefixSpan \cite{pei2004mining:prefixspan} which implements a depth first search and uses the principle of database projection to reduce the number of sequence scans. 
\NegPSpan~ adapts the pseudo-projection principle of PrefixSpan which uses a projection pointer to avoid copying the data.
For \NegPSpan, a projection pointer of some pattern $p$ is a triple $\langle sid, ppred, pos \rangle$ where 
$sid$ is a sequence identifier in the database, 
%
$pos$ is the position in sequence $sid$ that matches the last itemset of the pattern (necessarily positive)
%
and $ppred$ is the position of the previous positive pattern.

Algorithm \ref{algo:CeNSP-Rec} details the main recursive function of \NegPSpan~ for extending a current pattern $p$. The principle of this function is similar to PrefixSpan. Every pattern $p$ is associated with a pseudo-projected database represented by both the original set of sequences $\mathcal{S}$ and a set of projection pointers $occs$.
First, the function evaluates the size of $occs$ to determine whether pattern $p$ is frequent or not. If so, it is outputted, otherwise, the recursion is stopped because no larger patterns are possible (anti-monotonicity property).\\
Then, the function tries three types of pattern extensions:
\begin{itemize}
\item the positive sequence composition consists in adding  one item to the last itemset of $p$,
\item the positive sequence extension consists in adding a new positive singleton itemset at the end of  $p$,
\item the negative sequence extension consists in inserting a negative itemset between the positive penultimate itemset of $p$ and the last positive itemset of $p$. According to the chosen NSP syntactic restrictions, this extension is possible only when the pattern length is greater than or equal to 2.
\end{itemize}

The negative pattern extension is specific to our algorithm and is detailed in the next section.
The first two extensions are identical to PrefixSpan pattern extensions, including their gap constraints management, \ie maxgap and maxspan constraints between positive patterns.
For the sake of space limitation, positive extensions are not detailed in the sequel.

The proposed algorithm is correct and complete. The proposed algorithm is also consistent with order $\lhd$ on NSP and, according to proposition \ref{prop:antimonotonic}, NSP support is anti-monotonic.
More specifically, the negative composition by an item is anti-monotonic under the strict-embedding semantics.\footnote{A similar algorithm extracts NSPs with a non occurrence based on operator $\not\preceq$. $\blacktriangleleft$ order forbids the incremental extension of negative itemsets. Thus, all candidate itemsets, \ie $\mathcal{L}^-$, are computed and evaluated without recursions.} 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Extension of patterns with negated itemsets}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{algorithm}[tbp]
\footnotesize
%\small

\LinesNumbered

\SetKwInOut{Input}{input}
\SetKwData{break}{break}
\SetKwData{continue}{continue}
\SetKwComment{Comment}{//}{}

\SetKwFunction{FRec}{\NegPSpan}
\SetKwFunction{Fonction}{NegativeExtension}
\SetKwFunction{match}{Match}
\SetKwProg{Fn}{Function}{:}{}

\Input{$\mathcal{S}$: set of sequences, $p$: current pattern, $\sigma$: minimum support threshold,  $occs$: list of occurrences, $\mathcal{I}^f$: set of frequent items, $\theta$: maxgap, $\tau$: maxspan}
	\BlankLine
    \Fn{\Fonction{$\mathcal{S}$, $\sigma$, $p$, $occs$, $\mathcal{I}^f$, $\theta$, $\tau$}}{
        	
        \For{$it \in \mathcal{I}^f$}{
        	
        	\If{$p[-2]$ is pos} {
        		\Comment{Insert the negative item at the penultimate position}
        		$p.insert(\neg it)$\; 
        	} \Else {
        		\If{$it>p[-2].back()$} {
        			\Comment{Insert an item to the penultimate (negative) itemset}
        			$p[-2].insert(\neg it)$\;
        		} \Else {
        			\continue\;
        		}
        	}
        		$newoccs \gets \emptyset$\;
        		\For{$occ \in occs$}{
        			$found \gets false$\;
   		     		\For{$ sp = [occ.pred+1,occ.pos-1]$} {
      	  				\If{$it \in s_{occ.sid}[sp]$}{
        					$found \gets true$\;
        					\break\;
        				}
        			}
        			\If{!$found$} {
        				$newoccs \gets newoccs \cup \{occ\}$\;
        			} \Else {
        				\Comment{Look for an alternative occurrence}
        				$newoccs \gets newoccs \cup $ \match{$s_{sid}$, $p$, $\theta$, $\tau$}\;
        			}
        		}
        		
        		\FRec{$\mathcal{D}$, $\sigma$, $p$, $newoccs$, $\mathcal{I}^f$}\;
        		$p[-2].pop()$\;
       	 	}
    }
\caption{\NegPSpan: negative extensions}
\label{algo:NegExt}
\end{algorithm}

Algorithm \ref{algo:NegExt} extends the current pattern $p$ with negative items. It generates new candidates by inserting an item $it \in \mathcal{I}^f$, the set of frequent items. Let $p[-2]$ and $p[-1]$ denote respectively the penultimate itemset and the last itemset of $p$. If $p[-2]$ is positive, then a new negated itemset is inserted between $p[-2]$ and $p[-1]$. Otherwise, if $p[-2]$ is negative, item $it$ is added to $p[-2]$. To prevent redondant enumeration of negative itemsets, only items $it$ (lexicographically) greater than the last item of $p[-2]$ can be added.

Then, lines 10 to 20, evaluate the candidate by computing the pseudo-projection of the current database. According to the selected semantics associated with $\not\sqsubseteq$, \ie total non inclusion (see Definition \ref{def:NSP_embedding}), it is sufficient to check the absence of $it$ in the subsequence included between the occurrences of positive itemsets surounding $it$. 
To achieve this, the algorithm checks the sequence positions in the interval $[occ.ppred+1, occ.pos-1]$.
If $it$ does not occur in itemsets from this interval, then the extended pattern occurs in the sequence $occ.sid$.
Otherwise, to ensure the completeness of the algorithm, another occurrence of the pattern has to be searched in the sequence (\cf \FuncSty{Match} function that takes into account gap constraints).

For example, the first occurrence of pattern $\seq{p}=\langle abc\rangle$ in sequence $\langle abecabc \rangle$ is $occ_p=\langle sid,2,4\rangle$. Let's now consider $\seq{p}'=\langle ab\neg ec\rangle$, a negative extension of $p$. The extension of the projection-pointer $occ_p$ does not satisfy the absence of $e$. So a new occurrence of $p$ has to be searched for. $\langle sid,6,7\rangle$, the next occurrence of $\seq{p}$, satisfies the negative constraint.
Then, \NegPSpan~ is called recursively for extending the new current pattern $\langle ab\neg ec\rangle$.
