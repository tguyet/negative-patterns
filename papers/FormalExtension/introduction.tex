
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
In many application domains such as diagnosis or marketing, decision makers show a  strong interest for  rules that associates specific events (a context) to undesirable events to which they are correlated or that are frequently triggered in such a context. Sequential pattern mining algorithms can extract such hidden rules from execution traces or transactions. in the classical setting, sequential patterns contain only positive events, \ie really observed events.
However, the absence of a specific action or event can often better explain the occurrence of an undesirable situation \cite{Cao2015}. 
For example in diagnosis, if some maintenance operations have not been performed, \eg damaged parts have not been replaced, then a fault will likely occur in a short delay while if these operations were performed in time the fault would not occur. In marketing, if some market-place customer has not received special offers or coupons for a long time then she/he has a high probability of churning while if she/he were provided such special offers she/he should remain loyal to her/his market-place.
%
In these two cases, mining specific events, some present and some absent, to discover under which context some undesirable situation occurs or not may provide interesting so-called \emph{actionable} information for determining which action should be performed to avoid the undesirable situation, \ie fault in diagnosis, churn in marketing.


We aim at discovering sequential patterns that take into account the absence of some events called \emph{negative events} \cite{Cao2015}. 
Moreover, we want to take into account some aspect of the temporal dimension as well, maximal pattern span or maximal gap between the occurrences of pattern events. 
For example, suppose that from a sequence dataset, we want to mine a sequential pattern $\seq{p} = \langle a\  b\rangle$ with the additional
\emph{negative} constraint telling that the event $c$ should not appear between events $a$ and $b$ in $\seq{p}$.
The corresponding negative pattern is represented as $\seq{p} = \langle a \  \neg c \  b\rangle$, where the logical sign $\neg$ denotes an absent event or set of events. 
Once the general idea of introducing negative statements in a pattern has been stated, the syntax and  semantics of such negative patterns should be clearly formulated since they have a strong impact both on algorithms outcome and their computational efficiency.
% 
As we will see, the few algorithms from literature do not use the same syntactical constraints and rely on very different semantics principles (see Section \ref{sec:negpatterns:relwork}).  
%
More precisely, the efficiency of eNSP \cite{cao2016nsp}, the state-of-the-art algorithm for NSP mining, comes from a negation semantics that enables efficient operations on the sets of supported sequences. 
The two computational limits of eNSP are memory requirements and the impossibility for eNSP to handle embedding constraints such as the classical \emph{maxgap} and \emph{maxspan} constraints. 
%
When mining relatively long sequences (above 20 itemsets), such constraints appear semantically sound to consider short pattern occurrences where events are not too distant. In addition, such constraints can efficiently prune the occurrence search space. 

This article provides two main contributions:
\begin{itemize}
\item we clarify the syntactic definition of negative sequential patterns and we provide different negation semantics with their properties.
\item we propose \NegPSpan, an algorithm inspired by algorithm PrefixSpan to extract negative sequential patterns with maxgap constraints. 
\end{itemize}
 
Intensive experiments compare, on synthetic and real datasets, the performance of \NegPSpan\ and eNSP as well as the pattern sets extracted by each of them.
We show that algorithm \NegPSpan\ is more time-efficient than eNSP for mining long sequences thanks to the maxgap constraint and that its memory requirement is several orders of magnitude lower, enabling to process much larger datasets.
In addition, we highlight that eNSP misses interesting patterns on real datasets due to semantic restrictions.

%The paper is organized as follows.
%Section 2 presents some related work in NSP mining. Section 3 introduces a conceptual framework for formulating the syntax and semantics of negative sequential pattern mining methods. Section 4 describes \NegPSpan\ our proposed algorithm. Section 5 presents extensive experiments on synthetic and real datasets that compare the performance of  \NegPSpan\ with eNSP. The last section gives a conclusion and some perspectives.