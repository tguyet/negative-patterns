%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Related work} \label{sec:negpatterns:relwork}
Kamepalli et \al provide a survey of the approaches proposed for mining negative patterns \cite{Kamepalli:2014:FrequentNS}.
The three most significant algorithms appear to be PNSP, Neg-GSP and eNSP.
We briefly review each of them in the following paragraphs.

PNSP (Positive and Negative Sequential Patterns mining) \cite{hsueh:2008:PNSP} is the first algorithm proposed for mining full negative sequential patterns where negative itemsets are not only located at the end of the pattern.
PNSP extends algorithm GSP \cite{srikant1996:GSP} to cope with mining negative sequential patterns.
PNSP consists of three steps: i) mine frequent positive sequential patterns, by using algorithm GSP, ii) preselect negative sequential itemsets --- for PNSP, negative itemsets must not be too infrequent (should have a support less than a threshold \textit{miss\_freq}) --- iii) generate candidate negative sequences levelwise and scan the sequence dataset again to compute the support of these candidates and prune the search when the candidate is infrequent.
This algorithm is incomplete: the second parameter reduces the set of potential negative itemsets.
Moreover, the pruning strategy of PNSP is not correct \cite{zheng:2009:negative} and PNSP misses potentially frequent negative patterns.

Zheng et \al \cite{zheng:2009:negative} also proposed a negative version of algorithm  GSP, called Neg-GSP, to extract negative sequential patterns. They showed that traditional Apriori-based negative pattern mining algorithms relying on support anti-monotonicity have two main problems. The first one is that the Apriori principle does not apply to negative sequential patterns.
They gave an example of sequence that is frequent even if one of its sub-sequence is not frequent.
The second problem has to do with the efficiency and the effectiveness of finding frequent patterns due to a vast candidate space. 
Their solution was to prune the search space using the support anti-monotonicity over positive parts.
This pruning strategy is correct but is not really efficient considering the huge number of remaining candidates whose support has to be evaluated.
We will see in Section \ref{sec:negpatterns:semantics} that anti-monotonicity can be defined considering an order relation based on common prefixes.

eNSP (efficient NSP) has been recently proposed by Cao et \al cite{cao2016nsp}. It identifies NSPs by computing only frequent positive sequential patterns and deducing negative sequential patterns from positive patterns. 
Precisely, Cao et \al showed that the support of some negative pattern can be 
computed by arithmetic operations on the support of its positive sub-patterns, 
thus avoiding additional sequence database scans to compute the support of negative patterns. However, this necessitates to store all the (positive) sequential patterns with their set of covered sequences (tid-lists) which may be impossible in case of big dense datasets and low minimal support thresholds.
This approach makes the algorithm more efficient but it hides some restrictive constraints on the extracted patterns. First, a frequent negative pattern whose so-called positive partner (the pattern where all negative events have been switched to positive) is not frequent will not be extracted.
Second, every occurrence of a negative pattern in a sequence should satisfy absence constraints. We call this \emph{strong absence semantics} (see Section \ref{sec:negpatterns:semantics}).
These features lead eNSP to extract less patterns than previous approaches.
In some practical applications, eNSP may miss potentially interesting negative patterns from the dataset.\\
The first constraint has been partly tackled by Dong et \al with algorithm eNSPFI, an extension of eNSP which mines NSPs from frequent and some infrequent positive sequential patterns from the negative border \cite{Gong2017eNSPFI}.
E-msNSP \cite{xu2017msnsp} is another extension of eNSP  which uses multiple minimum supports:
an NSP is frequent if its support is greater than a local minimal support threshold computed from the content of the pattern and not a global threshold as in classical approaches.
A threshold is associated with each item, and the minimal support of a pattern is defined from the most constrained item it contains. Such kind of adaptive support prevents from extracting some useless patterns still keeping the pattern support anti-monotonic. The same authors also proposed high utility negative sequential patterns based on the same principles \cite{xu2017HighUtilityNSP}. It is worth noting that this algorithm relies basically on the same principle as eNSP and so, present the same drawbacks, heavy memory requirements, strong absence semantics for negation. 


\begin{table*}[tb]
\small
\caption{Comparison of negative pattern mining proposals. Optional constraints are specified in Italic.}
\label{NSP-comparison}
\begin{tabularx}{\textwidth}{|l|Y|Y|Y|Y|} % <-- choose width of 'P' column suitably
\hline & \textbf{PNSP} \cite{hsueh:2008:PNSP} & \textbf{NegGSP} \cite{zheng:2009:negative} & \textbf{eNSP} \cite{cao2016nsp} & \textbf{\NegPSpan} \\ 
\hline 
\textbf{negative elements} &  itemsets &  items\textit{?} &  itemsets &  itemsets \\ 
\hline 
\textbf{itemset non inclusion} & strict & strict\textit{?} & strict & strict/\textit{soft} \\ 
\hline 
\textbf{itemset absence} & weak & weak & strong & weak \\ 
\hline 
\textbf{constraints on negative itemsets} & not too infrequent ($supp \leqslant less\_freq $) & frequent items & positive partner is frequent & frequent items, \textit{bounded size} \\ 
\hline 
\textbf{global constraints on patterns} & positive part is frequent & positive part is frequent & positive part is frequent & positive part is frequent, \textit{maxspan}, \textit{maxgap}\\ 
\hline 
\end{tabularx}
\end{table*}