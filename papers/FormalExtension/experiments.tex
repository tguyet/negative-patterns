%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Experiments}\label{sec:expes}
This section presents experiments on synthetic and real data. Experiments on synthetic data aims at exploring and comparing \NegPSpan\ and eNSP for negative sequential pattern mining.
The other experiments were conducted on medical care pathways and illustrates results for negative patterns.
\NegPSpan\ and eNSP have been implemented in C++. 
We pay attention on the most significant results. More detailed results can be found in a compagnon website.\footnote{Code, data generator and synthetic benchmark datasets can be downloaded here: \url{http://people.irisa.fr/Thomas.Guyet/negativepatterns/}
%Code execution is available online, without installation, via the AllGo platform.
}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Benchmark}
This section presents experiments on synthetically generated data. The principle of our sequence generator is the following: generate random negative patterns and hide or not some of their occurrences inside randomly generated sequences.
The main parameters are the total number of sequences ($n$, default value is $n = 500$), the mean length of sequences ($l = 20$), the number of different items ($d = 20$), the total number of patterns to hide ($3$), their mean length ($4$) and the minimum occurrence frequency of patterns in the dataset ($10\%$). 

Generated sequences are sequences of items (not itemsets). 
For such kind of sequences, patterns extracted by eNSP hold only items because positive partners have to be frequent. For a fair evaluation and preventing \NegPSpan\ from generating more patterns, we restricted $\mathcal{L}^-$ to the set of frequent items.\co{R1: does that mean restricting the algorithm to singleton itemsets more generally, since there is nothing else?}
For both approaches, we limit the pattern length to 5 items.

Figure \ref{fig:mg_cmp} illustrates the computation time and number of patterns extracted by eNSP and \NegPSpan\ on sequences of length 20 and 30, under three minimal thresholds ($\sigma=10\%$, $15\%$ and $20\%$) and with different values for the maxgap constraint ($\tau=4$, $7$, $10$ and $\infty$).
For eNSP, the minimal support of positive partners is set to 70\% of the minimal threshold $f$. \co{R1:does this parameter match the hidden patterns? (i.e. allows to find them)\\TG: GOOD POINT TO DISCUSS!}
Each boxplot has been obtained with a 20 different sequence datasets.
Each run has a timeout of 5 minutes. 

\begin{figure*}[t]
\centering
\includegraphics[width=.44\textwidth]{nbpatVSmg_bp} \hspace{0.1cm}
\includegraphics[width=.44\textwidth]{timeVSmg_bp} \hspace{0.1cm}
\includegraphics[width=.06\textwidth]{legend}
\caption{Comparison of number of patterns (left) and computing time (right) between eNSP and \NegPSpan, with different values for maxgap ($\tau$). Top (resp. bottom) figures correspond to database with mean sequence length equal to $20$ (resp. $30$). Boxplot colors correspond to different values of $\sigma$ ($10\%$, $15\%$ and $20\%$).}
\label{fig:mg_cmp}
\end{figure*}
\co{R1: it seems the boxplot for l=20 are not aligned to match the x ticks and l=30 corresponding plots
	* -> because of some missing results, the number of boxplot changes !! This must be the problem (and color not visible on printed papers)
}

The main conclusion from Figure \ref{fig:mg_cmp} is that \NegPSpan\ is more efficient than eNSP when maxgap constraints are used.
As expected, eNSP is more efficient than \NegPSpan\ without any maxgap constraint.
This is mainly due to the number of extracted patterns. \NegPSpan\ extracts significantly more patterns than eNSP because of different choices for the semantics of NSPs.
First, eNSP uses a stronger negation semantics. It can \mco{easily be proven}{formalize constraints a bit more and proove it ;) + comparison with ensp: not maxgap, but maxduration.} that, without maxgap constraints, the set of patterns extracted by \NegPSpan\ is a superset of those extracted by eNSP.\footnote{Proof is given in an extended version of the paper available online.} 
{\color{blue}Second, eNSP potentially misses a lot of interesting patterns due to the minimal support imposed on the positive partners\footnote{We recall that the positive partner of a negative pattern is the sequential (positive) pattern in which all negative itemsets are turned into positive itemsets}, that is a strong additional constraint. 
For this constraint, eNSP has an additional parameter, denoted here $\varsigma$, which is the minimal frequency of the positive partner.
The lower is $\varsigma$, the less frequent negative patterns it discards, but the less efficient it is.
Indeed, we set $\varsigma=0.7\sigma$ \co{repetitif par rapport à avant?} which is a high value for specifying the set of positive partners on which negative patterns are explored. It is an advantageous setting for the efficiency of eNSP.}

An interesting result is that, for reasonably long sequences (20 or 30), even a weak maxgap constraint ($\tau=10$) significantly reduces the number of patterns and makes \NegPSpan\ more efficient.
This is of particular interest because the maxgap is a quite natural constraint when mining long sequences. It prevents from taking into account long distance correlations that are more likely irrelevant.
Another interesting question raised by this results is the real meaning of extracted patterns by eNSP.
In fact, under low frequency thresholds, it extracts numerous patterns that are not frequent when weak maxgap constraints are considered.
As a consequence, the significance of most of the patterns extracted by eNSP seems poor while processing ``long'' sequences datasets.

Figure \ref{fig:mg_cmp} also illustrates classical results encountered with sequential pattern mining algorithms. We can note that, for both algorithms, the number of patterns and runtime increase exponentially as the minimum support decreases. Also, the number of patterns and the runtime increase notably with sequence length.


\begin{figure*}[t]
\centering
\includegraphics[width=.45\textwidth]{timeVSfneg}
\includegraphics[width=.45\textwidth]{maxRSSVSfneg}\\
\includegraphics[width=.45\textwidth]{legend_fneg}
\caption{Comparison of computing time (left) and memory consumption (right) between eNSP and \NegPSpan\ wrt minimal support.}
\label{fig:th_cmp}
\end{figure*}
\co{modify the figure to change the legend from maxRSS to kBytes}

\espace

Figure \ref{fig:th_cmp} illustrates computation time and memory consumption with respect to minimum threshold for different settings: eNSP is ran with different values for $\varsigma$, the minimal frequency of the positive partner of negative patterns ($100\%$, $80\%$ and $20\%$ of the minimal frequency threshold) and \NegPSpan\ is ran with a maxgap of $10$ or without.
Computation times show similar results as in previous experiments: \NegPSpan\ becomes as efficient as eNSP with a (weak) maxgap constraint.
We can also notice that the minimal frequency of the positive partners does not impact eNSP computing times neither memory requirements.

The main result illustrated by this Figure is that \NegPSpan\ consumes significantly less memory than eNSP. This comes from the depth-first search strategy which prevents from memorizing many patterns. 
On the opposite, eNSP requires to keep in memory all frequent positive patterns and their occurrence list. The lower the threshold is, the more memory is required.


\subsection{Experiments on real datasets}
This section presents experiments on the real datasets from the SPMF repository.\footnote{\url{http://www.philippe-fournier-viger.com/spmf/index.php?link=datasets.php}} These datasets consist of click-streams or texts represented as sequences of items. Datasets features and results are reported in Table \ref{tab:realdatasets}. 
For every dataset, we have computed the negative sequential patterns with a maximum length of $l=5$ items and a minimal frequency threshold set to $\sigma=5\%$. \NegPSpan\ is set with a maxgap $\tau=10$ and eNSP is set with $\varsigma=.7\sigma$. For each dataset, we provide the computation time, the memory consumption and the numbers of positive and negative extracted patterns.
Note that the numbers of positive patterns for eNSP are given for $\varsigma$ threshold, \ie the support threshold for positive partners used to generate negative patterns.

For the \textit{sign} dataset, the execution has been stopped after 10 mn to avoid running out of memory. 
The number of positive patterns extracted by eNSP considering the $\sigma$ threshold is not equal to \NegPSpan\ simply because of the maxgap constraint.
 
The results presented in Table \ref{tab:realdatasets} confirm the results from experiments on synthetic datasets. 
First, it highlights that \NegPSpan\ requires significant less memory for mining every dataset. 
Second, \NegPSpan\ outperforms eNSP for datasets having a long mean sequence length (\textit{Sign}, \textit{Leviathan}, and \textit{MSNBC}).
In case of the \textit{Bible} dataset, the number of extracted patterns by eNSP is very low compared to \NegPSpan\ due to the constraint on minimal frequency of positive partners.

\begin{table}[tb]
%\centering

\footnotesize
\caption{Results on real datasets with setting $\sigma=5\%$, $l=5$, $\tau=10$, $\varsigma=.7 \sigma$. Bold faces highlight lowest computation times or memory consumptions.}
\label{tab:realdatasets}
~\hspace{-1.8cm}
\begin{tabular}{l|rrr|rrrr|rrrr|}
                      &  \multicolumn{3}{c|}{Dataset}  & \multicolumn{4}{c|}{\NegPSpan} & \multicolumn{4}{c|}{eNSP} \\ \hline
 & \multicolumn{1}{c}{$|\mathcal{D}|$} & \multicolumn{1}{c}{$|\mathcal{I}|$} & \multicolumn{1}{c|}{length} & \multicolumn{1}{c}{time ($s$)} & \multicolumn{1}{c}{mem ($kb$)} & \multicolumn{1}{c}{\#pos} & \multicolumn{1}{c|}{\#neg} & \multicolumn{1}{c}{time ($s$)} & \multicolumn{1}{c}{mem ($kb$)} & \multicolumn{1}{c}{\#pos} & \multicolumn{1}{c|}{\#neg}  \\ \hline
\textit{Sign}	& 730 &	267 &	51.99 &	\textbf{15.51} &	\textbf{6,220} &	348 &	1,357,278 &	349.84 (!) &	13,901,600 &	1,190,642 &	1,257,177  \\
\textit{Leviathan}	& 5,834	& 9,025	& 33.81	& \textbf{6.07}	& \textbf{19,932}	& 110	& 39797	& 28.43	& 428,916	& 7,691	& 17,220 \\
\textit{Bible}	& 36,369	& 13,905	& 21.64	& 38.82	& \textbf{68,944}	& 102	& 43,701	& \textbf{27.38}	& 552,288	& 1,364	& 2,621 \\
\textit{BMS1}	& 59,601	& 497	& 2.51	& \textbf{0.16}	& \textbf{22,676}	& 5	& 0	& 0.18	& 34,272	& 8	& 7\\
\textit{BMS2}	& 77,512	& 3,340	& 4.62	& 0.37	& \textbf{39,704}	& 1	& 0	& \textbf{0.35}	& 53,608	& 3	& 2\\
\textit{kosarak25k}	& 25,000	& 14804	& 8.04	& 0.92	& \textbf{24,424}	& 23	& 409	& \textbf{0.53}	& 43,124	& 50	& 51\\
\textit{MSNBC}	& 31,790	& 17	& 13.33	& \textbf{40.97}	& \textbf{41,560}	& 613	& 56,418	& 41.44	& 808,744	& 2,441	& 5,439\\ \hline
\end{tabular}
\end{table}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Case study: care pathway analysis}
This section presents the use of NSPs for analyzing  epileptic patient care pathways. Recent studies suggest that medication changes may be associated with epileptic seizures for patients with long term treatment with anti-epileptic (AE) medication \cite{polard2015brand}. 
NSP mining algorithms are used to extract patterns of drugs deliveries that may inform about the suppression of a drug from a patient treatment.
In \cite{DauxaisAIME17}, we studied discriminant temporal patterns but it does not explicitly extract the information about medication absence as a possible explanation of epiletic seizures. 

Our dataset was obtained from the french insurance database \cite{Moulis2015411} called SNIIRAM. 8,379 Epileptic patients were identified by their hospitalization
identified by their hospitalization related to an epileptic event.
For each patient, the sequence of drugs deliveries within the $90$ days before the epileptic event was obtained from the SNIIRAM. For each drug delivery, the event id is a tuple $\langle m, grp, g\rangle$ where $m$ is the ATC code of the active molecule, $g\in\{0,1\}$ is the brand-name ($0$) vs generic ($1$) status of the drug and $grp$ is the speciality group. The speciality group identifies the drug presentation (international non-proprietary name, strength per unit, number of units per pack and dosage form).
The dataset contains 251,872 events over 7,180 different drugs. The mean length of a sequence is 7.89$\pm$8.44 itemsets. Length variance is high due to the heterogenous nature of care pathways. Some of them represent complex therapies involving the consumption of many different drugs while others are simple case consisting of few deliveries of anti-epileptic drugs.

\espace

Let first compare results obtained by eNSP and \NegPSpan\ to illustrate the differences in the patterns sets extracted by each algorithm. To this end, we set up the algorithms with $\sigma=14.3\%$ ($1,200$ sequences), a maximum pattern length of $l=3$, $\tau=3$ for \NegPSpan\ and $\varsigma=.1\times\sigma$ the minimal support for positive partners for eNSP. eNSP extracts 1,120 patterns and \NegPSpan\ only 10 patterns (including positive and negative patterns). Due to a very low $\varsigma$ threshold, many positive patterns are extracted by eNSP leading to generate a lot of singleton negative patterns (\ie a pattern that hold a single negated item).

\begin{table}[tbh]
\caption{Patterns involving \textit{valproic acid} switches with their supports computed by eNSP and \NegPSpan.}
\small
\centering
\label{tab:valproicacid_comparison}
    \begin{tabular}{lcc}
        \hline
        pattern & \begin{tabular}[x]{@{}c@{}}support\\eNSP\end{tabular}  & \begin{tabular}[x]{@{}c@{}}support\\\NegPSpan\end{tabular}  \\ \hline
        $\seq{p}_1 = \langle 383\,\neg(86,383)\,383 \rangle$ & 1,579 & \\ 
        $\seq{p}_2 = \langle 383\,\neg 86\,383 \rangle$   & 1,251 & 1,243 \\ 
        $\seq{p}_3 = \langle 383\,\neg 112\,383 \rangle$  & 1,610 & \\ 
        $\seq{p}_4 = \langle 383\,\neg 114\,383 \rangle$  & 1,543 & 1,232\\
        $\seq{p}_5 = \langle 383\,\neg 115\,383 \rangle$  & 1,568 & 1,236\\
        $\seq{p}_6 = \langle 383\,\neg 151\,383 \rangle$  & 1,611 & \\
        $\seq{p}_7 = \langle 383\,\neg 158\,383 \rangle$  & 1,605 & \\
        $\seq{p}_8 = \langle 383\,\neg 7\,383 \rangle$    &      & 1,243\\
        \hline
    \end{tabular}
\end{table}

Precisely, we pay attention to the specific specialty of \textit{valproic acid} which exists in generic form (event $383$) or brand-named form (event $114$) by selecting patterns that start and finish with event $383$. The complete list of these patterns is given in Table \ref{tab:valproicacid_comparison}. 
Other events correspond to other anti-epileptic drugs 
 ($7$: \textit{levetiracetam}, $158$: \textit{phenobarbital})
or psycholeptic drugs 
 ($112$: \textit{zolpidem}, $115$: \textit{clobazam}, $151$: \textit{zopiclone})
except $86$ which is \textit{paracetamol}.

First, it is interesting to note that with this setting, the two algorithms share only 3 patterns $\seq{p}_2$, $\seq{p}_4$ and $\seq{p}_5$, which have lower support with \NegPSpan\ because of the maxgap constraint. This constraint also explains that pattern  $\seq{p}_3$ and $\seq{p}_6 $ are not extracted by \NegPSpan.
These patterns illustrate that in some cases, the patterns extracted by eNSP may not be really interesting because they involve distant events in the sequence.
Pattern $\seq{p}_{1}$ is not extracted by \NegPSpan\ due to the strict-embedding pattern semantics.
With eNSP semantics, $\seq{p}_{1}$ means that there is no delivery of \textit{paracetamol} and \textit{valproic acid} at the same time. With \NegPSpan\ semantics, $\seq{p}_{1}$ means that there is no delivery of \textit{paracetamol} neither \textit{valproic acid} between two deliveries of \textit{valproic acid}. The latter is stronger and the pattern support is lower.
On the opposite, \NegPSpan\ can extract patterns that are missed by eNSP. For instance, pattern $\seq{p}_8$ is not extracted by eNSP because its positive partner, $\langle 383, 7,383\rangle$, is not frequent. In this case, it leads eNSP to miss a potentially interesting pattern involving two anti-epileptic drugs.

Now, we look at patterns involving a switch from generic form to brand-named form of \textit{valproic acid} with the following settings $\sigma=1.2\%$, $l=3$ and $\tau=5$. Mining only positive patterns extracts the frequent patterns $\langle 114,383,114 \rangle$ and $\langle 114,114 \rangle$. It is impossible to conclude about the possible impact of a switch from $114$ to $383$ as a possible event triggering an epileptic crisis. From negative patterns extracted by \NegPSpan, we can observe that the absence of switch $\langle 114\,\neg 383\,114\rangle$ is also frequent in this dataset. Contrary to eNSP semantics which does bring a new information (that can be deduced from frequent patterns), this pattern concerns embeddings corresponding to real interesting cases thanks to gap constraints.

%(114),(383),(383): 102 
%(114),-(383),(114): 960
%(114),-(114),(383): 176
%(383),-(114),(383): 1504
%(114),-(383),(383): 214
%(114),(383): 214

