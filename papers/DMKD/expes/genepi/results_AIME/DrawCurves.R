rm(list=ls())
library(readr)
library(reshape2)
require(ggplot2)
library(dplyr)

#function required for legend generation
require(gridExtra)
g_legend <- function(a.gplot){
  tmp<- ggplot_gtable(ggplot_build(a.gplot))
  leg <- which(sapply(tmp$grobs, function(x) x$name) == "guide-box")
  legend <- tmp$grobs[[leg]]
  return(legend)
}

# if export legend is True, then, a legend is generated in a separated PDF file (and figure does not have a proper legend)
export_legend=T

######################################################################"
wd = "/home/tguyet/Recherche/Publications/2019/AIME/Negatives/results"
setwd(wd)
results <- read_delim("cmp_genepi.csv", ",", escape_double = FALSE)

###################################################
#courbes time
data = results %>%
  filter( !(method=="ensp" & f==0.12 & fp<=8) & !(method=="ensp" & f==0.15 & fp==4) )
data$param = data$fp*3 + data$mg
data$param=factor(data$param)

p <- ggplot(data, mapping=aes(x=f, y=time, linetype=method, shape=param))
p <- p + geom_smooth(method="lm", se=F)
p <- p + scale_y_log10()
p <- p + geom_point(size=5.)
p <- p + xlab("Frequency Threshold") + ggtitle("") + ylab("Time (s)")
p <- p + scale_linetype_discrete(
  limits=c('censp','ensp'),
  labels=c('censp' = "NegPSpan", 'ensp'= "eNSP"))
p <- p + scale_shape_discrete(
  limits=c('3','5','8','12','24','30'),
  labels=c('3'= expression(italic(tau)~"=3"), '5'= expression(italic(tau)~"=5"), '8'= expression(italic(tau)~"=8"), '12'= expression(varsigma~"= 0.4"~sigma), '24'= expression(varsigma~"= 0.8"~sigma), '30'= expression(varsigma~"="~sigma)))
p <- p + theme_bw()
p <- p + theme(
  axis.text.x = element_text(size = 20, color='black'),
  axis.text.y = element_text(size = 20, color='black'),
  axis.title.y = element_text(size=24),
  axis.title.x = element_text(size=24),
  strip.text = element_text(size=24),
  legend.title=element_blank(),
  panel.grid.major = element_blank())#, panel.grid.minor = element_blank())
#p <- p + theme(text = element_text(size=24),legend.position = c(0.9, 0.1))
if(export_legend) {
  p <- p + theme(legend.position="none")
}
p

ggsave(filename="timeVSfneg.pdf", plot=p, width = 12, height = 9)

if(export_legend) {
  # Generate legend (to be saved manually)
  p <- p + theme(text = element_text(size=20),legend.position = "bottom") + guides(color=guide_legend(title=""),shape=guide_legend(title="", keywidth=.6, keyheight=.3, default.unit="inch"),linetype=guide_legend(title="", keywidth=.6, default.unit="inch"))
  legend <- g_legend(p)
  #grid.arrange(legend)
  #legend
  ggsave(filename="legend_fneg.pdf", plot=legend, width = 7.5, height = .6)
}
###################################################
#courbes mémoire (maxRSS)
data = results %>%
  filter( !(method=="ensp" & f==0.12 & fp<=8) & !(method=="ensp" & f==0.15 & fp==4) )
data$param = data$fp*3 + data$mg
data$param=factor(data$param)

p <- ggplot(data, mapping=aes(x=f, y=MaxRSS, linetype=method, shape=param))
p <- p + geom_smooth(method="lm", se=F)
p <- p + scale_y_log10()
p <- p + geom_point(size=5.)
p <- p + xlab("Frequency Threshold") + ggtitle("") + ylab("Memory (bytes)")
p <- p + scale_linetype_discrete(
  limits=c('censp','ensp'),
  labels=c('censp' = "NegPSpan", 'ensp'= "eNSP"))
p <- p + scale_shape_discrete(
  limits=c('3','5','8','12','24','30'),
  labels=c('3'= expression(italic(tau)~"=3"), '5'= expression(italic(tau)~"=5"), '8'= expression(italic(tau)~"=8"), '12'= expression(varsigma~"= 0.4"~sigma), '24'= expression(varsigma~"= 0.8"~sigma), '30'= expression(varsigma~"="~sigma)))
p <- p + theme_bw()
p <- p + theme(
  axis.text.x = element_text(size = 20, color='black'),
  axis.text.y = element_text(size = 20, color='black'),
  axis.title.y = element_text(size=24),
  axis.title.x = element_text(size=24),
  strip.text = element_text(size=24),
  legend.title=element_blank(),
  panel.grid.major = element_blank())#, panel.grid.minor = element_blank())
#p <- p + theme(text = element_text(size=24),legend.position = c(0.9, 0.1))
if(export_legend) {
  p <- p + theme(legend.position="none")
}
p

ggsave(filename="maxRSSVSfneg.pdf", plot=p, width = 12, height = 9)

###################################################
#courbes nbpatterns
data = results %>%
  filter( !(method=="ensp" & f==0.12 & fp<=8) & !(method=="ensp" & f==0.15 & fp==4) )
data$param = data$fp*3 + data$mg
data$param=factor(data$param)

p <- ggplot(data, mapping=aes(x=f, y=NbPat, linetype=method, shape=param))
p <- p + geom_smooth(method="lm", se=F)
p <- p + scale_y_log10()
p <- p + geom_point(size=5.)
p <- p + xlab("Frequency Threshold") + ggtitle("") + ylab("#Patterns")
p <- p + scale_linetype_discrete(
  limits=c('censp','ensp'),
  labels=c('censp' = "NegPSpan", 'ensp'= "eNSP"))
p <- p + scale_shape_discrete(
  limits=c('3','5','8','12','24','30'),
  labels=c('3'= expression(italic(tau)~"=3"), '5'= expression(italic(tau)~"=5"), '8'= expression(italic(tau)~"=8"), '12'= expression(varsigma~"= 0.4"~sigma), '24'= expression(varsigma~"= 0.8"~sigma), '30'= expression(varsigma~"="~sigma)))
p <- p + theme_bw()
p <- p + theme(
  axis.text.x = element_text(size = 20, color='black'),
  axis.text.y = element_text(size = 20, color='black'),
  axis.title.y = element_text(size=24),
  axis.title.x = element_text(size=24),
  strip.text = element_text(size=24),
  legend.title=element_blank(),
  panel.grid.major = element_blank())#, panel.grid.minor = element_blank())
#p <- p + theme(text = element_text(size=24),legend.position = c(0.9, 0.1))
if(export_legend) {
  p <- p + theme(legend.position="none")
}
p

ggsave(filename="nbpatnegVSfneg.pdf", plot=p, width = 12, height = 9)


