"""
python3 -venv analyseenv
source ./bin/activate
"""

import pandas as pd
from mlxtend.frequent_patterns import fpgrowth


dataset=pd.read_csv(open("survey.csv"))

ds=dataset[['s0', 's1', 's2', 's3', 's4', 'i0', 'i1', 'i2', 'i3', 'i4', 'e0', 'e1', 'e2', 'e3',
       'o0', 'o1', 'o2', 'o3', 'm0', 'm1', 'm2', 'm3', 'm4']]


frequent_itemsets=fpgrowth(ds, min_support=0.3, use_colnames=True)

frequent_itemsets['length'] = frequent_itemsets['itemsets'].apply(lambda x: len(x))

#### association rules

import pandas as pd
from mlxtend.frequent_patterns import fpgrowth
from mlxtend.frequent_patterns import association_rules
dataset=pd.read_csv(open("survey.csv"))

#binarisation of 'expert' attribute
dataset['exp0']=(dataset.expert==0)
dataset['exp1']=(dataset.expert==1)
dataset['exp2']=(dataset.expert==2)

ds=dataset[['exp0', 'exp1', 'exp2','p0','p1','p2','p3','s0', 's1', 's2', 's3', 's4', 'i0', 'i1', 'i2', 'i3', 'i4', 'e0', 'e1', 'e2', 'e3', 'o0', 'o1', 'o2', 'o3', 'm0', 'm1', 'm2', 'm3', 'm4']]

ds=dataset[['exp0', 'exp1', 'exp2','p0','p1','p2','p3','i0', 'i1', 'i2', 'i3', 'i4']]


frequent_itemsets=fpgrowth(ds, min_support=0.15, use_colnames=True)
rules= association_rules(frequent_itemsets, metric='confidence', min_threshold=0.5, support_only=False)

rules[ rules['antecedents'].apply(lambda x: not ('i0' in x or 'i1' in x or 'i2' in x or 'i3' in x or 'i4' in x) ) ][['antecedents','consequents','support','confidence']]


rules[['antecedents','consequents','support','confidence']]

######################
# occurrence ?

ds=dataset[['exp0', 'exp1', 'exp2','p0','p1','p2','p3', 'o0', 'o1', 'o2', 'o3']]

ds['weaklyoccurs']= (ds.o0==1) & (ds.o1==1) & (ds.o3==1) & (ds.o2==0)
ds['stronglyoccurs']=(ds.o0==1) & (ds.o3==0) & (ds.o2==0)
ds['other']= ~( ds.weaklyoccurs | ds.stronglyoccurs)

ds=ds[['weaklyoccurs', 'stronglyoccurs', 'other', 'exp0', 'exp1', 'exp2','p0','p1','p2','p3']]

frequent_itemsets=fpgrowth(ds, min_support=0.15, use_colnames=True)
rules= association_rules(frequent_itemsets, metric='confidence', min_threshold=0.2, support_only=False)

sel_rules= rules[ rules['antecedents'].apply(lambda x: not ('weaklyoccurs' in x or 'stronglyoccurs' in x or 'other' in x) ) ]
sel_rules= sel_rules[ rules['consequents'].apply(lambda x: 'weaklyoccurs' in x or 'stronglyoccurs' in x or 'other' in x) ]

sel_rules[['antecedents','consequents','support','confidence']]


############### FCA data preparation

import json

names_json={'ObjNames':[i for i in range(len(ds))], 'Params':{'AttrNames':ds.columns.to_list()}}

data=[]

for i in range(len(ds)):
	count=sum(ds.iloc[i])
	inds=ds.iloc[i].reset_index(drop=True).loc[lambda s: s > 0].index.to_list()
	data.append( {'Count':count,'Inds':inds} )

data_json={'Count':len(ds), 'Data':data}

with open('fca_all.json', 'w') as outfile:
    json.dump([names_json,data_json], outfile)

# do it for each question

ds=dataset[['s0', 's1', 's2', 's3', 's4']]
names_json={'ObjNames':[i for i in range(len(ds))], 'Params':{'AttrNames':ds.columns.to_list()}}
data=[]
for i in range(len(ds)):
	count=sum(ds.iloc[i])
	inds=ds.iloc[i].reset_index(drop=True).loc[lambda s: s > 0].index.to_list()
	data.append( {'Count':count,'Inds':inds} )
data_json={'Count':len(ds), 'Data':data}
with open('fca_s.json', 'w') as outfile:
    json.dump([names_json,data_json], outfile)

ds=dataset[['i0', 'i1', 'i2', 'i3', 'i4']]
names_json={'ObjNames':[i for i in range(len(ds))], 'Params':{'AttrNames':ds.columns.to_list()}}
data=[]
for i in range(len(ds)):
	count=sum(ds.iloc[i])
	inds=ds.iloc[i].reset_index(drop=True).loc[lambda s: s > 0].index.to_list()
	data.append( {'Count':count,'Inds':inds} )
data_json={'Count':len(ds), 'Data':data}
with open('fca_i.json', 'w') as outfile:
    json.dump([names_json,data_json], outfile)

ds=dataset[['e0', 'e1', 'e2', 'e3']]
names_json={'ObjNames':[i for i in range(len(ds))], 'Params':{'AttrNames':ds.columns.to_list()}}
data=[]
for i in range(len(ds)):
	count=sum(ds.iloc[i])
	inds=ds.iloc[i].reset_index(drop=True).loc[lambda s: s > 0].index.to_list()
	data.append( {'Count':count,'Inds':inds} )
data_json={'Count':len(ds), 'Data':data}
with open('fca_e.json', 'w') as outfile:
    json.dump([names_json,data_json], outfile)

ds=dataset[['o0', 'o1', 'o2', 'o3']]
names_json={'ObjNames':[i for i in range(len(ds))], 'Params':{'AttrNames':ds.columns.to_list()}}
data=[]
for i in range(len(ds)):
	count=sum(ds.iloc[i])
	inds=ds.iloc[i].reset_index(drop=True).loc[lambda s: s > 0].index.to_list()
	data.append( {'Count':count,'Inds':inds} )
data_json={'Count':len(ds), 'Data':data}
with open('fca_o.json', 'w') as outfile:
    json.dump([names_json,data_json], outfile)

ds=dataset[['m0', 'm1', 'm2', 'm3', 'm4']]
names_json={'ObjNames':[i for i in range(len(ds))], 'Params':{'AttrNames':ds.columns.to_list()}}
data=[]
for i in range(len(ds)):
	count=sum(ds.iloc[i])
	inds=ds.iloc[i].reset_index(drop=True).loc[lambda s: s > 0].index.to_list()
	data.append( {'Count':count,'Inds':inds} )
data_json={'Count':len(ds), 'Data':data}
with open('fca_m.json', 'w') as outfile:
    json.dump([names_json,data_json], outfile)
