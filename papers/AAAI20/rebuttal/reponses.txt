We first thanks the reviewers for their insightful comments.

** To R1 **
Thanks a lot for your detailed comments and suggestions. They will be very interesting to improve our work and our article.
About your specific questions:
         - if |qi| <= 1 for all negative itemsets, then basically partial-non-inclusion and total non inclusion coincide, then by direct application of lemma 3 we get lemma 4.

Yes, Your are correct. Thank you.

         - if one uses gap constraints (successive positive itemsets cannot further apart than x itemsets) with gaps of at most 1, then strict and soft embedding coincide

Yes, Your are correct. Thank you.

         - "We can finally point out that Lemma 4..." -> doesn't it collapse also partial and total non-inclusion?

Yes, Your are correct. Thank you.

         - In the counter example for equation (11), I fail to see why not p\theta's.

The occurrence of the positive partner (1,5) is not an occurrence of the negative patterns because both b and c occur in between these positions (soft-embedding). And because \theta' involve strong-occurrence, it is false that p\theta' s.

** To R2 **
Our article does not present a study on algorithm complexity neither application because it is focus on the formal definition of negative sequential patterns. We strongly believe that it is valuable to study what is interesting to compute before to propose algorithms to compute them. 
We provide references showing the potential interest of negative sequential patterns and also the need to provide better formalisation. The reference you propose is an example of some practical developments that may result from our most interesting conclusions.

Moreover, this article defines negative sequential patterns, hence we do not think that readers need to have any background in this field.


- Please define what is N in lemma 4 ?

N is defined page 2. It denotes the set of negative sequential patterns

- Some proofs are unclear and contain typos, but maybe I’m wrong. Bellow, a list of examples that need to be clarified :
         1/ how α can be in q_i and than after in p_i, to finally conclude on q_i (proof of lemma 3)? 

This is a typo, p_i should be q_i.

         2/ Where the "l" came from in the proof of lemma 5 ? 

This is a typo, i should be m.

         3/ Can you explain the use of lemma 3 in the proof of proposition 1 ?

The Lemma 3 is indeed not useful in this case of the proposition 1. The Lemma 2 is sufficient here.

** to R3 **
- Am pretty sure I understood everything. Actually I found the technical constructions not particularly deep.

We agree that our technical construction is not deep, but we share your opinion about the need of clean-up for these notions. Recent works (see Cao 2016, in Artificial Intelligence Journal, and Wang 2019, in ACM Computing Survey) present practical uses of negative sequential patterns but they state false results in the broad community of artificial intelligence. In our mind, this is a necessity to convey clear definitions and correct properties about NSP in this community, ie the AAAI audience. 

