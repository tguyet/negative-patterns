\section{ASP -- Answer Set Programming}
\label{sec:ASP}

In this section we introduce the Answer Set Programming (ASP) paradigm, syntax and tools. Sect. \ref{sec:aspprinc} introduces the main principles and notations of ASP. Sect. \ref{sec:aspex} illustrates them on the well-known graph coloring problem.

\subsection{Principles of Answer Set Programming}
\label{sec:aspprinc}
ASP is a declarative programming paradigm. 
From a general point of view, declarative programming gives a description of what is a problem instead of specifying how to solve it.
Several declarative paradigms have been proposed, differing in the modelling formalism they use. For instance, logic programming \cite{lallouet:hal-00758896} specifies the problem using a logic formalism, the SAT para\-digm encodes the problem with boolean expressions \cite{biere2009handbook}, the CP (constraint programming) paradigm specifies the problem using constraints \cite{rossi2006handbook}.
%The problem is specified using a logic formalism where  or CP programming uses constraints \cite{}.
%A declarative program contains, one the one hand, the specification of the problem and, on the other hand, the specification of the problem instance, \ie the concrete problem to be solved.
ASP belongs to the class of logic programming paradigms, such as Prolog. 
The high-level syntax of logic formalisms makes generally the program easier to understand than other declarative programming paradigms.%\comment{RQ: statement with no justification. Sentence to be deleted?}

\espace

An \emph{ASP program} is a set of rules of the form
\begin{align}\label{eq:rule}\tt
a_0 \mathrel{\texttt{:-}} a_1,\ldots,a_m, \text{\lstinline!not! } a_{m+1},\ldots,\text{\lstinline!not! } a_n.
\end{align}

where each $\mathtt{a_i}$ is a propositional atom for $\mathtt{0}\leq\mathtt{i}\leq\mathtt{n}$ and \lstinline!not! stands for \emph{default negation}. In the body of the rule, commas denote conjunctions between atoms.
Contrary to Prolog, the order of atoms is meaningless.
In ASP, rule \eqref{eq:rule}  may be interpreted as ``\emph{if $\mathtt{a_1,\ldots,a_m}$ are all true and if none of $\mathtt{a_{n+1},\ldots,a_n}$ can be proved to be true, then $\mathtt{a_0}$ is true.}"

If $\mathtt{n}=\mathtt{0}$, \ie the rule body is empty,  \eqref{eq:rule} is called a \emph{fact} and the symbol ``\texttt{:-}'' may be omitted. Such a rule states that the atom $a_0$ has to be true.
If $\mathtt{a_0}$ is omitted, \ie the rule head is empty,  \eqref{eq:rule} represents an integrity constraint. 

Semantically, a logic program induces a collection of so-called \emph{answer sets},
which are distinguished models of the program determined by answer sets semantics;
see \cite{gellif91a} for details.
For short, a model assigns a truth value to each propositional atoms of the program and this set of assignments is valid. An answer set is a minimal set of true propositional atoms that satisfies all the program rules.
Answer sets are said to be minimal in the way that only atoms that have to be true are actually true.

\espace

To facilitate the use of ASP in practice, several extensions have been developed. 
First of all, rules with \emph{variables} are viewed as shorthands for the set of their ground instances.
This allows for writing logic programs using a first order syntax. Such kind of syntax makes program shorter, but it hides the grounding step and its specific encoding issues, especially from the memory management point of view.

Further language constructs include \emph{conditional literals} and \emph{cardinality constraints} \cite{siniso02a}.
The former are of the form
\[\tt
a\mathrel{:}{b_1,\dots,b_m}
\]
the latter can be written as
\[\tt
s~\{c_1;\dots;c_n\}~t
\]
where $\mathtt{a}$ and $\mathtt{b_i}$ are possibly default negated literals for $0\leq i\leq m$,
and each $\mathtt{c_j}$ is a conditional literal for $1\leq i\leq n$. The purpose of conditional literals is to govern the instantiation of a literal \texttt{a} through the literals $\tt{b_1,\dots,b_m}$. In a cardinality constraint,
$\mathtt{s}$ (resp. $\mathtt{t}$) provides the lower (resp. upper) bound on the number of literals from $\tt{c_1;\dots;c_n}$ that must be satisfied in the model.

A cardinality constraint in the head of the rule defines a specific rule called a \textit{choice rule}:
\[\tt
s~\{c_1;\dots;c_n\}~t \mathrel{\texttt{:-}} a.
\]
%Such specific rules are called \emph{choice rules}. 
If $\mathtt{a}$ is true then all atoms of a subset $\mathcal{S} \subset \{c_1,\dots,c_n\}$ of size between $s$ and $t$ have to be true. All such subsets are admissible according to this unique rule, but not in the same model. All such subsets contribute to alternative answer sets.
%This kind of rule specifies the problem combinatorics.
It should be noted that alternative models are solved independently. It is not possible to specify constraints that involve several models.

%The practical value of both constructs becomes more apparent when used in conjunction with variables.
%For instance, a conditional literal like
%\(\tt
%{a(\text{\lstinline!X!})}\mathrel{:}{b(\text{\lstinline!X!})}
%\)
%in a rule's antecedent expands to the conjunction of all instances of $\mathtt{a(\text{\lstinline!X!})}$ for which the corresponding instance of $\mathtt{b(\text{\lstinline!X!})}$ holds.
%
%Similarly,
%\(\tt
%2~\{a(\text{\lstinline!X!}):b(\text{\lstinline!X!})\}~4
%\)
%   holds whenever between two and four instances of $\mathtt{a(\text{\lstinline!X!})}$ (subject to $\mathtt{b(\text{\lstinline!X!})}$) are true.

%\espace

%Finally, it is possible to have some optimisation statements

% Similarly, objective functions minimizing the sum of weights $w_j$ of conditional literals $c_j$ are expressed as
% \(
% \#\mathit{minimize}~\{w_1:c_1,\dots,w_n:c_n\}
% \).

%Specifically, we rely in the sequel on the input language of the ASP system \clingo\ \cite{gekakasc14b}.


ASP problem solving is ensured by efficient solvers \cite{lifschitz:aaai:2008} which are based on the same technologies as constraint programming solvers or satisfiability checking (SAT) solvers. 
\smodels\ \cite{syrjanen2001smodels}, \dlv\ \cite{Leone2006}, \sysfont{ASPeRiX} \cite{Asperix} or \clingo\ \cite{gekakaosscsc11a} are  well-known ASP solvers. Due to the computational efficiency it has demonstrated and its broad application to real problems, we use \clingo\ as a basic tool for designing our encodings.

\espace

%To write ASP program, the programmer may have in mind that ASP follows a generate-and-test methodology. 
The basic method for  programming in ASP is to follow a \textit{generate-and-test} methodology.
Choice rules generate solution candidates, while integrity constraints are tested to eliminate those candidates that violate the constraints.
The programmer should not have any concern about how solutions are generated. He/she just has to know that all possible solutions will be actually evaluated.
From this point of view, the ASP programming principle is closer to CP programming than to Prolog programming.
Similarly to these declarative programming approaches, the  difficulty of programming in ASP lies in the choices for the best way to encode problem constraints: it must be seen as the definition of the search space (\emph{generate} part) or as an additional constraint on solutions within this search space (\emph{test} part). This choices may have a large impact on the efficiency of the problem encoding.

\subsection{A simple example of ASP program}\label{sec:aspex}
The following example illustrates the ASP syntax on encoding the graph coloring problem.
Lines 9-10 specify the problem as general rules that solutions must satisfy while lines 1-6 give the input data that defines the problem instance related to the graph in Fig. \ref{fig:graph}.

\begin{figure}[tp]
\centering
\includegraphics[width=0.3\linewidth]{graph}
\caption{An example graph for the graph coloring problem.}
\label{fig:graph}
\end{figure}

The problem instance is a set of colors, encoded with predicates \lstinline!col/1! and a graph, encoded with predicates \lstinline!vertex/1! and \lstinline!edge/2!.
The input graph has 5 vertices numbered from 1 to 5 and 12 edges. The 5 fact-rules describing the vertex are listed in the same line (Line 2).
It should be noted that, generally speaking, \lstinline!edge(1,2)! is different from \lstinline!edge(2,1)!, but considering that integrity constraints for the graph coloring problem are symmetric, it is sufficient to encode directed edge in only one direction.
Line 6 encodes the 3 colors that can be used: \lstinline!r!, \lstinline!g! and \lstinline!b!. Lower case letters represent values, internally encoded as integers, while strings beginning with upper case letters represent variables (see line 9 for instance).

\begin{lstlisting}[caption={Encoding of graph coloring -- ASP syntax and encoding example}, label=list:graph_coloring, float]
% instance of the problem: the graph and colors
vertex(1). vertex(2). vertex(3). vertex(4). vertex(5).
edge(1,2). edge(1,3). edge(1,4). edge(2,4). edge(2,5).
edge(3,1). edge(3,4). edge(3,5). edge(4,1). edge(4,2).
edge(5,3). edge(5,4).
col(r). col(b). col(g).

% graph coloring problem specification
1 { color(X, C) : col(C) } 1 :- vertex(X).
:- edge(X, Y), color(X, C), color(Y, C).
\end{lstlisting}

Lines 9 and 10 specify the graph coloring problem. The predicate \lstinline!color/2! encodes the color of a vertex:  \lstinline!color(X,C)! expresses that vertex \lstinline!X! has color \lstinline!C!.
Line 10 is an integrity constraint. It forbids neighbor vertices \lstinline!X! and \lstinline!Y! to have the same color \lstinline!C!\footnote{It is important to notice that the scope of a variable is the rule and each occurrence of a variable in a rule represents the same value.}.
The ease of expressing such integrity constraints is a major feature of ASP.

Line 9 is a choice rule indicating that for a given vertex \lstinline!X!, an answer set must contain exactly one atom of the form \lstinline!color(X,C)! where \lstinline!C! is a color. 
The grounded version of this rule is the following:
\begin{lstlisting}[numbers=none]
1 { color(1, r), color(1, b), color(1, g) } 1.
1 { color(2, r), color(2, b), color(2, g) } 1.
1 { color(3, r), color(3, b), color(3, g) } 1.
1 { color(4, r), color(4, b), color(4, g) } 1.
1 { color(5, r), color(5, b), color(5, g) } 1.
\end{lstlisting}

The variable \lstinline!X! is expanded according to the facts in line 2 and for each vertex, a specific choice rule is defined. Within the brackets, the variable \lstinline!C! is expanded according to the conditional expression in the rule head of line 9: the only admissible values for \lstinline!C! are color values.
For each line of the grounded version of the program, one and only one atom within brackets can be chosen. This corresponds to a unique mapping of a color to a vertex.
Line 9 can be seen as a search space generator for the graph coloring problem.

The set \lstinline!color(1,b) color(2,r) color(3,r) color(4,g) color(5,b)! is an answer set for the above program (among several others).
%\end{ex}

\espace

For more detailed presentation of ASP programming paradigm, we refer the reader to recent article of Janhunen and Nimeläthe \cite{Janhunen16}.

\subsection{The \sysfont{Potassco} collection of ASP tools}

The \sysfont{Potassco} collection is a set of tools for ASP developed at the University of Potsdam. The main tool of the collection is the ASP solver \clingo\  \cite{gekakaosscsc11a}. This solver offers both a rich syntax to facilitate encodings\footnote{\clingo\ is fully compliant with the recent ASP standard:
\url{https://www.mat.unical.it/aspcomp2013/ASPStandardization}} and a good solving efficiency. It is worth-noting that the ASP system \clingo\ introduced many facilities to accelerate the encoding of ASP programs. % that can be used in final programs. 
For the sake of simplicity, we do not use them in the presented programs. A complete description of the \clingo\ syntax can be found in \cite{gekakasc14b}.

The \clingo\ solving process follows two consecutive main steps:
\begin{enumerate}
\item  \emph{grounding} transforms the initial ASP program into a set of propositional clauses, cardinality constraints and optimisation clauses. Note that grounding is not simply a systematic problem transformation. It also simplifies the rules to generate the as short as possible equivalent grounded program.
\item \emph{solving} consists in finding from one to all solutions of the grounded program. This step is performed by \clasp\  which is a conflict-driven ASP solver. The primary \clasp\ algorithm relies on conflict-driven \emph{nogood} learning. 
It is further optimized using sophisticated reasoning and implementation techniques, some specific to ASP, others borrowed from CDCL-based SAT solvers.
\end{enumerate}

The overall process may be controlled using procedural languages, \eg \emph{Python} or \emph{lua} \cite{gekakasc14b}. These facilities are very useful to automate processes and to collect statistics on solved problems.
Despite this procedural control which enables to interact with the grounder or the solver, it is important to note that once a program has been grounded, it cannot be changed.




