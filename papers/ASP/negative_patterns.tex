\section{Negative sequential patterns}\label{sec:negpatterns:definitions}
In the sequel, $[n]=\{1, \dots, n\}$ denotes the set of the first $n$ strictly positive integers.
%
Let $\mathcal{I}$ be the set of items (alphabet). 
An \emph{itemset} $A=\{a_1\ a_2\ ...\ a_m\}\subseteq \mathcal{I}$ is a set of items.
%
A \emph{sequence} $\seq{s}$ is a set of sequentially ordered itemsets $\seq{s} = \langle s_1\ s_2\ ...\ s_n\rangle$: $\forall i,j \in [n],\; i < j$ means that $s_i$ is located before $s_j$ in sequence $\seq{s}$ which starts by $s_1$ and finishes by $s_n$. 

\espace

In the following, we introduce we 


\begin{definition}[Strongly negative sequential patterns (NSP)]\label{def:stronglynegativepattern}


\begin{definition}[Negative sequential patterns (NSP)]\label{def:negativepattern}
A negative pattern is a finite sequence of $n$ positive itemsets $p_i \subseteq \mathcal{I}\setminus\emptyset$  for all $i\in [n]$; and $n-1$ negative itemsets $q_i\subseteq \mathcal{I}$  for all $i\in [n-1]$.

$\seq{p} = \langle p_1\ \neg q_1 \ p_2\  \neg q_2\ \dots $ $p_{n-1}\ \neg q_{n-1}\ p_n\rangle$ is a finite sequence where $p_i \subseteq \mathcal{I}\setminus\emptyset$ for all $i\in [n]$ and $q_i\subseteq \mathcal{I}$ for all $i\in [n-1]$.

The \emph{length} of a NSP, denoted $|\seq{p}|$ is $n$, its number of itemsets (negative or positive). 
%
$\seq{p}^+=\langle p_1\,\dots\ p_n\rangle$ is so-called the positive part of the NSP.
\end{definition}

We denote by $\mathcal{N}$ the set of negative sequential patterns.

It can be noticed that Definition \ref{def:negativepattern} introduces a syntactic limitation on negative sequential patterns:
\begin{itemize}
\item a pattern can not start neither finish by a negative pattern,
\item a pattern can not have two successive negative itemsets.
\end{itemize}

\begin{example}[Negative sequential pattern]
This example illustrates notations of definitions \ref{def:negativepattern}.
Let $\mathcal{I}=\{a,b,c,d\}$ and $\seq{p}=\langle a\ \neg (bc)\ (ad)\ d\ \neg (ab)\ d\rangle$.
We have $p_1=\{a\}$,  $p_2=\{ad\}$, $p_3=\{d\}$, $p_4=\{d\}$ and $q_1=\{bc\}$, $q_2=\emptyset$, $q_3=\{ab\}$.
The length of $\seq{p}$ is $|\seq{p}|=6$ and $\seq{p}^+=\langle a\ (ad)\ d\ d\rangle$. 
\end{example}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Semantics of negative sequential patterns}\label{sec:negpatterns:semantics}
The semantics of negative sequential patterns relies on \emph{negative containment}: a sequence $\seq{s}$ supports pattern $\seq{p}$ (or $\seq{p}$ matches the sequence $\seq{s}$) iff $\seq{s}$ contains a sub-sequence $\seq{s}'$ such that every positive itemset of $\seq{p}$ is included in some itemset of $\seq{s}'$ in the same order and for any negative itemset $\neg i$ of $\seq{p}$, $i$ is \emph{not included} in any itemset occurring in the sub-sequence of $\seq{s}'$ located between the occurrence of the positive itemset preceding $\neg i$ in $\seq{p}$ and the occurrence of the positive itemset following $\neg i$ in $\seq{p}$.

\begin{definition}[Non inclusion]\label{def:IS_notincluded}
We introduce two operators relating two itemsets $P\subseteq\mathcal{I}\setminus\emptyset$ and $I\subseteq\mathcal{I}$:
\begin{itemize}
\item partial non inclusion: $P\not\preceq I \Leftrightarrow \exists e \in P$, $e \notin I$
\item total non inclusion: $P\not\sqsubseteq I \Leftrightarrow \forall e \in P, e \notin I$
\end{itemize}

and, by definition, $\emptyset\not\sqsubseteq I$ and $\emptyset\not\preceq I$ for all $I\subseteq\mathcal{I}$.

\end{definition}

Choosing one non inclusion interpretation or the other has consequences on extracted patterns as well as on pattern search. Let's illustrate this on related pattern support in the following sequence dataset:
$$\mathcal{D} = \left\{
\begin{array}{l}
\seq{s}_1=\langle (bc)\ f\ a \rangle \\
\seq{s}_2=\langle (bc)\ (cf)\ a \rangle \\
\seq{s}_3=\langle (bc)\ (df)\ a \rangle \\
\seq{s}_4=\langle (bc)\ (ef)\ a \rangle\\
\seq{s}_5=\langle (bc)\ (cdef)\ a \rangle
\end{array}
\right\}.$$
Table \ref{tab:partial-total} compares the support of patterns under the two semantics.
Let's consider pattern $\seq{p}_2$ on sequence $\seq{s}_2$. 
Considering that the positive part of $\seq{p}_2$ is in $\seq{s}_2$, $\seq{p}_2$ occurs in the sequence iff $(cd)\not\subseteq (cf)$. In case of total non inclusion, it is false that $(cd)\not\sqsubseteq (cf)$ because of $c$ that occurs in $(cf)$, and thus $\seq{p}_2$ does not occur in $\seq{s}_2$. But in case of a partial non inclusion, it is true that $(cd)\not\preceq (cf)$, because of $d$ that does not occur in $(cf)$, and thus $\seq{p}_2$ occurs in $\seq{s}_2$.

\begin{lemma}\label{lemma:non_incl_relation}\footnote{All proofs are provided in Annex \ref{sec:proofs}.}
Let $P, I \subseteq \mathcal{I}$ be two itemsets: 
\begin{equation}
P\not\sqsubseteq I \implies P\not\preceq I \label{eq:non_incl_relation}
\end{equation}
\end{lemma}


\begin{table}[tb]
\small\centering
\caption{Lists of supported sequences in $\mathcal{D}$ by negative patterns $\seq{p}_i$, $i=1..4$ under the total and partial non inclusion semantics. Each pattern has the shape $\langle a\ \neg q_i\ b\rangle$ where $q_i$ are itemsets such that $q_i \subset q_{i+1}$.}
\label{tab:partial-total}

%\renewcommand{\arraystretch}{1.3}
    \begin{tabular}{@{}lcc@{}}
        \toprule
        ~                               & partial & total \\ 
        ~                               & non inclusion & non inclusion \\
        ~ & $\not\preceq$ & $\not\sqsubseteq$\\
        \midrule
        $\seq{p}_1 = \langle b\ \neg c\ a \rangle$      & $\{\seq{s}_1, \seq{s}_3, \seq{s}_4\}$           & $\{\seq{s}_1, \seq{s}_3, \seq{s}_4\}$ \\ 
        $\seq{p}_2 = \langle b\ \neg (cd)\ a \rangle$   & $\{\seq{s}_1, \seq{s}_2, \seq{s}_3, \seq{s}_4\}$ & $\{\seq{s}_1, \seq{s}_4\}$ \\ 
        $\seq{p}_3 = \langle b\ \neg (cde)\ a \rangle$  & $\{\seq{s}_1, \seq{s}_2, \seq{s}_3, \seq{s}_4\}$   & $\{\seq{s}_1\}$ \\ 
        $\seq{p}_4 = \langle b\ \neg (cdeg)\ a \rangle$ &$\{\seq{s}_1, \seq{s}_2, \seq{s}_3, \seq{s}_4,\seq{s}_5\}$ & $\{\seq{s}_1\}$ \\ %\midrule
%        ~                               & monotonic           & anti monotonic        \\
        \bottomrule
    \end{tabular}
\end{table}


In the sequel we will denote the general form of itemset non inclusion by the symbol $\nsubseteq$,  meaning either $\not\preceq$ or $\not\sqsubseteq$.


Now, we formulate the notions of sub-sequence, non inclusion and absence by means of the concept of embedding.

\begin{definition}[Positive pattern embedding]\label{def:positivepattern_embedding}
Let $\seq{s}=\langle s_1\,\dots\, s_n\rangle$ be a sequence and $\seq{p}=\langle p_1\,\dots\, p_m\rangle$ be a (positive) sequential pattern.
$\seq{e}=(e_i)_{i\in[m]}\in [n]^m$ is an \emph{embedding} of pattern $\seq{p}$ in sequence $\seq{s}$ iff $\forall i\in[m],\; p_i \subseteq s_{e_i}$ and $\forall i\in[m-1],\; e_{i}<e_{i+1}$
\end{definition}

\begin{definition}[Strict and soft embeddings of negative patterns]\label{def:NSP_embedding}
Let $\seq{s}=\langle s_1\,\dots\, s_n\rangle$ be a sequence and $\seq{p}=\langle p_1\ \neg q_1\ \dots\ \ \neg q_{m-1}\ p_m\rangle$ be a negative sequential pattern.

$\seq{e}=(e_i)_{i\in[m]}\in [n]^m$ is a \textbf{soft-embedding} of pattern $\seq{p}$ in sequence $\seq{s}$ iff:
\begin{itemize}
\item $p_i \subseteq s_{e_i},\, \forall i\in[m]$
\item $q_i \nsubseteq s_j,\;\forall j\in [e_{i}+1,e_{i+1}-1]$ for all $i\in[m-1]$
\end{itemize}

$\seq{e}=(e_i)_{i\in[m]}\in [n]^m$ is a \textbf{strict-embedding} of pattern $\seq{p}$ in sequence $\seq{s}$ iff:
\begin{itemize}
\item $p_i \subseteq s_{e_i},\, \forall i\in[m]$
\item $q_i \nsubseteq \bigcup_{j\in [e_{i}+1,e_{i+1}-1]} s_j$ for all $i\in[m-1]$
\end{itemize}
\end{definition}

\begin{notation}
\emph{soft}-embedding is denoted $\circ$-embedding, and \emph{strict}-embedding is denoted $\bullet$-embedding.
\end{notation}

\begin{example}[Itemset absence semantics]\label{ex:itemsetsemantic}
Let $\seq{p}=\langle a\ \neg (bc)\ d\rangle$ be a pattern and four sequences:
\begin{center}
\centering
%\renewcommand{\arraystretch}{1.3}
\begin{tabular}{@{}lcccc@{}}
\toprule
Sequence & $\not\sqsubseteq$ & $\not\sqsubseteq$ & $\npreceq$ & $\npreceq$\\
~ &$\bullet$ & $\circ$ & $\bullet$ & $\circ$
 \\ \midrule
$\seq{s_1}=\langle a\ c\ b\ e\ d \rangle$ & & & &\ding{51}\\
$\seq{s_2}=\langle a\ (bc)\ e\ d \rangle$ & & & &\\
$\seq{s_3}=\langle a\ b\ e\ d \rangle$ & & &\ding{51} &\ding{51}\\
$\seq{s_4}=\langle a\ e\ d \rangle$ &\ding{51}&\ding{51}&\ding{51} &\ding{51}\\
\bottomrule
\end{tabular}
\end{center}
 One can notice that each sequence contains a unique occurrence of $\langle a\ d \rangle$, the positive part of pattern $\seq{p}$. 
Using soft-embeddings and total non inclusion ($\nsubseteq\myeq\not\sqsubseteq$), $\seq{p}$ occurs in $\seq{s_1}$, $\seq{s_3}$ and $\seq{s_4}$ but not in $\seq{s_2}$. 
Using the strict-embedding semantics and partial non-inclusion, $\seq{p}$ occurs in sequence $\seq{s_3}$ and  $\seq{s_4}$ considering that items $b$ and $c$ occur between occurrences of $a$ and $d$ in sequences $1$ and $2$. 
%
With partial non inclusion ($\nsubseteq\myeq\not\preceq$) and either type of embeddings, the absence of an itemset is satisfied if any of its item is absent. As a consequence,  $\seq{p}$ occurs only in sequence $\seq{s_4}$. 
\end{example}

\begin{lemma}\label{lemma:bulletimpliescirc}
If $\seq{e}$ is a $\bullet$-embedding, then $\seq{e}$ is a $\circ$-embedding, whatever is the itemset non-inclusion ($\not\subseteq$).
\end{lemma}

\begin{lemma}\label{prop:sqsubset_eqembeddings}
$\seq{e}$ is a $\circ$-embedding iff $\seq{e}$ is a $\bullet$-embedding when $\nsubseteq\myeq\not\sqsubseteq$.
\end{lemma}

\begin{lemma}\label{lemma:soft_implies_strict}
Let $p=\langle p_1\ \neg q_1\ \dots\ \neg q_{n-1}\ p_n \rangle \in \mathcal{N}$ s.t. $|q_i|\leq 1$ for all $i\in[n-1]$, then
$\seq{e}$ is a $\circ$-embedding iff $\seq{e}$ is a $\bullet$-embedding.
\end{lemma}

Lemma \ref{lemma:soft_implies_strict} shows that in the simple case of patterns with negative singleton only, strict and soft-embeddings are equivalent.

\begin{lemma}\label{lemma:pos_embedding}
Let $p=\langle p_1\ \neg q_1\ \dots\ \neg q_{n-1}\ p_n \rangle \in \mathcal{N}$,
if $\seq{e}$ is an embedding of pattern $\seq{p}$ in some sequence $\seq{s}$, then $\seq{e}$ is an embedding of the positive sequential pattern $\seq{p}^+$ in $\seq{s}$.
\end{lemma}

Example \ref{ex:itemsetsemantic} illustrates the impact of itemset non-inclusion operator and of embedding type.

Another point that determines the semantics of negative containment concerns the multiple occurrences of some pattern in a sequence: should all or at least one occurrence(s) of the pattern positive part in the sequence satisfy the non inclusion constraints? 

\begin{definition}[Negative pattern occurrence] \label{def:neg_occurrence}
Let $\seq{s}$ be a sequence, $\seq{p}$ be a negative sequential pattern, and $\seq{p}^+$ the positive part of $\seq{p}$.
Let $\not\subseteq\in\{\not\sqsubseteq,\not\preceq\}$ be a itemset non-inclusion operator, and $\cdot\in\{\circ,\bullet\}$ correspond to the embedding strategy ($\circ$: soft-embedding, and $\bullet$: strict-embedding).
\begin{itemize}
\item Pattern $\seq{p}$ \emph{softly-occurs} in sequence $\seq{s}$, denoted $\seq{p} \preceq^{\not\subseteq}_{\cdot} \seq{s}$, iff there exists at least one embedding of $\seq{p}$ in $\seq{s}$.
\item Pattern $\seq{p}$ \emph{strictly-occurs} in sequence $\seq{s}$, denoted $\seq{p} \sqsubseteq^{\not\subseteq}_{\cdot} \seq{s}$, iff for each embedding $\seq{e}$ of $\seq{p}^+$ in $\seq{s}$, $\seq{e}$ is also an embedding of $\seq{p}$ in $\seq{s}$, and there exists at least one embedding $\seq{e}$ of $\seq{p}^+$.
\end{itemize}
\end{definition}

Definition \ref{def:neg_occurrence} allows for capturing two semantics for negative sequential patterns depending on the occurrences of the positive part:
\begin{itemize}
\item \emph{strict occurrence}: a negative pattern $\seq{p}$ occurs in a sequence $\seq{s}$ iff there exists at least one occurrence of the positive part of pattern $\seq{p}$ in sequence $\seq{s}$ and \textbf{every} such occurrence satisfies the negative constraints,
\item \emph{soft occurrence}: a negative pattern $\seq{p}$ occurs in a sequence $\seq{s}$ iff there exists at least one occurrence of the positive part of pattern $\seq{p}$ in sequence $\seq{s}$ and \textbf{at least one} of these occurrences satisfies the negative constraints.
\end{itemize}

\begin{example}[Strict vs soft occurrence semantics]
Let $\seq{p}=\langle a\ b\ \neg c\  d\  \rangle$ be a pattern,  $\seq{s_1}=\langle a\ b\ e\ d \rangle$ and $\seq{s_2}=\langle a\ b\ c\ a\ d\ e\ b\ d \rangle$ be two sequences. The positive part of $\seq{p}$ is $\seq{p}^+=\langle a\ b\ d \rangle$. It occurs once in $\seq{s_1}$ so there is no difference for occurrences under the two semantics. But, it occurs fourth in $\seq{s_2}$  with embeddings $(1,2,5)$, $(1,2,8)$, $(1,7,8)$ and $(4,7,8)$. The two first occurrences do not satisfy the negative constraint ($\neg c$) while the two last occurrences do.
Under the soft occurrence semantics, pattern $\seq{p}$ occurs in sequence $\seq{s_2}$ whereas under the strict occurrence semantics it does not.
\end{example}

\begin{lemma}
\label{lemma:strictocc_implies_softocc}
Let $\seq{p}$ be a NSP and $\seq{s}$ a sequence, 
\begin{equation}
\seq{p}\sqsubseteq_{\cdot}^{\not\subseteq}\seq{s} \implies \seq{p}\preceq_{\cdot}^{\not\subseteq}\seq{s}
\end{equation}
where $\cdot\in\{\circ,\bullet\}$ and $\not\subseteq\in\{\not\preceq,\not\sqsubseteq\}$.
\end{lemma}


\begin{lemma}\label{lemma:notstrict_implies_notsoft}
Let $\seq{p}$ be a NSP and $\seq{s}$ a sequence, 
\begin{equation}
\seq{p}\leq_{\cdot}^{\not\sqsubseteq}\seq{s} \implies \seq{p}\leq_{\cdot}^{\not\preceq}\seq{s}\end{equation}
where $\leq\in\{\preceq,\sqsubseteq\}$ and $\cdot\in\{\circ,\bullet\}$
\end{lemma}

\espace

In this section, we shown that there are several semantics associated to negative patterns. This leads to eight different types of pattern occurrences.%\footnote{It is worth noticing that specifying the semantic of occurrences at the pattern level and not at the itemset level forbid to mix different types of negations in a pattern.} 
We denote $\Theta$ the set of considered pattern occurrence operators:
$$\Theta = \left\{
\preceq^{\not\sqsubseteq}_{\circ}, \preceq^{\not\sqsubseteq}_{\bullet},
\preceq^{\not\preceq}_{\circ}, \preceq^{\not\preceq}_{\bullet},
\sqsubseteq^{\not\sqsubseteq}_{\circ}, \sqsubseteq^{\not\sqsubseteq}_{\bullet},
\sqsubseteq^{\not\preceq}_{\circ}, \sqsubseteq^{\not\preceq}_{\bullet} 
\right\}
$$

These operators allows to disambiguate the semantics of negative pattern containment. 
But, is there no useless distinctions between containment relations? Is there some equivalent containment relations in $\Theta$. 
The next section answers this question by introducing the notion of dominance between semantics.
Then, we provide some results about the anti-mononicity of these containment relations.