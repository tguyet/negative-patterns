\section{Proofs and additional propositions}

Proof of Proposition \ref{prop:sqsubset_eqembeddings}.

\begin{proof}
Let $\seq{s}=\langle s_1,\dots, s_n\rangle$ be a sequence and $\seq{p}=\langle p_1,\dots, p_m\rangle$ be a negative sequential pattern.
Let $\seq{e}=(e_i)_{i\in[m]}\in [n]^m$ be a soft-embedding of pattern $\seq{p}$ in sequence $\seq{s}$. Then, the definition matches the one for strict-embedding if $p_i$ is positive. If $p_i$ is negative then $\forall j\in [e_{i-1}+1,e_{i+1}-1],\; p_i \not\sqsubseteq s_j$, \ie $\forall j\in [e_{i-1}+1,e_{i+1}-1],\; \forall \alpha\in p_i,\; \alpha \notin s_j$ and then $\forall \alpha\in p_i,\; \forall j\in [e_{i-1}+1,e_{i+1}-1],\; \alpha \notin s_j$.
It thus implies that $\forall \alpha\in p_i,\; \alpha \notin \bigcup_{j\in [e_{i-1}+1,e_{i+1}-1]} s_j$, \ie by definition, $p_i \not\sqsubseteq \bigcup_{j\in [e_{i-1}+1,e_{i+1}-1]} s_j$.

The exact same reasoning is done in reverse way to prove the equivalence.
\end{proof}


\begin{proposition}[]\label{prop:soft_implies_strict}
Soft-embedding $\implies$ strict-embedding for patterns consisting of items.
\end{proposition}

\begin{proof}
Let $\seq{s}=\langle s_1,\dots,s_n\rangle$ be a sequence and $\seq{p}=\langle p_1,\dots,p_m\rangle$ be a NSP s.t. each $\forall i,\; |p_i|=1$ and $\seq{p}$ occurs in $\seq{s}$ according to the soft-embedding semantic.

There exists $\epsilon =(e_i)_{i\in[m]}\in[n]^m$ s.t. for all $i\in[n]$,  $p_i$ is positive implies $p_i\in s_{e_i}$ and $p_i$ is negative implies that for all $j\in [e_{i-1}+1,e_{e+1}-1],\; p_i\notin s_j$ (items only) then $p_i\notin \bigcup_{j\in [e_{i-1}+1,e_{e+1}-1]}{s_j}$ \ie $p_i \not\subseteq \bigcup_{j\in [e_{i-1}+1,e_{e+1}-1]}{s_j}$ (no matter $\not\preceq$ or $\not\sqsubset$). As a consequence $\epsilon$ is a strict-embedding of $p$.
\end{proof}


\begin{proposition}\label{prop:eNSP_implies_CeNSP}
Let $\mathcal{D}$ be a dataset of sequences of items and $\seq{p}=\langle p_1,\dots,p_m\rangle$ be a sequential pattern extracted by eNSP, then without embedding constraints $\seq{p}$ is extracted by \CeNSP\ with the same minimum support.
\end{proposition}

\begin{proof} 
If $\seq{p}$ is extracted by eNSP, it implies that its positive partner is frequent in the dataset $\mathcal{D}$. As a consequence, each $p_i$, $i\in[m]$ is a singleton itemset.

According to the search space of \CeNSP\ defined by $\lhd$\footnote{$\lhd$ or $\blacktriangleleft$ ... no matter with sequences of items for which $\not\preceq$ and $\not\sqsubseteq$ are equivalent.} is $\seq{p}$ is frequent then it will be reached by the depth-first search.
Then it is sufficient to prove that for any sequence $\seq{s}=\langle s_1,\dots,s_n\rangle \in \mathcal{D}$ such that $\seq{p}$ occurs in $\seq{s}$ according to eNSP semantic (strict-embedding, strong absence), then $\seq{p}$ also occurs in $\seq{s}$ according to the \CeNSP\ semantics (soft-embedding, weak absence). With that and considering the same minimum support threshold, $\seq{p}$ is frequent according to \CeNSP.
Proposition \ref{prop:soft_implies_strict} gives this result.
\end{proof}

\begin{example}[Pattern set comparisons]
\CeNSP\ extracts more patterns than eNSP on sequences of items. In fact, \CeNSP\ can extract patterns with negative itemsets larger than 2.

eNSP extract patterns that are not extracted by \CeNSP\ on sequences of itemsets.
Practically, \CeNSP\ uses a size limit for negative itemsets $\nu\geq 1$.
eNSP extracts patterns whose positive partners are frequent. The positive partner, extracted by PrefixSpan may hold itemsets  larger than $\nu$, and if the pattern with negated itemset is also frequent, then this pattern will be extract by eNSP, but not by \CeNSP.
\end{example}


\begin{proposition}[Anti-monotonicity of NSP]
The support of NSP is anti-monotonic with respect to $\lhd$ (resp. $\blacktriangleleft$) when $\nsubseteq\myeq\not\sqsubseteq$ (resp. $\nsubseteq\myeq\not\preceq$) is considered.
\end{proposition}

\begin{proof}
Proof of anti-monotonicity of the support wrt $\lhd$, considering $\nsubseteq\myeq\not\sqsubseteq$.

Let $\seq{p}=\langle p_1,\dots, p_n\rangle$ and $\seq{q}=\langle q_1,\dots,q_n\rangle$ be two NSP. Let $|\seq{p}|=\sum_{i}|p_i|$ denote the total number of items in pattern $\seq{p}$, and we introduce $\lhd_1$ a order relation between immediate ``extensions'': $\seq{p}\lhd_1\seq{q} \Leftrightarrow \seq{p}\lhd\seq{q} \wedge |\seq{p}|=|\seq{q}|+1$.
We can easily prove recursively that $\seq{p}\lhd\seq{q} \Leftrightarrow \exists \{\seq{\rho}_i\}_{i\in [r]}$, a sequence of patterns\footnote{This sequence is not unique.} $\rho_0=p,\; \rho_r=q,\; \forall j, \rho_j \lhd_1 \rho_{j+1}$. Then, it is sufficient to prove that the support is anti-monotonic for relation $\lhd_1$.
 
By definition of $\lhd$ and $\lhd_1$, we have that:
$\seq{p}\lhd_1\seq{q}$ iff $m=n$, $\forall i\in [n],\;p_i$ negative $\Leftrightarrow q_i$ negative, $\exists! k\in [n],\; q_k = p_k \cup \{e\} $ where $e\in\mathcal{I}$ and $\forall i\neq k,\; q_i = p_i$. Note that this definition considers that if $q_k$ is a singleton, then $p_k$ is an empty itemset (with same sign as $q_k$) that has been inserted to have the same lengths.

Let $\seq{p}$  and $\seq{q}$ s.t. $\seq{p}\lhd_1\seq{q}$, then we have to show that for any sequence $\seq{s}\in\mathcal{D}$, $\seq{q}$ occurs in $\seq{s}$ implies that $\seq{p}$ occurs in $\seq{s}$.

Let assume that $\seq{q}$ occurs in $\seq{s}$ considering $\nsubseteq\myeq\not\sqsubseteq$ (and thus no matter the embedding strategy, according to proposition \ref{prop:sqsubset_eqembeddings}, we use strict-embedding in the following).
Then, for all embedding $\epsilon=(e_i)_i$, $q_i \subseteq s_{e_i}$ if $q_i$ is positive and $\forall j\in [e_{i-1}+1,e_{i+1}-1], \; q_i \nsubseteq s_j$ if $q_i$ is negative.
Then for all $i\neq k$, we have immediately that $p_i \subseteq s_{e_i}$ if $p_i$ is positive and $\forall j\in [e_{i-1}+1,e_{i+1}-1], \; p_i \nsubseteq s_j$ if $p_i$ is negative.
The remaining case is for the $k$-th itemset of $\seq{q}$.
If $q_k$ is positive, then $p_k \subseteq q_k \subseteq s_{e_i}$ and thus $\epsilon$ is an embedding of $\seq{p}$ in $\seq{s}$.
If $q_k$ is negative, $\forall j\in [e_{k-1}+1,e_{k+1}-1],\; q_k \nsubseteq s_j$ \ie $\forall j\in [e_{k-1}+1,e_{k+1}-1],\; \forall e \in q_k,\; e \notin s_j$. Then $\forall e \in q_k,\;\forall j\in [e_{k-1}+1,e_{k+1}-1],\; e \notin s_j$ and thus  $\forall e \in p_k,\;\forall j\in [e_{k-1}+1,e_{k+1}-1],\; e \notin s_j$ because $p_k \subset q_k$\footnote{Here, we also have that $\emptyset \subset q_k$ in case of singleton itemset $q_k$. Thus justify the use of the simplified version of $\lhd_1$.}, \ie in short $\forall j\in [e_{k-1}+1,e_{k+1}-1],\; p_k \nsubseteq s_j$.

Note any embedding of $\seq{q}$ yields an embedding for $\seq{p}$, then the property holds for weak and strong absence semantics.

\espace

Proof of anti-monotonicity of the support wrt $\blacktriangleleft$, considering $\nsubseteq\myeq\not\preceq$. 
Similary to $\lhd_1$, we define $\blacktriangleleft_1$ such that $\seq{p}\blacktriangleleft_1\seq{q}$ iff $|\seq{p}|=|\seq{q}|$\footnote{again, we allow empty sets.} there exists $k\in[n]$ s.t. $\forall i\neq k$, $p_i=q_i$ and if $q_k$ is negative, $q_k\subset p_k $ otherwise $p_k\subset q_k $.
The exact same reasoning can be done except for the last case of the negative $k$-th itemset for $\seq{q}$. We have here to distinguish strict and soft embeddings.
If $q_k$ is negative and considering strict-embeddings, $\forall j\in [e_{k-1}+1,e_{k+1}-1],\; q_k \nsubseteq s_j$ \ie $\forall j\in [e_{k-1}+1,e_{k+1}-1],\; \exists e \in q_k,\; e \notin s_j$ when using definition of $\not\preceq$ (see Definition \ref{def:IS_notincluded}).
Considering definition of $\blacktriangleleft_1$, we have that $q_k \subset p_k$ s.t. $e$ is also an element of $p_k$ and then $\forall j\in [e_{k-1}+1,e_{k+1}-1],\; p_k \nsubseteq s_j$

If $q_k$ is negative and considering soft-embeddings, $q_k \nsubseteq \bigcup_{j\in [e_{k-1}+1,e_{k+1}-1]} s_j$, \ie $\exists e \in q_k,\; e \notin \bigcup_{j\in [e_{k-1}+1,e_{k+1}-1]} s_j$. Again, we have $q_k \subset p_k$ and then $e$ is also an element of $p_k$ s.t. $p_k \nsubseteq \bigcup_{j\in [e_{k-1}+1,e_{k+1}-1]} s_j$.
\end{proof}

