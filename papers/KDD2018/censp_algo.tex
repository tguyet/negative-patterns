\section{Algorithm \CeNSP} \label{sec:censp}

\CeNSP~ extracts NSPs under maxgap and maxspan constraints and under a weak absence semantics with $\nsubseteq\myeq\not\sqsubset$ for itemset inclusion. As stated in proposition \ref{prop:sqsubset_eqembeddings}, no matter the embedding strategy, they are equivalent with this strict itemset inclusion. 
%The itemset non-inclusion operator can be modified and is $\not\sqsubset$ by default. 
$\mathcal{L}^-$ is the set of itemsets that can be built from frequent items.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{algorithm}[bp]
\footnotesize

\LinesNumbered

\SetKwInOut{Input}{input}
\SetKwData{break}{break}
\SetKwComment{Comment}{//}{}

\SetKwFunction{FRec}{\CeNSP}
\SetKwFunction{match}{Match}
\SetKwFunction{output}{OutputPattern}
\SetKwProg{Fn}{Function}{:}{}
\SetKwFunction{PositiveComposition}{PositiveComposition}
\SetKwFunction{PositiveSequence}{PositiveSequence}
\SetKwFunction{NegativeSequence}{NegativeExtension}
\SetKwFunction{NegativeComposition}{NegativeComposition}

\Input{$\mathcal{S}$: set of sequences, $p$: current pattern, $\sigma$: minimum support threshold,  $occs$: list of occurrences, $\mathcal{I}^f$: set of frequent items}
	\BlankLine
    \Fn{\FRec{$\mathcal{S}$, $\sigma$, $p$, $occs$, $\mathcal{I}^f$}}{
		\Comment{Support evaluation of pattern $p$}
        \If{$|occs|\geq \sigma$} {
        	\output{$p$, $occs$}\;
        }\Else {
        	\KwRet\;
        }
        
        \Comment{Positive itemset composition}
        \PositiveComposition{$\mathcal{S}$, $\sigma$, $p$, $occs$, $\mathcal{I}^f$}\;
        
        \Comment{Positive sequential extension}
        \PositiveSequence{$\mathcal{S}$, $\sigma$, $p$, $occs$, $\mathcal{I}^f$}\;

%        \If{$|p| \geq 2 \wedge [-2]$ is pos} {
%        	\Comment{Negative extension}
%        	\NegativeSequence{$\mathcal{S}$, $\sigma$, $p$, $occs$, $\mathcal{I}^f$}\;
%        }
        
        \If{$|p| \geq 2$} {
        	\Comment{Negative sequential extension}
			\NegativeSequence{$\mathcal{S}$, $\sigma$, $p$, $occs$, $\mathcal{I}^f$}\;
        }
  	}  	
\caption{\CeNSP: recursive function for negative sequential pattern extraction}
\label{algo:CeNSP-Rec}
\end{algorithm}

\subsection{Main algorithm}
\CeNSP~ is based on algorithm PrefixSpan \cite{pei2004mining:prefixspan} which implements a depth first search and uses the principle of database projection to reduce the number of dataset sequence scan. 
\CeNSP~ adapts the pseudo-projection principle of PrefixSpan which uses a projection pointer to avoid copying the data.
For \CeNSP, a projection pointer of some pattern $p$ is a triple $\langle sid, ppred, pos \rangle$ where 
%\begin{itemize}
%\item 
$sid$ is a sequence identifier in the database, 
%\item 
$pos$ is the position in sequence $sid$ which matches the last itemset of the pattern (necessarily positive)
%\item 
and $ppred$ is the position of the previous positive pattern.
%\end{itemize}

Algorithm \ref{algo:CeNSP-Rec} details the main \CeNSP~ recursive function for extending a current pattern $p$. The principle of this function is similar to PrefixSpan. Every pattern $p$ is associated with a pseudo-projected database represented by both the original set of sequences $\mathcal{S}$ and a set of projection pointers $occs$.
First, the function evaluates the size of $occs$ to determine whether pattern $p$ is frequent or not. If so, it is outputted, otherwise, the recursion is stopped because no larger patterns are possible (anti-monotonicity property).\\
Then, the function tries three types of pattern extensions:
\begin{itemize}
\item the positive composition consists in adding  one item to the last itemset of $p$
\item the positive sequence extension consists in adding a new singleton itemset at the end of  $p$
\item the negative sequence extension consists in inserting negative items between the penultimate and the ultimate positive itemsets of $p$. According to the chosen NSP syntactic restrictions, this extension is possible only when the pattern length is at least 2.
\end{itemize}

The negative pattern extension is specific to our algorithm and is detailed in next section.
The first two extensions are identical to PrefixSpan, including their gap constraints management, \ie maxgap and maxspan between positive patterns.
For the sake of space limitation, positive extensions are not detailed in the sequel.

The proposed algorithm is correct and complete. The proposed algorithm is consistent with order $\triangleleft$ on NSP and according to proposition \ref{prop:antimonotonic}, the support of NSP is antimonotonic.
More specifically, the negative composition by an item is anti-monotonic under the strict-embedding semantics \footnote{A relatively similar algorithm extracts NSP with occurrences strategies based on $\not\preceq$ operator. Instead of recursively extending negative patterns, all candidate itemsets are computed and evaluated without recursions.}. 

\subsection{Extension of patterns with negated itemsets}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{algorithm}[tbp]
\footnotesize
\LinesNumbered

\SetKwData{break}{break}
\SetKwData{continue}{continue}
\SetKwComment{Comment}{//}{}

\SetKwFunction{FRec}{\CeNSP}
\SetKwFunction{Fonction}{NegativeExtension}
\SetKwFunction{match}{Match}
\SetKwProg{Fn}{Function}{:}{}

    \Fn{\Fonction{$\mathcal{S}$, $\sigma$, $p$, $occs$, $\mathcal{I}^f$}}{
        	
        \For{$it \in \mathcal{I}^f$}{
        	
        	\If{$p[-2]$ is pos} {
        		$p.insert(\neg it)$\; \Comment{Insert the negative item at the penultimate position}
        	} \Else {
        		\If{$it>p[-2].back()$} {
        			$p[-2].insert(\neg it)$\; \Comment{Insert an item to the penultimate (negative) itemset}
        		} \Else {
        			\continue\;
        		}
        	}
        		$newoccs \gets \emptyset$\;
        		\For{$occ \in occs$}{
        			$found \gets false$\;
   		     		\For{$ sp = [occ.pred+1,occ.pos-1]$} {
      	  				\If{$it \in s_{occ.sid}[sp]$}{
        					$found \gets true$\;
        					\break\;
        				}
        			}
        			\If{!$found$} {
        				$newoccs \gets newoccs \cup \{occ\}$\;
        			} \Else {
        				\Comment{Look for an alternative occurrence}
        				$newoccs \gets newoccs \cup $ \match{$s_{sid}$, $p$}\;
        			}
        		}
        		
        		\FRec{$\mathcal{D}$, $\sigma$, $p$, $newoccs$, $\mathcal{I}^f$}\;
        		$p[-2].pop()$\;
       	 	}
    }
\caption{\CeNSP: negative extensions}
\label{algo:NegExt}
\end{algorithm}

Algorithm \ref{algo:NegExt} extends pattern $p$ with negative items. It generates new candidates by inserting an item $it \in \mathcal{I}^f$, the set of frequent items. If $p[-2]$, the penultimate itemset, is positive, then a new negated itemset is inserted between $p[-2]$ and $p[-1]$. Otherwise, item $it$ is added to $p[-2]$. To prevent redondant enumeration of negative itemsets, only items $it$ (lexicographically) greater than the last item of $p[-2]$ can be added.

Then, lines ? to ?, evaluate the candidate by computing the pseudo-projection of the current database. According to the selected semantics associated with $\not\sqsubseteq$ (see Definition \ref{def:NSP_embedding}), it is sufficient to check the absence of $it$ in the subsequence incuded between occurrences of positive itemsets surounding $it$. 
To achieve this, the algorithm checks the sequence positions in the interval $[occ.ppred+1, occ.pos-1]$.
If $it$ does not appear in this interval, then the extended pattern occurs in the sequence $occ.sid$.
Otherwise, the pattern has to be matched entirely to ensure the completeness of the algorithm.

For example, the occurrence of the pattern $\seq{p}=\langle abc\rangle$ in sequence $\langle abecabc \rangle$ is $occ_p=\langle sid,2,4\rangle$. Let's now consider $\seq{p}'=\langle ab\neg ec\rangle$, a negative extension of $p$. The extension of the projection-pointer $occ_p$ does not satisfy the absence of $e$, but $\langle sid,6,7\rangle$ does.

The pattern extended with a negative itemset is then given to the recursive function \CeNSP~ for a further extension.
