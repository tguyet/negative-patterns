#!/bin/bash

f=2
m=3

echo "partial non-inclusion"
../../ensp/prefixspan -cneg -f $f -m $m -miss 4 -softinc noninclusion.dat | grep "(2),-(3),(1)"
../../ensp/prefixspan -cneg -f $f -m $m -miss 4 -softinc noninclusion.dat | grep "(2),-(3,4),(1)"
../../ensp/prefixspan -cneg -f $f -m $m -miss 4 -softinc noninclusion.dat | grep "(2),-(3,4,5),(1)"
../../ensp/prefixspan -cneg -f $f -m $m -miss 4 -softinc noninclusion.dat | grep "(2),-(3,4,5,6),(1)"

echo "total non-inclusion"
../../ensp/prefixspan -cneg -f $f -m $m -miss 4 noninclusion.dat | grep "(2),-(3),(1)"
../../ensp/prefixspan -cneg -f $f -m $m -miss 4 noninclusion.dat | grep "(2),-(3,4),(1)"
../../ensp/prefixspan -cneg -f $f -m $m -miss 4 noninclusion.dat | grep "(2),-(3,4,5),(1)"
../../ensp/prefixspan -cneg -f $f -m $m -miss 4 noninclusion.dat | grep "(2),-(3,4,5,6),(1)"


