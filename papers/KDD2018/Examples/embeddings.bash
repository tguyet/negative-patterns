#!/bin/bash

f=2
m=3

echo "partial non-inclusion/weak-absence"
../../ensp/prefixspan -cneg -f $f -m $m -softinc -soft -miss 2 embeddings.dat | grep "(1),-(2,3),(4)"
echo "partial non-inclusion/strong-absence"
../../ensp/prefixspan -cneg -f $f -m $m -softinc -miss 2 embeddings.dat | grep "(1),-(2,3),(4)"
echo "total non-inclusion"
../../ensp/prefixspan -cneg -f $f -m $m -miss 2 embeddings.dat | grep "(1),-(2,3),(4)"

#../../ensp/prefixspan -cneg -f 2 -m 3 -softinc -soft -miss 2 embeddings.dat | grep "(1),-(2,3),(4)"
