/**
 Sequences implementation

 Author: Thomas Guyet <thomas.guyet@agrocampus-ouest.fr>

 Date (last update): 01/2018
 
 License: GPL2 (Gnu General Public License Version 2)
*/

#include "Sequence.h"
#include <algorithm>

ostream& operator<<(ostream &os, const itemset &is) {
    itemset::const_iterator it=is.begin();
    if( it!=is.end() ) {
        os << *it;
        it++;
    }
    while(it!=is.end()) {
        os << ","<<*it;
        it++;
    }
    return os;
}

ostream& operator<<(ostream &os, const sequence &s) {

    vector<itemset>::const_iterator it=s.begin();
#if TIMESTAMPS
    vector<float>::const_iterator itt=s.timestamps.begin();
#endif
    os << "<";
    if( it!=s.end() ) {
        os << "("<<*it<<")";
#if TIMESTAMPS
        os << "["<<*itt<<"]";
        itt++;
#endif
        it++;
    }
    while(it!=s.end()) {
        os << ", ("<<*it<<")";
#if TIMESTAMPS
        os << "["<<*itt<<"]";
        itt++;
#endif
        it++;
    }
    os << ">";
    return os;
}


/**
* lexicographic order, the first difference -- of items -- 
*/
bool operator<(const sequence &s1, const sequence &s2) {
    sequence::const_iterator it1=s1.begin();
    sequence::const_iterator it2=s2.begin();
    while( it1!=s1.end() && it2!=s2.end() ) {
        if( *it1 > *it2 ) return 0;
        if( *it1 < *it2 ) return 1;
        it1++; it2++;
    }
    return s1.size()<s2.size(); //equality
}

/**
* \brief itemset inclusion operator.
* \pre the itemset are ordered list of items (without repetitions)
* \todo make a bitwise representation to speed up this tests !!
*/
bool inc(const itemset &is1, const itemset &is2) {
    if( is1.size() > is2.size() ) return false;
    
    itemset::const_iterator it1 = is1.begin();
    itemset::const_iterator it2 = is2.begin();
    while( it1!=is1.end() && it2!=is2.end() ) {
        if( *it1>*it2) { //considering ordered list of items, if this test is true, *it1 is no in the itemsets
            return false;
        } else if ( *it1==*it2 ) {
            it1++;
        }
        it2++;
    }
    if( it1==is1.end() ) {
        return true;
    }
    return false;
}

/**
* \brief itemset hold operator (test whether an item is included in the itemset).
* \pre the itemset are ordered list of items (without repetitions)
* \todo make a bitwise representation to speed up this tests !!
*/
bool inc(unsigned int i, const itemset &is) {
    itemset::const_iterator it = is.begin();
    while( it!=is.end() ) {
        if( *it>i) { //considering ordered list of items, if this test is true, *it1 is no in the itemsets
            return false;
        } else if ( i==*it ) {
            return true;
        }
        it++;
    }
    return false;
}

unsigned int itemset_noinc_mode =IS_NOINC_STRICT;
/**
* test whether itemset is1 is NOT included in the itemset is2
* 
* Here, depending on the \p itemset_noinc_mode value, not being included has two semantics
* <li> strict (0): none of the is1 items are in is2 (empty intersection)
* <li> soft (1): there exists an item e in is1 which is not in is2 (negation of \f inc operator)
* 
* \pre the itemset are ordered list of items (without repetitions)
*/
bool noinc(const itemset &is1, const itemset &is2) {
    if( itemset_noinc_mode == IS_NOINC_STRICT ) {
        itemset::const_iterator it1 = is1.begin();
        itemset::const_iterator it2 = is2.begin();
        while( it1!=is1.end() && it2!=is2.end() ) {
            if( *it1>*it2) { //considering ordered list of items, if this test is true, *it1 is no in the itemsets
                it2++; 
            } else if ( *it1==*it2 ) {
                return false; // it1 is in is2
            } else {
                it1++;
            }
        }
        return true;
    } else {
        return not inc(is1,is2);
    }
}

/**
* \brief itemset intersection
*/
itemset inter(const itemset &v1, const itemset &v2) {
    itemset v3;
    set_intersection(v1.begin(),v1.end(),v2.begin(),v2.end(),back_inserter(v3));
    return v3;
}


void merge(itemset &cumulis, const itemset &is) {
    cumulis.insert(cumulis.end(),is.begin(),is.end());
    std::sort (cumulis.begin(), cumulis.end());
    std::unique (cumulis.begin(), cumulis.end()); 
}

