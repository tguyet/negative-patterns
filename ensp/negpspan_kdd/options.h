/**
 Overall software options

 Author: Thomas Guyet <thomas.guyet@agrocampus-ouest.fr>

 Date (last update): 01/2018
 
 License: GPL2 (Gnu General Public License Version 2)
*/

#ifndef OPTIONS_H
#define OPTIONS_H

/**
OUTPUT_SHOW
- 0 : no output 
- 1 : output patterns
- >1: output patterns and tid list
*/
#define OUTPUT_SHOW 1

/**
- 1 : prefixspan extract the SID, set of sequences that support the pattern
        -> in case of activation, SID are computed only while list of patterns are outputed
- 0 : SID not computed !
*/
#define COMPUTE_SID 1

/**
\warning this option can be activated only while COMPUTE_SID is activated
- 1 : activate timestamp management
- 0: no timestamp management (less memory requirement)
*/
#define TIMESTAMPS  1


#endif
