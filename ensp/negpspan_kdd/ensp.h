/**
 eNSP --- An Implementation of e-NSP algorithm for negative pattern mining

 Author: Thomas Guyet <thomas.guyet@agrocampus-ouest.fr>

 Date (last update): 01/2018

 License: GPL2 (Gnu General Public License Version 2)

 Reference:
  Longbing Cao, Xiangjun Dong, Zhigang Zheng,
  e-NSP: Efficient negative sequential pattern mining,
  Artif. Intell. 235: 156-182 (2016)
*/


#ifndef ENSP_H
#define ENSP_H

#include "options.h"

#if COMPUTE_SID

#include "prefixspan.h"
#include "NegativePattern.h"

#include <vector>
#include <list>
#include <iostream>

using namespace std;

class eNSP {
  list< pair<sequence,unsigned int> > pplist;   //positive patterns list
  list< NegativePattern> negpats;               //list of negative patterns
  Prefixspan prefixspan;
  unsigned int _negative_support;
  ostream &_os;
  bool _include_positives;   // if true, it includes positive patterns

#if TIMESTAMPS
  //vector<vector<float> > projection; //! Description of the temporal projection of the current pattern
  const Callable *caller = NULL;
#endif

public:
  /**
  \param _min_sup minimal support of positive partners
  \param ns minimal support of the pattern
  \param _max_pat maximum length of the patterns, if 0 there is no restriction
  \param os output stream
  */
  eNSP(unsigned int _min_sup, unsigned int _max_pat, unsigned int ns, ostream &os=cout) : prefixspan(_min_sup, _max_pat, &pplist, os), _negative_support(ns),_os(os),_include_positives(true){};
  const list< NegativePattern>& patterns() const {return negpats;};
  
  unsigned int run(ProjDB &data);
  
  void include_positives(bool ip=true) {_include_positives=ip;};
  
#if TIMESTAMPS
  // callback setter
  void setCallback( const Callable *fp ) { caller=fp; };
#endif
};

#endif // COMPUTE_SID

#endif // DEFINE ENSP_H
