/**
 PrefiSpan --- An Implementation of Prefix-projected Sequential Pattern mining

 Author: Thomas Guyet <thomas.guyet@agrocampus-ouest.fr>

 Date (last update): 01/2018

 License: GPL2 (Gnu General Public License Version 2)

 Reference:
  PrefixSpan: Mining Sequential Patterns Efficiently by Prefix-Projected Pattern Growth
  Jian Pei, Jiawei Han, Behzad Mortazavi-asl, Helen Pinto, Qiming Chen, Umeshwar Dayal and Mei-chun Hsu
  IEEE Computer Society, 2001, pages 215
*/

#ifndef _PREFIXSPAN_H_
#define _PREFIXSPAN_H_

#include <iostream>
#include <ostream>
#include <vector>
#include <list>
#include <string>
#include <map>
#include <fstream>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <set>
#include <cfloat>

#include "Sequence.h"

typedef pair<unsigned int, sequence> Transaction; //<id <itemsets> > : WARNING: identifier have to be the position in the list of the sequences that are initialy mined!

class projection_pointer {
public:
    projection_pointer(unsigned int f, unsigned int s, unsigned int mp, float md =FLT_MAX):first(f), second(s), max_pos(mp), max_date(md) {};
    unsigned int first /*sed id*/, second /*itemset id*/;
    unsigned int max_pos;
//#if TIMESTAMPS
    float max_date;
//#endif
};

struct ProjDB {
  vector<const Transaction*> database;
  vector< projection_pointer > indexes; //projection indexes ! Pair => sequence_id, itemset_id
  void clear() {
    database.clear();
    indexes.clear();
  }
  
  map<string,unsigned int> dictionnary;
  map<unsigned int,string> revdictionnary;
  bool usedict;
};

void read_ibm(const string &_filename, ProjDB &pairdata);
void read(const string &_filename, ProjDB &pairdata);

#if TIMESTAMPS
class Callable {
public:
    void (*fp)(const Callable *, const GenericPattern &, const vector<vector<float> >&);
};
#endif


/**
Attention (TG):
  * l'algo n'est pas complet.
    - dans le cas de la BD suivante, il y a un gros pb (avec un seuil de deux) :
        (1) (1,3)
        (1) (1,3)
        -> la projection sur (1) conduit à positionner la projection uniquement sur les premiers 1
        -> il n'y a pas de composition possible à ces positions !! Donc, on ne retrouve pas (1,3) comme frequent !
    - pour améliorer un peu, on gere bien le cas suivant :
        (1) (1,3)
        (1,3)
        -> dans ce cas, (1,3) sera de frequence 2 ! mais ce au pris une complexité calculatoire supplémentaire !!
        
  * "closure" and "maximality" have to be considered as the "backward-closure" and "backward-maximality" for itemsets sequences !
     -> "closure" does not improve the efficiency of the algorithm ... it just outputs only the closed patterns (but compute all patterns) !
     
  * pattern contraints management
     -> max_pat: maximal length of the patterns
     -> max_gap/max_dur: maximal gap/duration between occurrences of successives itemsets of the pattern
*/
class Prefixspan {
  unsigned int min_sup;
  unsigned int max_pat;
  unsigned int max_gap; //maximal gap between occurrences of successives itemsets of the pattern (number of positions), if 0, there is no constraint
#if TIMESTAMPS
  float max_dur; //maximal duration between occurrences of successives itemsets of the pattern (timestamps)
#endif
  bool print_closed_only, print_maximal_only; //options de sortie (n'améliore pas les temps de traitement !)
  sequence pattern;
  list< pair<sequence,unsigned int> > *pfpatterns; //! Référence à une liste externe de motifs fréquents qui peut être premplie (retenu pour l'interfaçage Python)
  ostream &_os;
  
#if TIMESTAMPS
  vector<vector<float> > projection; //! Description of the temporal projection of the current pattern
  const Callable *caller = NULL;
#endif

public:
  Prefixspan(unsigned int _min_sup, unsigned int _max_pat, list< pair<sequence,unsigned int> > *patlist =NULL, ostream &os=cout) : min_sup(_min_sup), max_pat(_max_pat), max_gap(0), print_closed_only(false), print_maximal_only(false), pfpatterns(patlist), _os(os){};
  unsigned int run(ProjDB &projected);
  void output_pattern(const ProjDB &);
  
  void show_closed(bool c=true){print_closed_only=c;};
  void show_maximal(bool m=true){print_maximal_only=m;};
  
  void setMaxGap(unsigned int mg) {max_gap=mg;};
  
  #if TIMESTAMPS
  // callback setter
  void setCallback( const Callable *fp ) { caller=fp; };
  #endif
  
protected:
  unsigned int project(ProjDB &projected);
};

#endif // _PREFIXSPAN_H
