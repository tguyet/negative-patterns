#!/bin/bash


##### Patterns extraction
fmin=1200
mlength=3
mgap_negpspan=3
ratio_ensp=1
fpos=$(echo "scale=4;$fmin*$ratio_ensp/10" | bc)
# Extraction des motifs avec NegPSpan
CDM="./prefixspan -cneg -f $fmin -m $mlength -mg $mgap_negpspan -miss 2 db-pos.db"
echo $CDM
$CDM 2> /dev/null > output_negpspan.pat
# Extraction des motifs avec eNSP
./prefixspan -no -n $fmin -f $fpos -m $mlength db-pos.db 2> /dev/null > output_ensp.pat


###### Tests ######
echo "-------------------------------------------------"
pattern_shape="(383),-.*(383)"
echo "> extracted by eNSP:"
grep $pattern_shape output_ensp.pat
echo "> extracted by NegPSpan:"
grep $pattern_shape output_negpspan.pat
echo "-------------------------------------------------"

####### Results #######
#extracted by eNSP:
# (383),-(86,383),(383) [f=1579]      # <- NegPSpan choose to forbid negative extensions including surounding events
# (383),-(86),(383) [f=1251]          # <- shared
# (383),-(112),(383) [f=1610]         # <- missing due to maxgap constraints
# (383),-(114),(383) [f=1543]         # <- shared
# (383),-(115),(383) [f=1568]         # <- shared
# (383),-(151),(383) [f=1611]         # <- missing due to maxgap constraints
# (383),-(158),(383) [f=1605]         # <- missing due to maxgap constraints
#extracted by NegPSpan:
# pattern: (383),-(7),(383): 1243     # <- new one, cause (383),-(7),(383) is not frequent 
# pattern: (383),-(86),(383): 1226    # <- shared
# pattern: (383),-(114),(383): 1232   # <- shared
# pattern: (383),-(115),(383): 1236   # <- shared


echo "--------------- without maxgap constraints --------------------"
mgap_negpspan=0
CDM="./prefixspan -cneg -f $fmin -m $mlength -mg $mgap_negpspan -miss 1 db-pos.db"
echo $CDM
$CDM 2> /dev/null > output_negpspan.pat
echo "> extracted by NegPSpan:"
grep $pattern_shape output_negpspan.pat
echo "-------------------------------------------------"
