from distutils.core import setup, Extension

# define the extension module
prefixspan_module = Extension('prefixspan', sources=['prefixspan.cpp','ensp.cpp','prefixspan_module.cpp'])

# run the setup
setup(ext_modules=[prefixspan_module])
