#!/bin/bash

#Formatage du nom de la sortie avec des éléments de date et d'heure (unique !)
d=`date '+%d%m_%H%M%S'`
repo="results_$d"
mkdir $repo
outputfile="$repo/cmp.csv"
#outputfile="cmp.csv"

timeout="10m"

max_pat_len=5
max_is_size=1

#reinitialisation du fichier avec les entêtes des colonnes
echo "method len size ql it fp f mg time MaxRSS NbPat NbTempPat" > $outputfile

for len in 30
do
for n in 200
do
for ql in 40 #15 20 30 40 80 160
do
	# Iteration id
	nbit=10
	for it in `seq 1 1 $nbit`
	do
		db="$repo/db_${len}_${n}_${ql}_${it}.dat"
		# Generation d'un jeu de donnees (not with -neg option)
		python generation_sequences.py -l $len -d $ql -n $n -o $db

		#Run algorithm with different parameters on the same dataset
		for f in 0.10 #0.3 0.25 0.2 0.17 0.14 0.11 0.08 0.05 #0.2 0.15 0.1
		do
		    #get the minimal threshold as integer numbers
		    val=$(echo "$n*$f" | bc)
		    min_supp=${val%.*}

		    echo "STEP len=" $len " q=" $ql " it=" $it " f=" $f

		    ### TeNSP with different values for frequency in positives ###
		    for fp in 8 # 10 8 5 2 1
		    do
		        lfp=$(echo "scale=4;$fp*$f/10" | bc) #compute the positive threshold as 80% of the negative threshold while fp=8
			    val=$(echo "$n*$lfp" | bc)
			    min_pos_supp=${val%.*}

			    echo -n "tensp $len $n $ql $it $fp $f 0 " >> $outputfile

			    #run command
			    # -> l'option -no est utilisée ici pour éviter de compter 2 fois les motifs positifs dans l'affichage !
			    CMD="../tensp -n $min_supp -f $min_pos_supp -m $max_pat_len $db"
			    echo "=>" $CMD
			    timeout ${timeout} /usr/bin/time -o tmp.txt -f " %U %M " $CMD > out.tmp

			    #extract featuresgrep "< " out1.tmp
			    echo -n `more tmp.txt` >> $outputfile
			    nbpat=`wc -l out.tmp | cut -d " " -f 1`
			    nbtemp=`grep "< " out.tmp | wc -l | cut -d " " -f 1`
			    echo " $nbpat $nbtemp" >> $outputfile
			    #clean
			    rm -f out.tmp tmp.txt
	        done

	        ### Tense ###
	        ### With different values for maxgap ###
	        for mg in 7 # 15 10 7 4
	        do
		        echo -n "tense $len $n $ql $it 0 $f $mg " >> $outputfile

	            #run command
	            CMD="../tense -f $min_supp -mg $mg -miss $max_is_size -m $max_pat_len $db"
	            echo "=>" $CMD
	            timeout ${timeout} /usr/bin/time -o tmp.txt -f " %U %M " $CMD > out.tmp

		        #extract features
		        echo -n `more tmp.txt` >> $outputfile
		        nbpat=`wc -l out.tmp | cut -d " " -f 1`
		        nbtemp=`grep "NTP:" out.tmp | wc -l | cut -d " " -f 1`
		        echo " $nbpat $nbtemp" >> $outputfile
		        #clean
		        rm -f out.tmp tmp.txt
	        done
		done
	done
done
done
done

