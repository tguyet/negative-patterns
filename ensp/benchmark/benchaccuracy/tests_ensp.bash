#!/bin/bash

timeout="10m"
max_pat_len=5
db="output.dat"
n=1000
l=20

expectednb=1
outputfile="cmp.csv"

echo "method minsup varsigma mg data_th data_minl data_maxl nbfound nbreal it time mem" > $outputfile

th=0.3
for mlambda in 0.2 #0.25 #0.05 0.1 0.15 0.2 0.25
do
    Mlambda=$(echo "scale=4;$mlambda+0.05" | bc)
    nb=0
    while [ ! $expectednb -eq $nb ]
    do
        python generation_sequences.py -n ${n} -l $l --np=4 --lp=3 -d 20 --th=$th --neg --Mlambda=$Mlambda --mlambda=$mlambda
        cat output.pat | grep "-" > neg.pat
        realnb=`wc -l neg.pat | cut -f1 -d' '`
        if [ $realnb -eq "0" ]
        then
            echo "no negative pattern generated with lambda ${mlambda}, done $nb/$expectednb."
            continue
        fi

        for f in 0.14 #0.3 0.25 0.2 0.17 0.14 0.11 0.08 0.05 #0.2 0.15 0.1
        do
            #get the minimal threshold as integer numbers
            val=$(echo "$n*$f" | bc)
            min_supp=${val%.*}

            ### eNSP with different values for frequency in positives ###
            for fp in 10 8 5 2 1
            do
                lfp=$(echo "scale=4;$fp*$f/10" | bc) #compute the positive threshold as 80% of the negative threshold while fp=8
	            val=$(echo "$n*$lfp" | bc)
	            min_pos_supp=${val%.*}

	            #run eNSP
	            CMD="../../prefixspan -n $min_supp -f $min_pos_supp -m $max_pat_len $db"
	            echo "=>" $CMD
	            timeout ${timeout} /usr/bin/time -o tmp.txt -f " %U %M " $CMD > tmp_ensp.txt
	            
                while IFS='' read -r line || [[ -n "$line" ]]; do
                    grep "^"$line" " tmp_ensp.txt
                done < "neg.pat" > "found.txt"

                foundnb=`wc -l found.txt | cut -f1 -d' '`
                ratio_ensp=$(echo "scale=4;$foundnb/$realnb" | bc)
                
                echo -n "eNSP $f $fp -1 $th $mlambda $Mlambda $foundnb $realnb $nb " >> $outputfile
                echo `more tmp.txt` >> $outputfile
            done
        done
        
        let "nb++"
    done
done

