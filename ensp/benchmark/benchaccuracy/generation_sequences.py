#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Random sequence generator based on chronicle models.

The program generates two types of datasets:
- dataset containing frequent patterns
- discriminant patterns (with positive and negative sequences)

Moreover, it is possible to generate dataset of patterns with negations.
    -> in this case, chronicles are simply positive, linear patterns !
    The principle of the generation of a negative patterns is first to generate a dataset containing 
    frequent chronicles randomly generated (with linear, positive temporal constraints only). Each 
    generated chronicle is complemented with the choice of a negative position
    While the database has been generated, the generated dataset is analyzed to identify which event can
    be choosen to be negative at the negative position.
    This event has to be frequent enough (in order to be detected by e-NSP), but not to much, such that the 
    negative pattern is frequent! This is set up by the two thresholds Mlambda and mlambda!
    
    
@author: T. Guyet
@date: 09/06/2017
"""

import numpy as np
import sys, getopt
import warnings
import scipy.sparse.csgraph

        
class chronicle:
    """Class for a chronicle pattern modeling
    """
    nchro = 0
    
    def __init__(self):
        self.multiset=list() #sorted list of items (e)
        
        self.negative_item=None
        self.negative_position=None
        self.occurred_possible_neg_items={} # { item : [seqid, ...] }
        
        self.tconst={}  #temporal constraints,
                        # keys: couple (ei,ej) where ei is a index in the item
                        #   in the multiset
                        # values; couple (mu,var) where mu is the 
        self.inconsistent = False
        self.tidl_pos=[]    # TID list in positive dataset
        self.tidl_neg=[]    # TID list in negative dataset

        self.cid = chronicle.nchro #chronicle id
        chronicle.nchro += 1
        
    def add_possible_neg_item(self,item, seqid):
        self.occurred_possible_neg_items.setdefault(item,set())
        self.occurred_possible_neg_items[item].add(seqid)
        
        
    def add_item(self, item):
        """Add an item to the chronicle and return the id of the added event
        The function creates all infinite constraints, without variability
        - the id of the event correspond to the order of added items
        """
        self.multiset.append(item)
        id = len(self.multiset)-1
        for i in range(len(self.multiset)):
            self.tconst[(i,id)]= (-float("inf"),float("inf"))
        return id
        
    def add_constraint(self, ei, ej, constr):
        """Add a constraint-template to the chronicle pattern
        - ei, ej: index of the events in the multiset
        - constr: a 4-tuple (mu-start, var-start, mu-end, var-end) of the mean and variance of temporal constraint
        """
        if not type(constr) is tuple:
            print "error: constraint must be a tuple (=> constraint not added)"
            return
            
        if len(constr)!=2:
            print "error: constraint must have 4 values (=> constraint not added)"
            return
            
        try:
            self.tconst[(ei,ej)] = constr
        except IndexError:
            print "add_constraint: index_error (=> constraint not added)"
        
    def __getitem__(self, i):
        """return the item at position i in the multiset if i is an integer
        and return the constraint between i[0] and i[1] if i is a couple
        """
        if not type(i) is tuple:
            return self.multiset[i]
        else:
            return self.tconst[(min(i[0],i[1]),max(i[0],i[1]))]
        
    def __len__(self):
        return len(self.multiset)
            
    def __str__(self):
        s = "C"+str(self.cid)+": {"+str(self.multiset) + "}\n"
        for i in range(len(self.multiset)):
            for j in range(i+1,len(self.multiset)):
                s += str(i) + "," + str(j) + ": " + str(self.tconst[(i,j)])+"\n"
        if not self.negative_item is None:
            s+= "neg item " + str(self.negative_item)+ " after event at position " + str(self.negative_position)+"\n"
        elif not self.negative_position is None:
            s+= "neg pos: " + str(self.negative_position)+"\n"
        return s

    def str_neg(self):
        s=''
        for i in range(len(self.multiset)-1):
            s += "("+str(self.multiset[i])+"),"
            if (not self.negative_item is None) and (i==self.negative_position):
                s += "-("+str(self.negative_item)+"),"
        s += "("+str(self.multiset[len(self.multiset)-1])+")"
        return s

    def minimize(self):
        #construction of distance graph
        mat=np.matrix( np.zeros( (len(self),len(self)) ))
        for i in range(len(self)):
            for j in range(i+1,len(self)):
                mat[i,j] = self.tconst[ (i,j) ][1]
                mat[j,i] = -self.tconst[ (i,j) ][0]
        try:
            matfw = scipy.sparse.csgraph.floyd_warshall( mat )
            #construction of simpllified chronicle
            for i in range(len(self)):
                for j in range(i+1,len(self)):
                    self.tconst[ (i,j) ] = (- matfw[j,i], matfw[i,j])
        except:
            warnings.warn("*** Minimisation: Inconsistent chronicle ***")
            self.inconsistent = True
        
class item_generator:
    def __init__(self, n=100, fl="gaussian"):
        self.flaw=[]    # distribution for the item frequency: "uniform" or "gaussian"
        self.nb=n       # number of items
        if fl=="uniform":
            self.flaw=np.ones(self.nb)/self.nb
        elif fl=="gaussian":
            vec = range(0,self.nb)
            vec = [v/float(self.nb) for v in vec]
            sigma=.05
            mu=0.5
            for v in vec:
                self.flaw.append(np.exp( - (v-mu)*(v-mu)/(2*sigma*sigma)) )
            np.random.shuffle(self.flaw)
            self.flaw=self.flaw/sum(self.flaw) #normalisation
        else:
            warnings.warn("*** Unknown distribution law ***")
        
    """ function that generates a random item according to the item distribution modeling
    """
    def generate_item(self):
        return np.random.choice(self.nb, 1, p=self.flaw)[0]


class constraint_generator:
    
    def __init__(self, minstart=-100, maxstart=100, minduration=0.1, maxduration=200):
        self.ms=minstart
        self.Ms=maxstart
        self.md=minduration
        self.Md=maxduration
    
    def minstart(self):
        return self.ms
    
    def generate(self, ctype=""):
        if ctype=="after":
            s= np.random.uniform(0, self.Ms)
            f= s + np.random.uniform(self.md, self.Md)
        else:
            s= np.random.uniform(self.ms, self.Ms)
            f= s + np.random.uniform(self.md, self.Md)
        c=(s, f)
        return c


class chronicle_generator:
    """
    \class chronicle_generator
    \brief Factory class for chronicles
    It provide a function to generates chronicles with consistant, minimal constraints; and a function to generate disturbed chronicles
    In this class negation are not used
    """
    
    maxtest=10
    
    def __init__(self, ig, cg):
        self.itemGenerator = ig
        self.constraintGenerator = cg
        self.constraintdensity = 0.3
        
    def __raw_generate__(self, l):
        chro = chronicle()
        for i in range(l):
            item = self.itemGenerator.generate_item()
            chro.add_item(item)
        for i in range(l):
            for j in range(i+1,l):
                if np.random.rand()<self.constraintdensity:
                    c=self.constraintGenerator.generate()
                    chro.add_constraint(i, j, c)
        return chro
    
    def generate(self,l,cd):
        """
        Function that generate a random consistent, minimal chronicle.
        Generate and test approach: generate a random chronicle, test if it is consistent (at most maxtime times, otherwise None is returned)
        - l: mean size of the multiset a chronicle pattern
        - cd: constraint density
        """
        self.constraintdensity = cd
        T=0
        while T<chronicle_generator.maxtest:
            chro = self.__raw_generate__(l)
            #consistency checking and minimisation
            chro.minimize()
            if not chro.inconsistent:
                return chro
            T+=1
        raise NameError("Impossible to generate a consistent chronicles")
        return None
        
        
    def generate_similar(self, C, proba=[0.1,0.8,0.1]):
        """function that generates a chronicle similar to C
        - C is a chronicle
        - proba: list of 3 values representing the proba of modifications:
            1- removing items (defailt 0.1)
            2- modyfying constraints (default 0.8)
            3- adding item (default 0.1)
        
        what can change in the generated chronicle:
            - temporal bounds
            - multi-set (add or remove items)
        """
        if not isinstance(C, chronicle):
            return
            
        chro = chronicle()
        
        ########## RANDOM MODIFICATION SELECTION  ###############
        removeitem=False
        modify_tconst = False
        additem=False
        
        #proba normalisation
        vec=np.array(proba)
        proba = vec/np.sum(vec)
        alea=np.random.rand()
        i=0
        while i<3 and alea>proba[i]:
            alea -= proba[i]
            i+=1
        if i==0:
            removeitem=True
        elif i==1:
            modify_tconst = True
        else:
            additem=True
        
        ################ CHRONICLE MODIFICATIONS #############
        l=len(C.multiset)
        if removeitem:
            idr = np.random.randint( l )
            for i in range(l):
                if i==idr:
                    continue
                chro.multiset.append( C[i] )
            #copy constraints (removing idr + decay references)
            for i in range(idr):
                for j in range(i+1,idr):
                    chro.add_constraint(i, j, C[(i,j)] )
                for j in range(idr+1,l):
                    chro.add_constraint(i, j-1, C[(i,j)] )
            for i in range(idr+1,l):
                for j in range(i+1,l):
                    chro.add_constraint(i-1, j-1, C[(i,j)] )
                    
        if additem: #add a new item to
            chro.multiset = list(C.multiset)
            chro.tconst = C.tconst.copy()
            ni = self.itemGenerator.generate_item()
            chro.multiset.append(ni)
            nl = len(chro.multiset)-1
            for j in range( nl ):
                if np.random.uniform(0,1)<self.constraintdensity:
                    c=self.constraintGenerator.generate()
                else:
                    c=(-float("inf"), float("inf"))
                chro.add_constraint(j, nl, c)
        
        if modify_tconst:
            chro.multiset = list(C.multiset)
            chro.tconst = dict(C.tconst)
            j = np.random.randint( 1, l )
            i = np.random.randint( j )
            
            """
            tc = C[(i,j)]
            if tc[0]==-float("inf") and tc[1]==float("inf"):
                #no constraint, we add a constraint
                c = self.constraintGenerator.generate()
            elif tc[0]==-float("inf"):
                c = ( np.random.uniform(self.constraintGenerator.ms,max(self.constraintGenerator.ms,min(self.constraintGenerator.Ms,tc[1]))), tc[1] )
            elif tc[1]==float("inf"):
                c = ( tc[0], tc[0] +np.random.uniform(self.constraintGenerator.md,self.constraintGenerator.Md))
            else:
                #more constraint
                al = np.random.uniform(tc[0],tc[1],2)
                c = (min(al[0],al[1]), max(al[0],al[1]))
            """
            
            #generate a new random constraint
            c = self.constraintGenerator.generate()
            chro.add_constraint(i, j, c )
        
        ################ CHRONICLE MINIMISATION #############
        chro.minimize()
        return chro

class negative_chronicle_generator:
    """
    \class negative_chronicle_generator
    \brief Factory class that generates a linear chronicle, including a negative item
    """
    
    maxtest=10
    
    
    def __init__(self, ig, cg):
        """
        \param ig is an itemset generator
        \param cg is a constraint generator that **must generate only positive temporal constraints**
        """
        self.itemGenerator = ig
        self.constraintGenerator = cg
        if cg.minstart()<0:
            warnings.warn( "minstart value for the constraintGenerator will not be used\n" )
        
    def generate(self, l, dc):
        """
        Function that generates a linear chronicle
        The generate chronicle will be automatically consistent consitering the positive linear constraints we impose (on the contrary to the other chronicle generator)
        \param l: mean size of the multiset a chronicle pattern
        \param dc: unused
        """
        chro = chronicle()
        for i in range(l):
            item = self.itemGenerator.generate_item()
            chro.add_item(item)
        for i in range(l):
            c=self.constraintGenerator.generate(ctype="after")
            chro.add_constraint(i, i+1, c)
            
        chro.minimize() #compute the complete graph of temporal constraints
        assert(not chro.inconsistent)
        
        #generate a (unique) position in which to place a negative item
        if l>1:
            chro.negative_position = np.random.randint(0,l-1)
        
        return chro
        

class sequence:
    """ A sequence is a list of time-stamped items
    All sequences start at 0 and there duration is defined while constructed (self.duration)
    """
    nbs=0 #counter of sequences
    gen_int_timestamped=True #if True, timestamped will be integers
    
    def __init__(self, rl =20, d=1000):
        """
        """
        self.patterns = []      # list of chronicle included in the sequence
        self.duration = d       # duration of the sequence
        self.seq=[]             # list of couple (t,e) where t is a timestamp and e an item
        self.requiredlen=rl     # sequence length (number of items)
        self.id=sequence.nbs
        sequence.nbs+=1

    """
    add a pattern without redundancy
    """
    def add_pattern(self, p):
        if not p in self.patterns:
            self.patterns.append(p)

    """
    Compute the sum of pattern length 
    """
    def __patcontentlen__(self):
        total=0
        for p in self.patterns:
            total += p.len()
        return total
    
    def interval_substraction(self,intervals, int_rest):
        """
        \param list of intervals
        \param int_rest interval to substract to each element of intervals
        """
        new_inter = intervals[:]
        for inter in intervals:
            if inter[0]<int_rest[0]:
                if inter[1] > int_rest[1]:
                    new_inter.append( (inter[0], int_rest[0]) )
                    new_inter.append( (int_rest[1], inter[1]) )
                elif inter[0] > int_rest[0]:
                    new_inter.append( (inter[0], int_rest[0]) )
                else:
                    new_inter.append( (inter[0], inter[1]) )
            elif inter[0]<int_rest[1]:
                if inter[1] > int_rest[1]:
                    new_inter.append( (int_rest[1], int_rest[0]) )
            else:
                new_inter.append( (inter[0], inter[1]) )
        return new_inter
        
        
    """
    Generate the sequence of items from the patterns it contains and according to the require length
    - pg : pattern generator used to generate random items to mix the pattern occurrences with random items
    
    Timestamped are generated according to a random uniform law within the duration of the sequence
    """    
    def self_generate(self, pg):
        
        
        # no patterns inside: fully random sequences
        if len(self.patterns)==0:
            l=int(np.random.normal(self.requiredlen, float(self.requiredlen)/float(10.0)))
            for i in range(l):
                #random item (according to an item generator)
                item = pg.generate_item()
                #random timestamp
                timestamp = np.random.uniform(self.duration)
                self.seq.append( (timestamp,item) )
                if sequence.gen_int_timestamped:
                    self.seq = [ (int(c[0]),c[1]) for c in self.seq ]
                self.seq.sort(key=lambda tup: tup[0])
            return
            
        negative_period={}
        
        totcreated=0
        for p in self.patterns:
            occurrence=[] #timestamped of the occurrence
            t=np.random.uniform(0,self.duration/2) #chronicle start at the beginning of the sequence
            occurrence.append(t)
            self.seq.append( (t,p[0]) )
            
            for i in range(1,len(p)):
                # construct a interval in which i-th event could occurs
                interval=[0,self.duration]
                for j in range(i):
                    lc = p[ (j,i) ] #chronicle constraint between j and i
                    #interval intersection (constraints conjunction)
                    if interval[0]<occurrence[j]+lc[0]:
                        interval[0]=occurrence[j]+lc[0]
                    if interval[1]>occurrence[j]+lc[1]:
                        interval[1]=occurrence[j]+lc[1]
                    
                #generate a timestamp in the interval
                if interval[0]>=interval[1]:
                    warnings.warn("*** chronicle is not consistent ***")
                    self.seq=[]
                    return
                
                t = np.random.uniform(interval[0],interval[1])
                self.seq.append( (t,p[i]) )
                occurrence.append(t) #timestamp of the i-th item in the chronicle occurrence
                
            if not p.negative_position is None:
                if p.negative_position == (len(occurrence)-1):
                    negative_period[p]=(occurrence[p.negative_position],float("inf"))
                else:
                    negative_period[p]=(occurrence[p.negative_position],occurrence[p.negative_position+1])
                
            totcreated += len(p)
            
        l=int(np.random.normal(self.requiredlen, float(self.requiredlen)/float(10.0)))
        while totcreated<l:
            #random item (according to an item generator)
            item = pg.generate_item()
            #random timestamp
            timestamp = np.random.uniform(self.duration)
            self.seq.append( (timestamp,item) )
            totcreated+=1
            
            
        #sort the list according to timestamps
        if sequence.gen_int_timestamped:
            self.seq = [ (int(c[0]),c[1]) for c in self.seq ]
        self.seq.sort(key=lambda tup: tup[0])
        
        for p in self.patterns:
            for item in self.seq:
                if item[0]>negative_period[p][0] and item[0]<negative_period[p][1]:
                    p.add_possible_neg_item(item[1], self.id)
        
    
    def write_IBM(self):
        """sequence format: IBM format
        """
        if len(self.seq)==0:
            return ""
        s=""
        for item in self.seq:
            s+= str(self.id)+" " + str(item[0]) + " " + str(1) + " " + str(item[1]) + "\n"
        return s
        
    def write_line(self):
        """Write out function: 1 sequence in a line
        """
        if len(self.seq)==0:
            return ""
        s=str(self.seq[0])
        for c in self.seq[1:]:
            s += " ("+str(c[0])+","+str(c[1])+")"
        return s
    
    def __str__(self):
        """string cast of a sequence
        """
        #return self.write_line()
        return self.write_IBM()

class db_generator:
    """Database generator
    """

    def __init__(self, neg_pat=False):
        """
        \param neg_pat: if True, generate linera chornicles with 1 negative item
        """
        self.patgen = None      # last generated pattern generator
        self.patterns = []      # set of patterns
        self.db = []            # database of sequences
        
        self.nbex = 1000        # number of sequences in the database
        self.l = 30             # mean length of the sequences
        self.n = 100            # number of items
        self.fl="uniform"       # item frequency distribution
        self.nbpat = 5          # number of the patterns
        self.lpat = 5           # mean length of the patterns
        self.th = 0.20          # pattern threshold
        self.dc = 0.30          # constraint density (if 0: no constraints, if 1 each pair of event are temporaly constraints)
        self.neg_pat = neg_pat
        self.min_lambda=0.2     # lambda threshold (for negative patterns: maximal proportion of sequence supporting the positive partner without satisfying the negative item)
        self.max_lambda=0.5     
    
    
    def generate_patterns(self):
        """ The function generates a set of self.nbpat patterns of mean length self.lpat.
        It uses the self.patgen pattern generator
        - Requires the self.patgen to be defined (return an empty list if not)
        """
        if self.patgen == None:
            warnings.warn("*** undefined chronicle pattern generator ***")
            return []
        patlen = [int(np.floor(v)) for v in np.random.normal(self.lpat,max(0.5,float(self.lpat)/10.0),self.nbpat)]
        patterns = [self.patgen.generate(l,self.dc) for l in patlen]
        return patterns
    
    def output_patterns(self):
        s = ''
        for p in self.patterns:
            s += p.str_neg() + "\n"
            '''
            s += str(p)
            s += "TID:"+str(p.tidl_pos) + "\n"
            s += "TID+:"+str(p.tidl_neg) + "\n"
            '''
        return s
    
    def generate_sequences(self, nb=None, l=None, n=None, npat=None, lp=None, th=None, cd=None):
        """Generation of the sequence database
        - nb number of sequences (default)
        - l mean length of the sequence
        - n dictionary size
        - npat number of chronicle patterns
        - lp mean length of chronicle patterns
        - th frequency threshold (number of sequences covering each pattern)
        """
        #update the parameters of the generation
        if not nb is None:
            self.nbex=nb
        if not l is None:
            self.l=l
        if not th is None:
            self.th=th
        if not n is None:
            self.n=n
        if not npat is None:
            self.nbpat=npat
        if not lp is None:
            self.lpat=lp
        if not cd is None:
            self.dc=cd
            
        #collect "real" statistics about the generated sequences
        self.stats={}
        
        #generate a set of nbex empty sequences
        self.db=[]
        for i in range(self.nbex):
            self.db.append( sequence(self.l) )
        self.stats["nbex"]=self.nbex
        
        # create an item factory
        self.itemgen = item_generator(n=self.n, fl=self.fl)
        
        if self.neg_pat :
            # create a constraint factory
            self.constgen = constraint_generator(0,100,0.1,200)
            
            # create a new chronicle pattern generator
            self.patgen = negative_chronicle_generator(self.itemgen, self.constgen)
        else:
            # create a constraint factory
            self.constgen = constraint_generator(-100,100,0.1,200)
            
            # create a new chronicle pattern generator
            self.patgen = chronicle_generator(self.itemgen, self.constgen)
            
        #generate self.nbpat random patterns using the generator
        self.patterns=self.generate_patterns()
        self.stats["nbpat"] = len(self.patterns)
        
        nbM=-1
        nbMean=0
        nbm=self.nbex
        #attribute transactions to the patterns
        for p in self.patterns:
            nbocc = self.nbex*self.th  + (np.random.geometric(0.15)-1) # number of occurrences generated (randomly above the threshold)
            nbocc = min(nbocc, self.nbex)
            #generation of a random set of sequences of size nbex (ensure no repetitions)
            vec = range(0,self.nbex)    # get a set of id
            np.random.shuffle( vec )    # shuffle it a little bit
            patpos = vec[:int(nbocc)]   # take just take the require number of sequence (at the beginning)
            p.tidl_pos = patpos
            
            nb=0
            for pos in patpos:
                try:
                    self.db[pos].add_pattern( p )
                    nb+=1
                except IndexError:
                    warnings.warn("*** index error: "+str(pos)+" ***")
                    pass
            nbM=max(nbM,nb)
            nbM=min(nbm,nb)
            nbMean+=nb
        if self.stats["nbpat"]!=0: 
            nbMean/=self.stats["nbpat"]
        else:
            nbMean=0
        self.stats["nboccpat_max"]=nbM
        self.stats["nboccpat_min"]=nbm
        self.stats["nboccpat_mean"]=nbMean
        
        #generate sequences
        for i in range(self.nbex):
            self.db[i].self_generate(self.itemgen)
            
        
        #generate negation in patterns
        for p in self.patterns:
            todelete=[]
            for item, seqids in p.occurred_possible_neg_items.iteritems():
                gen_lambda = float(len(seqids))/float(len(p.tidl_pos)) #proportion of sequences that do have the item item, among sequences that support the positive pattern
                
                if gen_lambda < self.min_lambda or gen_lambda > self.max_lambda:
                    todelete.append(item)
                    
            # delete from the possible negative items, those that does not satisfy the lambda constraint
            for item in todelete:
                del( p.occurred_possible_neg_items[item] )

            #random choice of an item that satisfy the th_lambda threshold
            if len( p.occurred_possible_neg_items.keys() )>0:
                ritem = p.occurred_possible_neg_items.keys()[ np.random.randint(len( p.occurred_possible_neg_items.keys() )) ]
                p.negative_item = ritem
                #update the list of 
                p.tidl_neg = p.tidl_pos[:]
                p.tidl_pos = [item for item in p.tidl_pos if item not in p.occurred_possible_neg_items[ritem] ]
        
        
        return self.db
        

helpstring="""generation.py -n <val> -l <val> --np=<val> --lp=<val> --th=<val> -d <val> -o <outputfile> --neg --discr
    * n: number of sequences (default: 100)
    * l: default length of the sequence (default: 10)
    * d: dictionnary size (number of different items) (default: 20)
    * o output filename
    * np: number of patterns (default 1),
    * lp: mean length of patterns (default 5),
    * c: constraint density (mean number of constraints) (default: 0.3)
    * th: threshold of patterns, minimum occurrence frequency in the database (default 0.1),
    * discr: activate data generation for dicriminant pattern mining
    * g: growth rate for discriminant patterns (default 2)
    * --neg: activate negative (not compatible with discriminant)
    * --mlambda / --Mlambda: minimum and maximum lambda thresholds (only for negative patterns). Lambda is the proportion of sequences that does not have its negative item, among sequences that support the positive pattern
    
    The generate generate random chornicle patterns and hide them in sequences of the database. Note that the given list of patterns may not be complete with regard to the generated dataset: generation of random sequences/items may create additional occurrences !!
    
Usage example:
    - Generation of datasets for frequent patterns mining
    $ python generation.py -l 10 -d 20 -c 0.6 -o output.dat
    This command generates two files:
        - output.dat: IBM format or a space separated file (one transaction per line)
        - output.pat: description of the hidden patterns (for each hidden pattern, you have its description, a sequence of events, and its locations in the database, ie the ids of the transaction in which it is!)
        
    - Generation of dataset for discriminant pattern mining
    $ python generation.py -l 10 -d 20 -c 0.6 --discr -g 3 -o output.dat
    This command generates three files:
        - output_pos.dat: database of sequences for positive class (IBM format or 1 line per sequence)
        - output_neg.dat: database of sequences for negative class 
        - output.pat: description of the hidden patterns
        
    - Generation of dataset for negative pattern mining
    $ python generation.py -l 10 -d 20 --neg -o output.dat
    This command generates two files:
        - output.dat: database of sequences with 
        - output.pat: description of the hidden patterns (linear chronicles, a single negative event)
"""


def main(argv):
    """Main function
    - parse the command line arguments
    - generate a database in a file
    """
    
    ## Default parameters values
    seqnb=100
    length=10
    maxitems=20
    outputfile="output.dat"
    nbpatterns = 3
    lengthpatterns = 4
    threshold=0.10
    min_lambda=0.20
    max_lambda=0.50
    cd = 0.30
    discriminant = False
    negatives = True
    growthrate = 2
    
    ## Read parameters
    try:
       opts, args = getopt.getopt(argv,"hn:d:l:c:o:g:",["discr","neg","Mlambda=","mlambda=","nbseq=","nbitems=","length=","ofile=", "np=", "lp=", "th="])
    except getopt.GetoptError:
       print helpstring
       sys.exit(2)
    for opt, arg in opts:
       if opt == '-h':
           print helpstring
           sys.exit()
       elif opt in ("-d", "--nbitems"):
           try:
               maxitems = int(arg)
           except:
               print("error with argument -d: an integer must be given")
       elif opt in ("-l", "--length"):
           try:
               length = int(arg)
           except:
               print("error with argument -l: an integer must be given")
       elif opt in ("-n", "--nbseq"):
           try:
               seqnb = int(arg)
           except:
               print("error with argument -n: an integer must be given")
       elif opt in ("-o", "--ofile"):
           outputfile = arg
       elif opt in ("--np"):
           try:
               nbpatterns = int(arg)
           except:
               print("error with argument --np: an integer must be given")
       elif opt in ("--lp"):
           try:
               lengthpatterns = int(arg)
           except:
               print("error with argument --lp: an integer must be given")
       elif opt in ("-c", "--constdensity"):
           try:
               cd = float(arg)
           except ValueError:
               print("error with argument -c: a float must be given")
       elif opt in ("--discr"):
           discriminant = True
       elif opt in ("--neg"):
           negatives = True
       elif opt in ("--mlambda"):
           try:
               min_lambda = float(arg)
           except:
               print("error with argument --mlambda: a float must be given")
           if min_lambda<0 or min_lambda>1:
               print("error with argument --mlambda: invalid value (default value)")
               min_lambda=0.2
       elif opt in ("--Mlambda"):
           try:
               max_lambda = float(arg)
           except:
               print("error with argument --Mlambda: a float must be given")
           if max_lambda<0 or max_lambda>1:
               print("error with argument --Mlambda: invalid value (default value)")
               max_lambda=0.2
       elif opt in ("-g"):
           try:
               growthrate = float(arg)
           except ValueError:
               print("error with argument -g: a float must be given")
       elif opt in ("--th"):
           try:
               threshold = float(arg)
           except ValueError:
               print("error with argument --th: a float must be given")

    
    ## Generate database of sequences
    dbgen = db_generator(negatives)
    dbgen.min_lambda = min_lambda
    dbgen.max_lambda = max_lambda
    db=dbgen.generate_sequences(nb=seqnb, l=length, n=maxitems, npat=nbpatterns, lp=lengthpatterns, th=threshold, cd=cd)
       
    ##write output
    fout = open(outputfile, "w")
    for s in db:
        fout.write(str(s))
    fout.close()
    
    #output patterns
    filename=outputfile.rsplit(".",1)
    outputfile=filename[0]+".pat"
    fout = open(outputfile, "w")
    fout.write(dbgen.output_patterns())
    fout.close()

if __name__ == "__main__":
    main(sys.argv[1:])

    
