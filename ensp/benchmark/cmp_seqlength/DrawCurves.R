rm(list=ls())
library(readr)
library(reshape2)
require(ggplot2)
library(dplyr)

#function required for legend generation
require(gridExtra)
g_legend <- function(a.gplot){
  tmp<- ggplot_gtable(ggplot_build(a.gplot))
  leg <- which(sapply(tmp$grobs, function(x) x$name) == "guide-box")
  legend <- tmp$grobs[[leg]]
  return(legend)
}

# if export legend is True, then, a legend is generated in a separated PDF file (and figure does not have a proper legend)
export_legend=T

######################################################################"
wd = "/home/tguyet/Recherche/EnCours/negative-patterns/ensp/benchmark/cmp_seqlength"
setwd(wd)
results <- read_delim("cmp_all.csv", ",", escape_double = FALSE)

###################################################
#courbes time
data = results %>%
  filter( time<600 )
data[data$fp==0&data$mg==0,]$fp=-1
data= data[data$fp!=-1|data$time!=300,]
data$fp=factor(data$fp)

p <- ggplot(data, mapping=aes(x=len, y=time, color=fp, shape=fp))
p <- p + geom_smooth(method="lm",formula=(y ~ x))
p <- p + scale_y_log10()
p <- p + geom_point(size=2.)
p <- p + xlab("Sequence length") + ggtitle("") + ylab("Time (s)")
p <- p + scale_color_discrete(
  limits=c('-1','0','5','8','10'),
  labels=c('-1'=expression("NegPSpan ("~italic(tau)~"="~infinity~")"),'0'= expression("NegPSpan ("~italic(tau)~"=10 )"), '5'= expression("eNSP ("~varsigma~"=0.5"~sigma~")"), '8'= expression("eNSP ("~varsigma~"=0.8"~sigma~")"), '10'= expression("eNSP ("~varsigma~"="~sigma~")")))
p <- p + scale_shape_discrete(
  limits=c('-1','0','5','8','10'),
  labels=c('-1'=expression("NegPSpan ("~italic(tau)~"="~infinity~")"),'0'= expression("NegPSpan ("~italic(tau)~"=10 )"), '5'= expression("eNSP ("~varsigma~"=0.5"~sigma~")"), '8'= expression("eNSP ("~varsigma~"=0.8"~sigma~")"), '10'= expression("eNSP ("~varsigma~"="~sigma~")")))
p <- p + geom_hline(yintercept=c(300), color='black',linetype=5)
p <- p + theme_bw()
p <- p + theme(
  axis.text.x = element_text(size = 20, color='black'),
  axis.text.y = element_text(size = 20, color='black'),
  axis.title.y = element_text(size=24),
  axis.title.x = element_text(size=24),
  strip.text = element_text(size=24),
  legend.title=element_blank(),
  panel.grid.major = element_blank())#, panel.grid.minor = element_blank())
#p <- p + theme(text = element_text(size=24),legend.position = c(0.9, 0.1))
if(export_legend) {
  p <- p + theme(legend.position="none")
}
p

###
# compute regression coefficients
fp=0
expo.model=lm(formula = time ~ exp(len), data = data[data$fp == fp, ])
print(expo.model$coefficients)
print(log(expo.model$coefficients[2]))
plot(data[data$fp == fp, ]$len, data[data$fp == fp, ]$time, pch=16)
lines(data[data$fp == fp, ]$len, predict(expo.model,list(len=data[data$fp == fp, ]$len)), lwd=2, col = "red")


ggsave(filename="timeVSlength.pdf", plot=p, width = 12, height = 7)

if(export_legend) {
  # Generate legend (to be saved manually)
  p <- p + theme(text = element_text(size=12),legend.position = "bottom") + guides(color=guide_legend(title=""),shape=guide_legend(title=""),linetype=guide_legend(title=""))
  legend <- g_legend(p)
  grid.arrange(legend)
  legend
  ggsave(filename="legend_length.pdf", plot=legend, width = 7, height = 0.3)
}
###################################################
#courbes mémoire (maxRSS)
data = results %>%
  filter( time<600 )
data[data$fp==0&data$mg==0,]$fp=-1
data= data[data$fp!=-1|data$time!=300,]
data$fp=factor(data$fp)

p <- ggplot(data, mapping=aes(x=len, y=MaxRSS, color=fp, shape=fp))
#p <- p + geom_smooth()
p <- p + geom_smooth(method="lm",formula=(y ~ x))
p <- p + scale_y_log10()
p <- p + geom_point(size=2.)
p <- p + xlab("Sequence length") + ggtitle("") + ylab("maxRSS (bytes)")
p <- p + scale_color_discrete(
  limits=c('-1','0','5','8','10'),
  labels=c('-1'=expression("NegPSpan ("~italic(tau)~"="~infinity~")"),'0'= expression("NegPSpan ("~italic(tau)~"=10 )"), '5'= expression("eNSP ("~varsigma~"=0.5"~sigma~")"), '8'= expression("eNSP ("~varsigma~"=0.8"~sigma~")"), '10'= expression("eNSP ("~varsigma~"="~sigma~")")))
p <- p + scale_shape_discrete(
  limits=c('-1','0','5','8','10'),
  labels=c('-1'=expression("NegPSpan ("~italic(tau)~"="~infinity~")"),'0'= expression("NegPSpan ("~italic(tau)~"=10 )"), '5'= expression("eNSP ("~varsigma~"=0.5"~sigma~")"), '8'= expression("eNSP ("~varsigma~"=0.8"~sigma~")"), '10'= expression("eNSP ("~varsigma~"="~sigma~")")))
p <- p + theme_bw()
p <- p + theme(
  axis.text.x = element_text(size = 20, color='black'),
  axis.text.y = element_text(size = 20, color='black'),
  axis.title.y = element_text(size=24),
  axis.title.x = element_text(size=24),
  strip.text = element_text(size=24),
  legend.title=element_blank(), 
  panel.grid.major = element_blank())
#p <- p + theme(text = element_text(size=24),legend.position = c(0.9, 0.1))
if(export_legend) {
  p <- p + theme(legend.position="none")
}
p

ggsave(filename="maxRSSVSlength.pdf", plot=p, width = 12, height = 7)

###################################################
#courbes nbpatterns
data = results %>%
  filter( time < 600 )
data[data$fp==0&data$mg==0,]$fp=-1
data= data[data$fp!=-1|data$time!=300,]
data$fp=factor(data$fp)

p <- ggplot(data)
p <- p + geom_smooth(mapping=aes(x=len, y=log(NbPatNeg), shape=fp, color="#Neg patterns"))
p <- p + geom_smooth(mapping=aes(x=len, y=log(NbPat), shape=fp, color="#patterns"))
p <- p + geom_point(size=3., mapping=aes(x=len, y=log(NbPatNeg), shape=fp, color="#Neg patterns"))
p <- p + geom_point(size=3., mapping=aes(x=len, y=log(NbPat), shape=fp, color="#patterns"))
p <- p + theme_bw()
p <- p + xlab("Sequence length") + ggtitle("") + ylab("log(#patterns)")
p <- p + scale_shape_discrete(limits=c('-1','0','5','8','10'),
                              labels=c('-1'=expression("NegPSpan ("~italic(tau)~"="~infinity~")"),'0'= expression("NegPSpan ("~italic(tau)~"=10 )"), '5'= expression("eNSP ("~varsigma~"=0.5"~sigma~")"), '8'= expression("eNSP ("~varsigma~"=0.8"~sigma~")"), '10'= expression("eNSP ("~varsigma~"="~sigma~")")))
p <- p + theme(legend.title=element_blank(), panel.grid.major = element_blank())
p

ggsave(filename="nbpatnegVSlength.pdf", plot=p, width = 12, height = 7)


