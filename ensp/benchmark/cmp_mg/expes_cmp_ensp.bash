#!/bin/bash

#Formatage du nom de la sortie avec des éléments de date et d'heure (unique !)
d=`date '+%d%m_%H%M%S'`
repo="results_$d"
mkdir $repo
outputfile="$repo/cmp.csv"
#outputfile="cmp.csv"

timeout="5m"

#reinitialisation du fichier avec les entêtes des colonnes
echo "method len size ql it fp f mg time MaxRSS NbPat NbPatNeg" > $outputfile

for len in 20 30
do
for n in 500
do
for ql in 20 #15 20 30 40 80 160
do
	# Iteration id
	nbit=20
	for it in `seq 1 1 $nbit`
	do
		db="$repo/db_${len}_${n}_${ql}_${it}.dat"
		# Generation d'un jeu de donnees (not with -neg option)
		python ../generation_sequences.py -l $len -d $ql -n $n -o $db

		#Run algorithm with different parameters on the same dataset
		for f in 0.2 0.15 0.1 #0.2 0.15 0.1
		do
		    #get the minimal threshold as integer numbers
		    val=$(echo "$n*$f" | bc)
		    min_supp=${val%.*}

		    echo "STEP len=" $len " q=" $ql " it=" $it " f=" $f

		    ### eNSP with different values for frequency in positives ###
		    for fp in 8 #10 8 5 2 1
		    do
		        lfp=$(echo "scale=4;$fp*$f/10" | bc) #compute the positive threshold as 80% of the negative threshold while fp=8
			val=$(echo "$n*$lfp" | bc)
			min_pos_supp=${val%.*}

			echo -n "ensp $len $n $ql $it $fp $f 0 " >> $outputfile

			#run command
			CMD="../../prefixspan -no -n $min_supp -f $min_pos_supp -o out.tmp $db"
			echo "=>" $CMD
			timeout ${timeout} /usr/bin/time -o tmp.txt -f " %U %M " $CMD

			#extract features
			echo -n `more tmp.txt` >> $outputfile
			nbpat=`wc -l out.tmp | cut -d " " -f 1`
			nbneg=`grep "-" out.tmp | wc -l | cut -d " " -f 1`
			echo " $nbpat $nbneg" >> $outputfile
			#clean
			rm -f out.tmp tmp.txt
		    done

		    ### CeNSP ###
		    ### With different values for maxgap ###
		    for mg in 0 15 10 7 4
		    do
			echo -n "censp $len $n $ql $it 0 $f $mg " >> $outputfile

		        #run command
		        CMD="../../prefixspan -cneg -f $min_supp -mg $mg -o out.tmp $db"
		        echo "=>" $CMD
		        timeout ${timeout} /usr/bin/time -o tmp.txt -f " %U %M " $CMD

			#extract features
			echo -n `more tmp.txt` >> $outputfile #time
			nbpat=`wc -l out.tmp | cut -d " " -f 1`
			nbneg=`grep "-" out.tmp | wc -l | cut -d " " -f 1`
			echo " $nbpat $nbneg" >> $outputfile
			#clean
			rm -f out.tmp tmp.txt
		    done
		done
	done
done
done
done

