rm(list=ls())
library(readr)
library(reshape2)
require(ggplot2)

#function required for legend generation
require(gridExtra)
g_legend <- function(a.gplot){
  tmp<- ggplot_gtable(ggplot_build(a.gplot))
  leg <- which(sapply(tmp$grobs, function(x) x$name) == "guide-box")
  legend <- tmp$grobs[[leg]]
  return(legend)
}

######################################################################"

wd = "/home/tguyet/Recherche/EnCours/negative-patterns/ensp/benchmark/cmp_mg"
setwd(wd)
results <- read_delim("cmp.csv", " ", escape_double = FALSE)

###################################################
#courbes TIME
data = results
data[data$method=="ensp",]$mg=-1
data$f=factor(data$f)
data$mg=factor(data$mg)
p <- ggplot(data, mapping=aes(x=mg, y=time, color=f)) + facet_wrap(~len, labeller=label_both)
p <- p + geom_boxplot()
p <- p + scale_y_log10()
p <- p + theme_bw()
p <- p + xlab("Max gap constraint") + ggtitle("") + ylab("time (s)")
p <- p + scale_x_discrete(labels=c('-1' = "ensp", '0' = "censp\nno constraint", '4' = "censp\nmg=4", '7' = "censp\nmg=7", '10' = "censp\nmg=10", '15' = "censp\nmg=15"))
p <- p + theme(legend.position="none")
p

ggsave(filename="timeVSmg_bp.pdf", plot=p, width = 12, height = 4)

# Generate legend (to be saved manually)
p <- p + theme(text = element_text(size=12),legend.position = "bottom") + guides(color=guide_legend(title=""),shape=guide_legend(title=""),linetype=guide_legend(title=""))
legend <- g_legend(p)
grid.arrange(legend)
legend
ggsave(filename="legend.pdf", plot=legend, width = 3.5, height = 0.3)

###################################################
#courbes nbpatterns

data = results
data[data$method=="ensp",]$mg=-1
data$f=factor(data$f)
data$mg=factor(data$mg)
p <- ggplot(data, mapping=aes(x=mg, y=NbPatNeg, color=f)) + facet_wrap(~len, labeller=label_both)
p <- p + geom_boxplot()
p <- p + scale_y_log10()
p <- p + theme_bw()
p <- p + xlab("Max gap constraint") + ggtitle("") + ylab("Nb Neg Patterns")
p <- p + scale_x_discrete(labels=c('-1' = "ensp", '0' = "censp\nno constraint", '4' = "censp\nmg=4", '7' = "censp\nmg=7", '10' = "censp\nmg=10", '15' = "censp\nmg=15"))
p <- p + theme(legend.position="none")
p

ggsave(filename="nbpatVSmg_bp.pdf", plot=p, width = 12, height = 4)


