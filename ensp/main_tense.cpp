/**
* Main program for TeNSe algorithm for mining temporal patterns
*
* TIMESTAMPS and COMPUTE_SID have to be turn on in the Prefixspan.h file.
*
* date (last update): 01/2018
* author: T. Guyet <thomas.guyet@agrocampus-ouest.fr>,
* institution: AGROCAMPUS-OUEST/IRISA
* 
* License: GPL2 (Gnu General Public License Version 2)
*/

#include "tense.h"
#include "neg_ps.h"

#include <list>
#include <vector>
#include <climits>
#include <cstdlib>
#include <algorithm>


int main(int argc, char **argv) {
    //bool closed=false, maximal=false;
    //bool itemseq=false;
    long f=10, m=0, /*ns=0, */mW=0, MW=0, min_nbins=0, mg=0;
    float alpha=0.0, beta=0.0;
    char *fn=NULL;
    bool soft=false;
    bool is_inc_soft=false;
    int max_negis_size=1;

    if(argc<=1) {
        cout << "Error: a file must be provided" <<endl;
        return 0;
    }
    
    int i=1;
    while(i<argc) {
        if( argv[i][0]=='-' ) {
            if( !strcmp(argv[i],"-f" ) ) {
                i++;
                if( i>=argc ) {
                    cout << "Error: missing value after -f" <<endl;
                    return 0;
                } else {
                    f = strtol (argv[i], NULL, 0);
                    if( f<=1 || f>=INT_MAX) {
                        cout << "Error: wrong value for -f option (must be >1)" <<endl;
                        return 0;
                    }
                }
            } else if( !strcmp(argv[i],"-softinc" ) ) {
                is_inc_soft=true;
            } else if( !strcmp(argv[i],"-soft" ) ) {
                soft=true;
            } else if( !strcmp(argv[i],"-miss" ) ) {
                i++;
                if( i>=argc ) {
                    cerr << "Error: missing value after -miss" <<endl;
                    return 0;
                } else {
                    max_negis_size = strtol (argv[i], NULL, 0);
                    if( max_negis_size<0 || max_negis_size>=INT_MAX) {
                        cerr << "Error: wrong value for -miss option (must be >=0)" <<endl;
                        return 0;
                    }
                }
            } else if( !strcmp(argv[i],"-n" ) ) {
                i++;
                if( i>=argc ) {
                    cout << "Error: missing value after -n" <<endl;
                    return 0;
                } else {
                    min_nbins= strtol (argv[i], NULL, 0);
                    if( min_nbins<1 ) {
                        cout << "Error: wrong value for -n option (must be >=1)" <<endl;
                        return 0;
                    }
                }
            } else if( !strcmp(argv[i],"-mW" ) ) {
                i++;
                if( i>=argc ) {
                    cout << "Error: missing value after -mW" <<endl;
                    return 0;
                } else {
                    mW= strtol (argv[i], NULL, 0);
                    if( mW<1 ) {
                        cout << "Error: wrong value for -mW option (must be >=1)" <<endl;
                        return 0;
                    }
                }
            } else if( !strcmp(argv[i],"-MW" ) ) {
                i++;
                if( i>=argc ) {
                    cout << "Error: missing value after -MW" <<endl;
                    return 0;
                } else {
                    MW= strtol (argv[i], NULL, 0);
                    if( MW<1 ) {
                        cout << "Error: wrong value for -MW option (must be >=1)" <<endl;
                        return 0;
                    }
                }
            } else if( !strcmp(argv[i],"-b" ) ) {
                i++;
                if( i>=argc ) {
                    cout << "Error: missing value after -b" <<endl;
                    return 0;
                } else {
                    beta = strtof (argv[i], NULL);
                    if( beta<=0.0 ) {
                        cout << "Error: wrong value for -b option (must be >0)" <<endl;
                        return 0;
                    }
                }
            } else if( !strcmp(argv[i],"-a" ) ) {
                i++;
                if( i>=argc ) {
                    cout << "Error: missing value after -a" <<endl;
                    return 0;
                } else {
                    alpha = strtof (argv[i], NULL);
                    if( alpha<=0.0 ) {
                        cout << "Error: wrong value for -a option (must be >0)" <<endl;
                        return 0;
                    }
                }
            }/* else if( !strcmp(argv[i],"-c" ) ) {
                closed=true;
            } else if( !strcmp(argv[i],"-M" ) ) {
                maximal=true;
            }*/ else if( !strcmp(argv[i],"-m" ) ) {
                i++;
                if( i>=argc ) {
                    cout << "Error: missing value after -m" <<endl;
                    return 0;
                } else {
                    m = strtol (argv[i], NULL, 0);
                    if( m<=1 || m>=INT_MAX) {
                        cout << "Error: wrong value for -m option (must be >1)" <<endl;
                        return 0;
                    }
                }
            } else if( !strcmp(argv[i],"-mg" ) ) {
                i++;
                if( i>=argc ) {
                    cerr << "Error: missing value after -mg" <<endl;
                    return 0;
                } else {
                    mg = strtol (argv[i], NULL, 0);
                    if( mg<=1 || mg>=INT_MAX) {
                        cerr << "Error: wrong value for -mg option (must be >1)" <<endl;
                        return 0;
                    }
                }
            } else if( !strcmp(argv[i],"-h" ) ) {
                cout << "Usage: " << argv[0] << " [-f %d] [-n %d|-cneg] [-m %d] [-c] [-M] [-h] datafile" <<endl;
                cout << "\t -h: print this message" <<endl;
                cout << "\t -f: minimum support (number of transactions)" <<endl;
                cout << "\t -m: maximum length of the patterns" <<endl;
                cout << "\t -mg: max gap constraint (default none)" <<endl;
                cout << "\t -MW: (MAFIA param) maximum number of windows" <<endl;
                cout << "\t -mW: (MAFIA param) minimum number of windows " <<endl;
                cout << "\t -alpha: (MAFIA param) density threshold for dense unit " <<endl;
                cout << "\t -beta: (MAFIA param) merging threshold" <<endl;
                cout << "\t -n: minimal number of bins for adaptive boundaries" <<endl;
                //cout << "\t -c: print only closed patterns" <<endl;
                cout << "\t datafile: transactions of sequences (IBM format)" <<endl;
                cout << "author: T. Guyet (AGROCAMPUS-OUEST)" <<endl;
            } else {
                cout << "Warning: unknown option " << argv[i] << endl;
            }
        } else {
            if(fn==NULL) {
                fn=argv[i];
            } else {
                cout << "Warning: unused datafile " << argv[i] << endl;
            }
        }
        i++;
    }
  
    if(fn==NULL) {
        cout << "Error: no datafile profided" << endl;
        return 0;
    }

    NegProjDB data;
    read_ibm(fn, data);
    
    TCeNSP tcensp((unsigned int)f, (unsigned int)m);
    tcensp.setMaxGap(mg);
    //transmit MAFIA parameters
    if(alpha != 0.0 ) {
        tcensp.setAlpha(alpha);
    }
    if(beta != 0.0 ) {
        tcensp.setBeta(beta);
    }
    if(mW != 0 ) {
        tcensp.setMinWindows(mW);
    }
    if(MW != 0 ) {
        tcensp.setMaxWindows(MW);
    }
    if(min_nbins != 0 ) {
        tcensp.setNbBins(min_nbins);
    }
    
    if( soft ) tcensp.setStrictEmbedding(false);
    tcensp.setMaxItemsetSize(max_negis_size);
    tcensp.setMaxGap(mg);
    
    //run TCeNSP algorithm
    tcensp.run(data);

    //clean transaction in the data structure
    for(vector<const Transaction*>::const_iterator tr=data.database.begin(); tr!=data.database.end();tr++) {
        delete(*tr);
    }

    return 0;
}


