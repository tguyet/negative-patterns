/**
* Implementation of a eNSP/TGSP-like algorithm for mining negative temporal patterns
*
* date (last update): 01/2018
* author: T. Guyet <thomas.guyet@agrocampus-ouest.fr>,
* institution: AGROCAMPUS-OUEST/IRISA
* 
* License: GPL2 (Gnu General Public License Version 2)
*/

#include "tensp.h"
#include <assert.h>

ostream &operator<<(ostream &os, const TemporalNegativePattern &pattern) {
    os << "<";
    const NegativePattern &s = pattern.symbolic_signature;
    unsigned int l = s.negs.size();
    unsigned int tp_id=0;
    unsigned int i=0;
    if( s.negs[i] ) {
        os<< " -(" << s.pp[i] <<")";
    } else {
        os << " (" << s.pp[i] <<") [" << pattern.temporal_projection[tp_id].first << "," << pattern.temporal_projection[tp_id].second << "] ";
        tp_id++;
    }
    i++;
    for(; i<l; i++) {
        if( s.negs[i] ) {
            os<< ", -(" << s.pp[i] <<")";
        } else {
            if( tp_id<pattern.temporal_projection.size() ) {
                os << ", (" << s.pp[i] <<") [" << pattern.temporal_projection[tp_id].first << "," << pattern.temporal_projection[tp_id].second << "] ";
                tp_id++;
            } else {
                os << ", (" << s.pp[i] <<") ";
            }
        }
    }
    os << ">";
    return os;
}

#if TIMESTAMPS
TeNSP::TeNSPCallable::TeNSPCallable( void (*f)(const Callable *, const GenericPattern &, const vector<vector<float> >&) ) {
    fp=f;
}
#endif

void TeNSP::setAlpha(float alpha) {
    Options::options().alpha = alpha;
}

void TeNSP::setBeta(float beta) {
    Options::options().beta = beta;
}

void TeNSP::setMinWindows(unsigned int mW) {
    Options::options().min_nwindows = mW;
}

void TeNSP::setMaxWindows(unsigned int MW) {
    Options::options().max_nwindows = MW;
}

void TeNSP::setNbBins(unsigned int n) {
    Options::options().min_nbins = n;
}

TeNSP::TeNSP(unsigned int _min_sup, unsigned int _max_pat, unsigned int _ns,  list< TemporalNegativePattern > *patlist) :
    ensp(_min_sup, _max_pat, _ns)
#if TIMESTAMPS
    , caller(&TeNSP::process_temporal_pattern)
#endif
    {
#if TIMESTAMPS
    //MAFIA options
    Options::init();
    
    Options::options().flags=OptionUseBitmaps;
    
    //Branching the callback
    ensp.setCallback( &caller );
    caller.threshold = (unsigned int)_min_sup;
    caller.print_patterns = true;
    caller.tpatterns=patlist;
#endif
}

unsigned int TeNSP::run(ProjDB &data) {
    //run algorithm
    return ensp.run(data);
}

#if TIMESTAMPS

/**
* @param caller
* @param pattern (must be a NegativePattern is the case of tensp !!)
* @param temporal_projection list of all temporal projection of the pattern (constructed by ensp)
*/
void TeNSP::process_temporal_pattern(const Callable* caller, const GenericPattern &pattern, const vector<vector<float> >&temporal_projection) {
    // cast the parameters of the generic caller corresponding to the specific values of TGSP
    TeNSP::TeNSPCallable *tcaller = (TeNSP::TeNSPCallable*)(caller); //Warning reverse-casting
    NegativePattern & symbolic_signature = (NegativePattern &)pattern; //warning reverse-casting
    
    if( temporal_projection.size() < tcaller->threshold) {
        return;
    }
    
    //NB: in the following variable name "n", "d" and "ps" must not be changed cause of PS macro (in the MAFIA algorithm)
    unsigned int n=temporal_projection.size(); 
    if( n==0 ) return;
    int d=symbolic_signature.size() - symbolic_signature.nbn -1;
    if( d<=0 ) return; //case of empty pattern
    
    //Time-gap table construction (different from TGSP)
    float *ps = (float*)bulk_alloc( sizeof(float)*n*d );
    for(unsigned int i = 0; i<n;i++) {
        for(unsigned int idim=0; idim<(unsigned int)d; idim++) {
            PS(i, idim) = temporal_projection[i][idim+1]-temporal_projection[i][idim];
        }
    }
    
	const Options &opts = Options::options();
	vector< cluster_rule<float> > du_clusters= mafia_solve(ps, n, d, opts);
    
    vector< cluster_rule<float> >::iterator itclust = du_clusters.begin();
    while( itclust != du_clusters.end() ) {
        if( itclust->nbpoints < tcaller->threshold ) {
            itclust = du_clusters.erase(itclust); 
        } else {
            //The cluster yields a new temporal pattern
            if( tcaller->tpatterns ) {
                TemporalNegativePattern tpattern;
                tpattern.symbolic_signature=symbolic_signature;
                tpattern.temporal_projection = itclust->intervals;
                tcaller->tpatterns->push_back( tpattern );
                if( tcaller->print_patterns ) {
                    //cout <<  tpattern << ": "<< itclust->nbpoints << "/" << itclust->nbCdus << endl;
                    cout <<  tpattern << " [f="<< itclust->nbpoints << "]"<<endl;//
                }
            } else if ( tcaller->print_patterns ) {
                TemporalNegativePattern tpattern;
                tpattern.symbolic_signature=symbolic_signature;
                tpattern.temporal_projection = itclust->intervals;
                //cout <<  tpattern << ": "<< itclust->nbpoints << "/" << itclust->nbCdus << endl;
                cout <<  tpattern << " [f="<< itclust->nbpoints << "]"<<endl;//
            }
            
            itclust++;
        }
    }

    bulk_free(ps);
}
#endif


