/**
 Temporal sequences

 Author: Thomas Guyet <thomas.guyet@agrocampus-ouest.fr>

 Date (last update): 01/2018
 
 License: GPL2 (Gnu General Public License Version 2)
*/

#ifndef SEQUENCE_H
#define SEQUENCE_H


#include "options.h"

#include <iostream>
#include <ostream>
#include <vector>
#include <string>
#include <fstream>
#include <set>

using namespace std;

typedef vector<unsigned int> itemset; //<! sorted list of itemsets
enum {IS_NOINC_STRICT,IS_NOINC_SOFT};

extern unsigned int itemset_noinc_mode;

class GenericPattern {};

#if COMPUTE_SID
class sequence : public vector<itemset>, public GenericPattern { //TODO change this !
public:
    set<unsigned int> sid;
    unsigned int support() const {return sid.size();};
#if TIMESTAMPS
    vector<float> timestamps;
#endif

    void printJSON( std::ostream & ) const;
};
#else
typedef vector<itemset> sequence;
#endif

ostream& operator<<(ostream &os, const itemset &is);

ostream& operator<<(ostream &os, const sequence &s);

bool operator<(const sequence &s1, const sequence &s2);

/**
* test whether itemset is1 is included in the itemset is2
*
* An itemset \p is1 is included in the itemset \p is2 iff all \p is1 is a subset of \p is2 (all items of is1 are in is2)
*/
bool inc(const itemset &is1, const itemset &is2);


/**
* test whether itemset is1 is NOT included in the itemset is2
* 
* Here, depending on the \p itemset_noinc_mode value, not being included has two semantics
* <li> strict (0): none of the is1 items are in is2
* <li> soft (1): there exists an item e in is1 which is not in is2
*/
bool noinc(const itemset &is1, const itemset &is2);

void merge(itemset &cumulis, const itemset &is);



/**
* compute the itemset intersection
*/
itemset inter(const itemset &is1, const itemset &is2);


/**
* test whether item i is included in the itemset is2
*/
bool inc(unsigned int i, const itemset &is2);

#endif
