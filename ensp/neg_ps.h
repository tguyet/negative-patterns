/**
 NegPS --- Implementation of a depth-first generate & test algorithm for mining negative sequential patterns

 Author: Thomas Guyet <thomas.guyet@agrocampus-ouest.fr>

 Date (last update): 01/2018
 
 License: GPL2 (Gnu General Public License Version 2)
*/


#ifndef CENSP_H
#define CENSP_H

#include "ensp.h"

#include <vector>
#include <list>
#include <iostream>

struct triple {
    unsigned int first /*sed id*/, second /*itemset id*/, prev_is_id;
    
    triple(unsigned int f, unsigned int s, unsigned int t) {
        first=f;second=s;prev_is_id=t;
    }
};

struct NegProjDB {
  vector<const Transaction*> database;
  vector< triple > indexes; //projection indexes
  void clear() {
    database.clear(); //do not clean the shared transactions !! 
    indexes.clear();
  }
  map<string,unsigned int> dictionnary;
  map<unsigned int,string> revdictionnary;
  bool usedict;
};

void read(const string &_filename, NegProjDB &pairdata);
void read_ibm(const string &_filename, NegProjDB &pairdata);

class CeNSP {
  list< NegativePattern > negpats;               //list of frequent negative patterns
  unsigned int min_sup;
  unsigned int max_pat;
  unsigned int max_gap;
  NegativePattern current_pattern;
  ostream &_os;
  bool print_json;

  map<unsigned int,unsigned int> fitems; //list of frequent items

  //bool negitems_only; // if true, negations only holds items (and not itemsets)
  unsigned int max_negis_size;
public:
  CeNSP(unsigned int _min_sup, unsigned int _max_pat, ostream &os=cout) : min_sup(_min_sup), max_pat(_max_pat), max_gap(0), _os(os), print_json(false), max_negis_size(1){
    current_pattern.strict_embedding=true;
  };
  virtual ~CeNSP(){};
  void output_pattern(const NegProjDB &);
  
  const list< NegativePattern>& patterns() const {return negpats;};
  
  void setMaxItemsetSize(unsigned int val=1) {max_negis_size=val;};
  void setMaxGap(unsigned int mg) {max_gap=mg;};
  void setStrictEmbedding(bool val=true) {current_pattern.strict_embedding=val;};
  
  void output_json(bool v=true){print_json=v;};
  
  unsigned int run(NegProjDB &data);
  
  static void get_subsets(vector<itemset> &subsets, vector<unsigned int > &v, vector<unsigned int > currentpos, int max_size, int level = 0, int pos=-1);
protected:
  unsigned int project(NegProjDB &data);
};

#endif
