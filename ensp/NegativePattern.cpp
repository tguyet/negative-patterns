/**
 Implementation of Negative Pattern functions used by eNSP and TeNSe

 Author: Thomas Guyet <thomas.guyet@agrocampus-ouest.fr>

 Date (last update): 01/2018
 
 License: GPL2 (Gnu General Public License Version 2)
*/


#include "NegativePattern.h"

//////////////////// pattern matching ////////////////

NegativePattern::NegativePatternOccurrence NegativePattern::find(const sequence &s, unsigned int startpos) const{
    NegativePatternOccurrence current;
    current.maxpos=s.size();
    current.pat_pos=0;
    return find_rec(current, s, startpos);
}

NegativePattern::NegativePatternOccurrence NegativePattern::find_rec(NegativePattern::NegativePatternOccurrence current, const sequence &s, unsigned int startpos) const {
    if( current.pat_pos>=pp.size() ) {
        //if there is a negative item at the end, it must be checked that the last negative item is at the end (after last position) !
        if(negs[pp.size()-1] && current.maxpos!=s.size()) {
            return NegativePatternOccurrence();
        }
        return current;
    }
    
    const itemset &next_itemset = pp[current.pat_pos];
    
    if( negs[current.pat_pos] ) {
        //otherwise, it means that there is two consecutive negative events in the pattern p
        //case of a negative item: looking for its first occurrence to bound the search of next item
        itemset cumulated_items;
        for(unsigned int pos=startpos; pos<current.maxpos; pos++) {
            if( strict_embedding ) {
                if(not noinc(next_itemset,s[pos]) ) {
                    current.maxpos=pos+1;
                    current.pat_pos++;
                    return find_rec(current, s, startpos); // stop when the first occurrence of the negative itemset has been found !
                                                           // If it occurs for position pos it occurs for all larger position!
                }
            } else {
                merge(cumulated_items, s[pos]);
                if(not noinc(next_itemset,cumulated_items) ) { // stop as sound as negative itemset has been found in the cumulative itemset !
                    // HACK: to be replaced by a "not notinc(cumulated_items,s[pos])" ??
                    current.maxpos=pos+1;
                    current.pat_pos++;
                    return find_rec(current, s, startpos);  
                }
            }
        }
        //HERE: no occurrence of the negative itemset has been found: continue with an upper bound corresponding to the sequence end
        if(mg==0) {
            current.maxpos=s.size();
        } else {
            current.maxpos=(s.size()<mg+startpos-1?s.size():mg+startpos-1);
        }
        current.pat_pos++;
        return find_rec(current, s, startpos); 
    } else {
        //looking for an itemset ... but it can not be farest than the current.maxpos position (due to negations)!
        for(unsigned int pos=startpos; pos<current.maxpos; pos++) {
            if( inc(next_itemset, s[pos]) ) { //itemset inclusion test
                unsigned int oldmaxpos=current.maxpos;
                //next temporal constraint is the end of the sequence:
                if(mg==0) {
                    current.maxpos=s.size();
                } else {
                    current.maxpos=(s.size()<mg+startpos-1?s.size():mg+startpos-1);
                }
                current.pat_pos++;
                current.ppos.push_back(pos);
                NegativePatternOccurrence ret=find_rec(current, s, pos+1);
                if( ret.ppos.size()>0 ) {
                    return ret;
                }// else, the position for this itemset matching didn't succeed in matching the remainder of the pattern, so, we try another one: backtrack!
                current.ppos.pop_back();
                current.maxpos=oldmaxpos;
                current.pat_pos--;
            }
        }
        //not any occurrence has been found from position of earlier itemsets: backtrack !
        return NegativePatternOccurrence();
    }
}

/**
* return 0 if the sequence does not hold the patterns, otherwise it returns the position of the last 
*/
bool operator<(const sequence &s, const NegativePattern &p) {
    return (p.find(s).ppos.size()>0);
}

/*
list<NegativePatternOccurrence> occurrences(const sequence &s, const NegativePattern &p) {
    list<NegativePatternOccurrence> occs;
    findall_rec(current, s, p, false)
}
*/

/**
output the NegativePattern in JSON format

For instance, pattern <a-b(ce)-(ad)> will be output
{
    "pattern":[ {
        "ids":[1],
        "neg":"f"
    },
    {
        "ids":[2],
        "neg":"f"
    },
    {
        "ids":[3,5],
        "neg":"f"
    },
    ...
    ]
}
*/
void NegativePattern::printJSON(std::ostream &os) const {
    os << "{\"pattern\":[";
    unsigned int l = negs.size();
    if( l!=0 ) {
        os << "{\"ids\":["<< pp[0] <<"],\"neg\":";
        if( negs[0] ) os<< "true";
        else os<< "false";
        os<< "}";
    }
    for(unsigned int i=1; i<l; i++) {
        os << ",{\"ids\":["<< pp[i] <<"],\"neg\":";
        if( negs[i] ) os<< "true}";
        else os<< "false}";
    }
    os << "]}";
}

ostream& operator<<(ostream &os, const NegativePattern &s) {
    unsigned int l = s.negs.size();
    if( l!=0 ) {
        if( s.negs[0] ) os<< "-";
        os << "(" << s.pp[0] <<")";
    }
    for(unsigned int i=1; i<l; i++) {
        os << ",";
        if( s.negs[i] ) os<< "-";
        os << "(" << s.pp[i] <<")";
    }
#if TIMESTAMPS
    if( l!=0 && s.hasts && s.timestamps.size()>0 ) {
        os << "[";
        for(unsigned int i=0;i<s.timestamps.size()-1;i++) { //Je mets -1, parce que la dimension est liées aux timestamps, mais on a en fait les délais (1 de moins !!)
            os << s.timestamps[i].first << "," << s.timestamps[i].second <<";";
        }
        os << "]";
    }
#endif
    return os;
}

