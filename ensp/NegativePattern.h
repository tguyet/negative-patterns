/**
 Definition of negative patterns for eNSP and TeNSe algorithms

 Author: Thomas Guyet <thomas.guyet@agrocampus-ouest.fr>

 Date (last update): 01/2018
 
 License: GPL2 (Gnu General Public License Version 2)
*/

#ifndef NEGATIVE_PATTERN_H
#define NEGATIVE_PATTERN_H


#include "prefixspan.h"
#include "Sequence.h"
#include <vector>
#include <list>
#include <iostream>

using namespace std;

class NegativePattern : public GenericPattern {
public:
    sequence pp;        // positive partner
    vector<bool> negs;  // negations
    unsigned int nbn;   // number of negative itemsets
    unsigned int ppsup; // positive partner support
    unsigned int sup;   // pattern support
    bool strict_embedding;  // itemset embedding method (is true, use a strict embedding: none of the items must occur) vs soft
    unsigned int mg;        // maxgap constraint
#if TIMESTAMPS
    vector< pair<float,float> > timestamps; //used for TCeNSP, but not for TeNSP !!
    bool hasts;
#endif
    
    NegativePattern():GenericPattern(), nbn(0), strict_embedding(true), mg(0)
#if TIMESTAMPS
    , hasts(false)
#endif
    {};
    
    unsigned int size() {
        return pp.size();
    }
    
    itemset &back() {
        return pp.back();
    }
    
    void push_back(const itemset &is, bool neg =false) {
        pp.push_back(is);
        negs.push_back(neg);
        nbn += (neg?1:0);
    }
    
    void pop_back() {
        pp.pop_back();
        if( negs.back() ) nbn--;
        negs.pop_back();
    }

    /////////// some additional facilities to extract occurrences of a negative sequential pattern ///////////

    typedef struct {
        vector<unsigned int> ppos;
        unsigned int pat_pos;
        unsigned int maxpos;
    } NegativePatternOccurrence;
    
    NegativePatternOccurrence find(const sequence &s, unsigned int startpos=0) const;
    
    void printJSON(std::ostream &os) const;
    
private:
    /**
    * \param current description of the current pattern occurrence, the length of current.ppos gives the number of positive positions that have been correctly mapped. While current.ppos has the same size has the positive part of p, then one occurrence has been found !
    * \param s sequence in which the pattern p is looked for
    * \param startpos (default 0), position in the sequence to start the search with! If lower than the last position in the sequence of the current occurrence, this parameter has no effect ... otherwise it enables to skip itemsets in the sequence (this can be used to identify all occurrences of a negative pattern in a sequence)
    *
    * The behavior of the function also depends on the value of \p mg (max gap constraint) and \p strict_embedding, which indicates in which manner the negative itemsets have to be evaluated
    */
    NegativePatternOccurrence find_rec(NegativePatternOccurrence current, const sequence &s, unsigned int startpos=0) const;
};

ostream& operator<<(ostream &os, const NegativePattern &s);

bool operator<(const sequence &s, const NegativePattern &p);

#endif
