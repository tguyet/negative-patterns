/**
 PrefiSpan --- An Implementation of Prefix-projected Sequential Pattern mining

 Author: Thomas Guyet <thomas.guyet@agrocampus-ouest.fr>

 Date (last update): 01/2018
 
 License: GPL2 (Gnu General Public License Version 2)

 Reference:
  PrefixSpan: Mining Sequential Patterns Efficiently by Prefix-Projected Pattern Growth
  Jian Pei, Jiawei Han, Behzad Mortazavi-asl, Helen Pinto, Qiming Chen, Umeshwar Dayal and Mei-chun Hsu
  IEEE Computer Society, 2001, pages 215
  
 Todo:
  Possible improvment: while constructing the list of items by which a sequence could be extend, I don't take into account the maxgap !
*/

#include "prefixspan.h"
#include <set>
#include <algorithm>
#include <sstream>


void Prefixspan::output_pattern(const ProjDB &projected) {
#if OUTPUT_SHOW>0
    if( print_json ) {
        pattern.printJSON( _os );
        if( pattern.size()!=0) _os << ",";
        _os << endl;
    } else if(not nooutput) {
        _os << "PF:" << pattern << ": " << projected.database.size() << endl;
    }
#endif
#if OUTPUT_SHOW>1
    //transaction list
    _os << endl << "( ";
//    for (vector<const Transaction*>::const_iterator it = projected.database.begin(); it != projected.database.end(); it++) {
    for (unsigned int i = 0; i < projected.database.size(); i++) {
        _os << projected.database[i]->first << " ";
    }
    _os << ") : " << projected.database.size() << endl;
#endif
    
#if COMPUTE_SID
    pattern.sid.clear();
    vector<unsigned int> trans_id;
    //for (vector<const Transaction*>::const_iterator it = projected.database.begin(); it != projected.database.end(); it++) {
    for (unsigned int i = 0; i < projected.database.size(); i++) {
        unsigned int tid = projected.database[i]->first;
        pattern.sid.insert( tid );
    }

#if TIMESTAMPS
    //Callback launch when a pattern is extracted with a temporal projection
    if( caller && caller->fp) {
        (*(caller->fp))(caller, pattern, projection);
    }
#endif
#endif


    if(pfpatterns) {
        pfpatterns->push_back( pair<sequence, unsigned int >(pattern, projected.database.size()) ); //couple pattern and its support
    }
}

unsigned int Prefixspan::run(ProjDB &projected) {
    #if TIMESTAMPS
    //prepare the projection vector: same size as the dataset (number of sequences)
    projection.resize( projected.database.size() );
    #endif
    
    #if OUTPUT_SHOW>0
    if( print_json ) {
        _os << "{\"patterns\":["<<endl;
    }
    unsigned int ret=project(projected);
    
    if( print_json ) {
        _os << "]}"<<endl;
    }
    return ret;
    #else
    return project(projected);
    #endif
}


/********************************************************************
 * Project database
 ********************************************************************/
unsigned int Prefixspan::project(ProjDB &projected) {
    if (projected.database.size() < min_sup) return 0;
    
    if (max_pat != 0 && pattern.size() == max_pat) {
        output_pattern( projected );
        return projected.database.size();
    }

    map<unsigned int, unsigned int> map_item;     // seqs
    map<unsigned int, unsigned int> map_item_sd;  // composition
    const vector<const Transaction*> &database = projected.database;
    for (unsigned int i = 0; i < database.size(); i++) {
        const sequence &seq = database[i]->second;

        //evaluating the possible compositional extensions
        unsigned int iter = projected.indexes[i].first;
        //Here iter is the position of the projection in the seq i
        if( pattern.size()!=0 ) {
            //Here, j is the position of the projection in the itemset at position iter of the seq i
            for (unsigned int j=projected.indexes[i].second; j<seq[iter].size(); j++) {
                map_item_sd[ seq[iter][j] ]++;
            }
            //HACK Warning TG: not complete !!!
            //      -> you may imagine that there exists further itemsets containing the last
            //         itemset of the current pattern and which hold alternative items !!
        }
        
        iter++; //next itemset
        //evaluating the possible sequential extensions
        for (; iter < seq.size(); iter++) {
            set<unsigned int> found_items;
            for (itemset::const_iterator item=seq[iter].begin(); item!=seq[iter].end(); item++) {
                found_items.insert( *item );
            }
            for( set<unsigned int>::iterator item=found_items.begin();item!=found_items.end();item++) {
                map_item[ *item ]++;
            }
        }
    }
 

    //creation of a new data on which project
    ProjDB db_sd;   //db_sd is the new database
    db_sd.usedict=projected.usedict;
    vector<const Transaction *> &new_database_sd = db_sd.database; //new_database_sd is a reference-shortcut to the database
    vector< projection_pointer > &new_indexes_sd = db_sd.indexes; //new_indexes_sd is a reference to the list of indexes of the new database to create
    bool closed=true; // <=> not exists any super-pattern that covers the same set of transactions (same support)
    bool maximal =true;
    
    //Composition (not at the beginning)
    if( pattern.size()!=0 ) {
        for (map<unsigned int, unsigned int>::iterator it_1 = map_item_sd.begin(); it_1 != map_item_sd.end(); it_1++) {
            if( it_1->second<min_sup) continue; //prune this extension: not frequent in the projected database!
            
            pattern.back().push_back(it_1->first);
            bool toclose=true;
            for (unsigned int i = 0; i < database.size(); i++) {
                const Transaction &transaction = *database[i];
                const sequence &seq = transaction.second;
                
                unsigned int iter = projected.indexes[i].first;
                bool extended=false;
                unsigned int j=projected.indexes[i].second;
                for (; j<seq[iter].size() && seq[iter][j] >= it_1->first; j++) {
                    if ( seq[iter][j] == it_1->first) {
                        new_database_sd.push_back( &transaction );
                        new_indexes_sd.push_back( projection_pointer(iter, j+1, projected.indexes[i].max_pos) );
                        extended=true;
                        break;
                    }
                }
                
                //the itemset can be later in the sequence !!
                if( !extended ) {
                    iter++; //go to next item
                         
                    for(;iter<projected.indexes[i].max_pos; iter++) {
                        itemset &is = pattern.back();
                        itemset::iterator itis=is.begin();
                        for(j=0;j<seq[iter].size(); j++) {
                            unsigned int item = seq[iter][j];
                            if( item == *itis ) {
                                itis++;
                            } else if (item > *itis || item>it_1->first ) {
                                //uses the item order in the itemsets
                                break;
                            }
                            if( itis==is.end() && item == it_1->first ) {
                                new_database_sd.push_back( &transaction );
                                
                                unsigned int new_max_pos = projected.indexes[i].max_pos+j+1-projected.indexes[i].first;
                                new_max_pos = (new_max_pos<seq.size()?new_max_pos:seq.size());
                                
                                new_indexes_sd.push_back( projection_pointer(iter, j+1, new_max_pos) );
#if COMPUTE_SID && TIMESTAMPS
                                //update the timestamp of the temporal projection
                                projection[ transaction.first ].back() = seq.timestamps[ iter ] ;
#endif
                                extended=true;
                                break;
                            }
                        }
                        if(extended) break;
                    }
                }
                if(!extended) toclose=false;
            }
            // si toclose==true, alors l'extension avec it1->first conduit a un motif avec le même support
            // pattern n'est alors plus intéressant !
            if( toclose) closed=false;
            
            //pattern.back().push_back(it_1->first);
            int nb = project(db_sd); // recursive call
            pattern.back().pop_back();
#if COMPUTE_SID && TIMESTAMPS
            // Timestamp may have change ... we put systematically the "old" values
            vector< projection_pointer >::iterator itproj=projected.indexes.begin();
            vector< const Transaction* >::iterator ittrans=projected.database.begin();
            while( itproj!=projected.indexes.end() ) {
                projection[ (*ittrans)->first ].back() = (*ittrans)->second.timestamps[ itproj->first ] ;
                itproj++;
                ittrans++;
            }
#endif
            db_sd.clear();
            if(nb>0) maximal=false;
        }
    }
    
    
    
    //sequential extension of the pattern
    ProjDB db;
    db.usedict=projected.usedict;
    vector<const Transaction*> &new_database = db.database;
    vector< projection_pointer > &new_indexes = db.indexes;
    for (map<unsigned int, unsigned int>::iterator it_1 = map_item.begin(); it_1 != map_item.end(); it_1++) {
        bool toclose=true;
        if(it_1->second<min_sup) continue; //prune this extension: not frequent in the projected database!
    
        for (unsigned int i = 0; i < database.size(); i++) {
            const Transaction &transaction = *database[i];
            const sequence &seq = transaction.second;
            bool extended=false;
            
            
            for (unsigned int iter = projected.indexes[i].first+1; iter < projected.indexes[i].max_pos ;iter++) {
                extended=false;
                
                for (unsigned int j=0; j<seq[iter].size() /*itemset size*/ && seq[iter][j]<=it_1->first /*ordered itemsets*/; j++) {
                    if( seq[iter][j] == it_1->first ) {
                        new_database.push_back( &transaction );
                        
                        unsigned int new_max_pos = 0;
                        if( max_gap!=0 ) {
                            new_max_pos = projected.indexes[i].first + max_gap;
                            new_max_pos = (new_max_pos<seq.size()?new_max_pos:seq.size());
                        } else {
                            new_max_pos = seq.size();
                        }
                        
                        new_indexes.push_back( projection_pointer(iter, j+1, new_max_pos) );
#if COMPUTE_SID && TIMESTAMPS
                        //Add a timestamp of the temporal projection of the last item corresponding to the sequence transaction->first
                        projection[ transaction.first ].push_back( transaction.second.timestamps[ iter ] );
#endif
                        extended=true;
                        break;
                    }
                }
                if(extended) break;
            }
            if(!extended) toclose=false;
        }
        if( toclose) closed=false; //il existe un motif plus grand qui supporte les mêmes sequences que pattern

        itemset is;
        is.push_back( it_1->first );
        pattern.push_back( is );
#if COMPUTE_SID && TIMESTAMPS
        pattern.timestamps.push_back( 0.0 );
#endif
        int nb = project(db);//recursive call
        pattern.pop_back();
#if COMPUTE_SID && TIMESTAMPS
        pattern.timestamps.pop_back();
        
        //Unstack the last temporal value of the temporal projection
        //      'new_database[i]->first' is the transaction id of one of the transactions whose just be inserted
        for(unsigned int i=0; i<new_database.size(); i++) {
            projection[ new_database[i]->first ].pop_back();
        }
#endif
        db.clear();
        if(nb>0) maximal=false;
    }
    
    
    if (print_maximal_only ) {
        if( maximal ) {
            output_pattern(projected);
        }
    } else if( print_closed_only ) {
        if( closed ) {
            output_pattern(projected);
        }
    } else {
        output_pattern(projected);
    }
    
    
    return projected.database.size();
}


////////////////  FILE READ FUNCTIONS ////////////////////

void read_ibm(const string &_filename, ProjDB &pairdata) {
    string       line;
    int          item;
    unsigned int id = 0, nb =0, pid=0;
    unsigned int timestamp = 0;

    ifstream is( _filename.c_str() );
    Transaction *transaction = NULL;
    sequence *seq=NULL;
    
    pairdata.usedict=false;
    
    //float mean_length=0;
    
    while( getline(is, line) ) {
        stringstream ss(line.c_str());
    
        //sequence id
        if( not (ss >> id) ) {
            continue;
        }
        
        //itemset timestamp
        if( not (ss >> timestamp) ) {
            continue;
        }
        
        //itemset size
        if( not (ss >> nb) ) {
            continue;
        }
    
        if( !transaction ) {
            //create new transaction
            transaction = new Transaction();
            seq = &(transaction->second);
            if(id!=0) {
                cerr << "warning: sequence ids must start with 0" << endl;
            }
            pid=0;
        } else if (id != pid ) {
            if( transaction->second.size()>0 ) {
                transaction->first = pairdata.database.size();
                //mean_length += transaction->second.size();
                pairdata.database.push_back(transaction);
                pairdata.indexes.push_back( projection_pointer(-1,0,transaction->second.size()) );
            } else {
                delete(transaction);
            }
            
            if( (id-1)!=pid ) {
                cerr << "warning: sequence ids are not contigues, expected transaction id:" << id-1 << endl;
            }
            
            //create new transaction
            transaction = new Transaction();
            pid=id;
            seq = &(transaction->second);
        }
        
        //read the itemset
        itemset is;
        while (ss >> item) {
            is.push_back( item );
        }
        
        if( nb!=is.size() ) {
            cerr << "warning: itemset size invalid" << endl;
        }
        
        sort( is.begin(), is.end() );
        unique( is.begin(), is.end() ); //ensure itemset without repetitions
        seq->push_back( is );
#if TIMESTAMPS
        seq->timestamps.push_back( timestamp );
#endif
    }
    //add the last sequence
    if( transaction && transaction->second.size()>0 ) {
        transaction->first = pairdata.database.size();
        //mean_length += transaction->second.size();
        pairdata.database.push_back(transaction);
        pairdata.indexes.push_back( projection_pointer(-1,0,transaction->second.size()) );
    }
    
    /*
    mean_length /= pairdata.database.size();
    float var_length=0;
    for(vector<const Transaction*>::iterator it=pairdata.database.begin(); it!=pairdata.database.end();it++) {
        var_length += (mean_length - (*it)->second.size() ) * (mean_length - (*it)->second.size() );
    }
    var_length = sqrt( var_length/ pairdata.database.size() );
    cout << mean_length<< ", " << var_length << endl;
    */
}

void read(const string &_filename, ProjDB &pairdata) {
    string       line;
    int          item;
    unsigned int id = 0;
    #if TIMESTAMPS
    unsigned int timestamp = 0;
    #endif

    ifstream is(_filename.c_str());
    Transaction *transaction = new Transaction();
    while (getline (is, line)) {
        sequence &seq= transaction->second;
        stringstream ss(line.c_str());
#if TIMESTAMPS
        timestamp = 0;
#endif

        while (ss >> item) {
            itemset is;
            is.push_back( item );
            seq.push_back( is );
#if TIMESTAMPS
            seq.timestamps.push_back( ++timestamp );
#endif
        }

        transaction->first = id++;
        pairdata.usedict=false;
        pairdata.database.push_back(transaction);
        pairdata.indexes.push_back( projection_pointer(-1,0,transaction->second.size()) );
        transaction = new Transaction();
    }
}




