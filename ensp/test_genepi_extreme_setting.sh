#!/bin/bash


##### Patterns extraction
fmin=700
mlength=3
mgap_negpspan=3
ratio_ensp=1
fpos=$(echo "scale=4;$fmin*$ratio_ensp/10" | bc)
# Extraction des motifs avec NegPSpan
./prefixspan -cneg -f $fmin -m $mlength -mg $mgap_negpspan -miss 1 db-pos.db 2> /dev/null > output_negpspan.pat

# Extraction des motifs avec eNSP

./prefixspan -no -n $fmin -f $fpos -m $mlength db-pos.db 2> /dev/null > output_ensp.pat

###### Tests
echo "-------------------------------------------------"
pattern_shape="(158),-.*(158)"
echo "extracted by eNSP:"
grep $pattern_shape output_ensp.pat
echo "extracted by NegPSpan:"
grep $pattern_shape output_negpspan.pat
echo "--------------------------------------------------"



##### Patterns extraction but without maxgap constraints (same other parameters)
mgap_negpspan=0
# Extraction des motifs avec NegPSpan
./prefixspan -cneg -f $fmin -m $mlength -mg $mgap_negpspan -miss 1 db-pos.db 2> /dev/null > output_negpspan.pat
# Extraction des motifs avec eNSP
./prefixspan -no -n $fmin -f $fpos -m $mlength db-pos.db 2> /dev/null > output_ensp.pat

###### Tests
echo "--------------- without maxgap constraints --------------------"
echo "extracted by eNSP:"
grep $pattern_shape output_ensp.pat
echo "extracted by NegPSpan:"
grep $pattern_shape output_negpspan.pat
echo "--------------------------------------------------"

####### Results #######
#-------------------------------------------------
#extracted by eNSP:
# (158),-(87),(158) [f=718]
#extracted by NegPSpan:
#--------------------------------------------------
#--------------- without maxgap constraints --------------------
#extracted by eNSP:
# (158),-(87),(158) [f=718]
#extracted by NegPSpan:
# pattern: (158),-(7),(158): 790
# pattern: (158),-(51),(158): 789
# pattern: (158),-(86),(158): 770
# pattern: (158),-(87),(158): 789
# pattern: (158),-(114),(158): 785
# pattern: (158),-(115),(158): 784
# pattern: (158),-(159),(158): 764
# pattern: (158),-(383),(158): 763
