#!/bin/bash


datasets=("SIGN" "LEVIATHAN" "BIBLE" "BMS2" "BMS1_spmf" "kosarak25k" "MSNBC" )
## 10% thresholds
thresholds=(73     584         3637    7751   5960        2500             3179)
## 5% thresholds
thresholds=(37     292         1818    3875   2980        1250             1589)


datasets=("LEVIATHAN" )
thresholds=(292)

echo "dataset method fmin mlength mgap ratio_ensp miss time memory nbpat nbneg"

id=0
while [ "x${datasets[id]}" != "x" ]
do
    ds=${datasets[${id}]}
    fmin=${thresholds[${id}]}

    mlength=5
    mgap_negpspan=10
    ratio_ensp=7
    miss=1
    # Extraction des motifs avec NegPSpan
    CMD="./prefixspan -cneg -f $fmin -m $mlength -mg $mgap_negpspan -miss $miss -o output_negpspan.pat spmf_datasets/$ds.ibm"
    #echo $CMD
    echo -n "$ds NegPSpan $fmin $mlength $mgap_negpspan $ratio_ensp $miss "
    /usr/bin/time -o tmp.txt -f " %U %M " $CMD 2>/dev/null

    echo -n `more tmp.txt`
    nbpat=`wc -l output_negpspan.pat | cut -d " " -f 1`
    nbneg=`grep "-" output_negpspan.pat | wc -l | cut -d " " -f 1`
    echo " $nbpat $nbneg"

    # Extraction des motifs avec eNSP
    fpos=$(echo "scale=4;$fmin*$ratio_ensp/10" | bc)
    CMD="./prefixspan -no -n $fmin -f $fpos -m $mlength -o output_ensp.pat spmf_datasets/$ds.ibm"
    #echo $CMD
    echo -n "$ds eNSP $fmin $mlength $mgap_negpspan $ratio_ensp $miss "
    /usr/bin/time -o tmp.txt -f " %U %M " $CMD 2>/dev/null

    echo -n `more tmp.txt`
    nbpat=`wc -l output_ensp.pat | cut -d " " -f 1`
    nbneg=`grep "-" output_ensp.pat | wc -l | cut -d " " -f 1`
    echo " $nbpat $nbneg"

    id=$(( $id + 1 ))
done




