/** @file options.cpp implementations of parsing options */

#include "options.h"
#include "utils.h"

#include <assert.h>
#include <getopt.h>
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

// used to parse options
static struct option long_opts[] = {
	{"alpha", required_argument, 0, 'a'},
	{"beta", required_argument, 0, 'b'},
	{"bins", required_argument, 0, 'n'},
	{"min-wins", required_argument, 0, 'u'},
	{"max-wins", required_argument, 0, 'M'},
	{"no-set-dedup", no_argument, 0, 'D'},
	{"no-set-gen", no_argument, 0, 'G'},
	{"no-bitmap", no_argument, 0, 'P'}, 
	{"no-bitmaps", no_argument, 0, 'P'}, 
	{"verbose", no_argument, 0, 'V'},
	{"timing", no_argument, 0, 't'},
	{"device", no_argument, 0, 'd'},
	{"output-points", no_argument, 0, 'p'},
	{"seq", no_argument, 0, 'S'},
	{"sequential", no_argument, 0, 'S'},
	{"help", no_argument, 0, 'h'},
	{0, 0, 0, 0}
};

// global options storage
Options *Options::opts = 0;


Options::Options() 
	: in_path(0), out_path(0), min_nbins(1000), min_nwindows(5), max_nwindows(100),
		alpha(1.5), beta(0.25)
		, flags(OptionSetDedup | OptionUseBitmaps | OptionSetGenUnjoin) {}

Options::Options(int argc, char **argv) 
	: in_path(0), out_path(0), min_nbins(1000), min_nwindows(5), max_nwindows(100),
		alpha(1.5), beta(0.25)
		, flags(OptionSetDedup | OptionUseBitmaps | OptionSetGenUnjoin)
		{
	
	/*
	in_path = (char *)malloc((strlen(argv[optind]) + 1) * sizeof(char));
	strcpy(in_path, argv[optind]);
	
	// form the base output path; do this by removing the extension from the input
	// file name
	char *pdot_char = strrchr(in_path, '.');
	size_t base_len = pdot_char ? pdot_char - in_path : strlen(in_path);
	out_path = (char *)malloc((base_len + 1) * sizeof(char));
	memset(out_path, 0, base_len + 1);
	strncpy(out_path, in_path, base_len);
    //*/
    
	// set the number of OpenMP threads to 1 to force sequential computation
	if(flags & OptionSequential)
		omp_set_num_threads(1);
	
} // Options

void Options::parse_cmdline(int argc, char **argv) {
	opts = new Options(argc, argv);
}  // parse_cmdline


void Options::init() {
	opts = new Options();
}

void Options::parse_double(const char *opt, double *pval) {
	if(!sscanf(optarg, "%lf", pval)) {
		fprintf(stderr, "%s: invalid %s argument, expecting double\n", 
						optarg, opt);
		print_usage(-1);
	}
} // parse_double
 
void Options::parse_int(const char *opt, int *pval, int min_val) {
	if(!sscanf(optarg, "%d", pval)) {
		fprintf(stderr, "%s: invalid %s argument, expecting int\n", 
						optarg, opt);
		print_usage(-1);
	} else if(*pval < min_val) {
		fprintf(stderr, "%d: value is less that minimum allowed %d\n", 
						*pval, min_val);
		print_usage(-1);
	}
} // parse_double

void Options::print_usage(int exit_code) {
	fprintf(stderr, "cppmafia options (GNU option syntax):\n");
	fprintf(stderr, "\t-a,--alpha alpha - dense threshold (alpha)\n");
	fprintf(stderr, "\t-b,--beta beta - window merge threshold (alpha)\n");
	fprintf(stderr, "\t-n,--bins nbins - minimum number of bins\n");
	fprintf(stderr, "\t-u,--min-wins nwins - number of windows for uniform " 
					"dimensions\n");
	//fprintf(stderr, "\t-M,--max-wins nwins - maximum number of windows for " 
	//				"a dimension\n");
	fprintf(stderr, "\t-h,--help - print this message and exit\n");
	fprintf(stderr, "\t-V,--verbose - print intermediate data as the algorithm " 
					"runs\n");
#ifdef MAFIA_USE_DEVICE
	fprintf(stderr, "\t-d,--device - offload some work to device, i.e. GPU\n");
#endif
	exit(exit_code);
}  // print_usage

Options::~Options() {
	free(in_path);
	free(out_path);
} // ~Options
