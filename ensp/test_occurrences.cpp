/**
 Test occurrences

 Author: Thomas Guyet <thomas.guyet@agrocampus-ouest.fr>

 Date (last update): 01/2018
 
 License: GPL2 (Gnu General Public License Version 2)
*/

#include "ensp.h"
#include "prefixspan.h"
#include <vector>


void add_item(unsigned int val, sequence &s) {
    itemset is;
    is.push_back(val);
    s.push_back(is);
}

void add_item(unsigned int val, bool isneg, NegativePattern &p) {
    itemset is;
    is.push_back(val);
    p.pp.push_back(is);
    p.negs.push_back(isneg);
}

int main(int /*argc*/, char **/*argv*/) {
    sequence s;
    
    /*
    itemset a,b;
    
    a.push_back(1);
    b.push_back(2);
    if( a<b ) {
        cout << "true"<<endl;
    } else {
        cout << "false"<<endl;
    }
    if( inc(a,b) ) {
        cout << "true"<<endl;
    } else {
        cout << "false"<<endl;
    }
    return 0;
    //*/
    
    
    add_item(2,s);
    add_item(3,s);
    add_item(1,s);
    add_item(1,s);
    //add_item(2,s);
    add_item(4,s);
    add_item(3,s);
    add_item(4,s);


    NegativePattern p;
    add_item(2,false,p);
    add_item(1,true ,p);
    add_item(3,false,p);
    
    if( s<p ) {
        cout << "occurrence found" <<endl;
    } else {
        cout << "not found" <<endl;
    }
    
    
    NegativePattern p2;
    add_item(2,false,p2);
    add_item(1,true ,p2);
    add_item(4,false,p2);
    
    if( s<p2 ) {
        cout << "occurrence found" <<endl;
    } else {
        cout << "not found" <<endl;
    }
    
    return 0;
}
