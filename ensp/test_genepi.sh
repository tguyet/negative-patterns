#!/bin/bash


##### Patterns extraction
fmin=80
mlength=3
mgap_negpspan=5
ratio_ensp=7
# Extraction des motifs avec NegPSpan
./prefixspan -cneg -f $fmin -m $mlength -mg $mgap_negpspan -miss 3 db-neg.db 2>/dev/null > output_negpspan.pat

# Extraction des motifs avec eNSP
fpos=$(echo "scale=4;$fmin*$ratio_ensp/10" | bc)

./prefixspan -no -n $fmin -f $fpos -m $mlength db-neg.db 2>/dev/null > output_ensp.pat

###### Tests
echo "-------------------------------------------------"
pattern_shape="(383).*(383)"

echo "extracted by eNSP:"
grep $pattern_shape output_ensp.pat
echo "extracted by NegPSpan:"
grep $pattern_shape output_negpspan.pat

#grep $pattern_shape output_negpspan.pat > filtered_negpspan.pat 
#grep $pattern_shape output_ensp.pat > filtered_ensp.pat 

#diff filtered_ensp.pat filtered_negpspan.pat
echo "--------------------------------------------------"



