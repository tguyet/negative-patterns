/**
 TeNSe --- Implementation of a depth-first generate & test algorithm for mining negative temporal patterns

 Author: Thomas Guyet <thomas.guyet@agrocampus-ouest.fr>

 Date (last update): 01/2018
 
 License: GPL2 (Gnu General Public License Version 2)
*/

#ifndef TCENSP_H
#define TCENSP_H

#include "neg_ps.h"
#include "cppmafia/options.h"

#include <vector>
#include <list>
#include <iostream>

class TCeNSP {
private:
  list< NegativePattern > negpats;               //list of frequent negative patterns
  unsigned int min_sup;
  unsigned int max_pat;
  unsigned int max_gap;
  NegativePattern current_pattern;
  ostream &_os;
  
#if TIMESTAMPS
  vector<vector<float> > projection; //! Description of the temporal projection of the current pattern 
  void process_temporal_pattern(const vector<const Transaction*> &db, const vector< triple > &idx);
#endif

  map<unsigned int,unsigned int> fitems; //list of frequent items
  //bool negitems_only; // if true, negations only holds items (and not itemsets)
  unsigned int max_negis_size;
public:
  TCeNSP(unsigned int _min_sup, unsigned int _max_pat, ostream &os=cout);
  virtual ~TCeNSP(){};
  void output_pattern(const NegProjDB &);
  
  const list< NegativePattern>& patterns() const {return negpats;};
  
  
  void setAlpha(float alpha){
    Options::options().alpha = alpha;
  };
  void setBeta(float beta) {
    Options::options().beta = beta;
  };
  void setMinWindows(unsigned int mW) {
    Options::options().min_nwindows = mW;
  };
  void setMaxWindows(unsigned int MW) {
    Options::options().max_nwindows = MW;
  };
  void setNbBins(unsigned int n) {
    Options::options().min_nbins = n;
  };
  void setMaxItemsetSize(unsigned int val=1) {max_negis_size=val;};
  void setMaxGap(unsigned int mg) {max_gap=mg;};
  void setStrictEmbedding(bool val=true) {current_pattern.strict_embedding=val;};
  
  unsigned int run(NegProjDB &data);
protected:
  unsigned int project(NegProjDB &data);
  
};

#endif
