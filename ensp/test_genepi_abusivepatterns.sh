#!/bin/bash


##### Patterns extraction
fmin=430
mlength=3
mgap_negpspan=4
ratio_ensp=5
# Extraction des motifs avec NegPSpan
./prefixspan -cneg -f $fmin -m $mlength -mg $mgap_negpspan -miss 1 db-pos.db > output_negpspan.pat

# Extraction des motifs avec eNSP
fpos=$(echo "scale=4;$fmin*$ratio_ensp/10" | bc)

./prefixspan -no -n $fmin -f $fpos -m $mlength db-pos.db > output_ensp.pat

###### Tests
echo "-------------------------------------------------"
echo "---------- Abusive pattern example --------------"
pattern_shape="(383).*(383)"

echo "double (383) extracted by eNSP:"
grep $pattern_shape output_ensp.pat
echo "double (383) extracted by NegPSpan"
grep $pattern_shape output_negpspan.pat

echo "positive patterns that includes (86 -- Paracetamol) are not really interesting"

echo "--------------------------------------------------"



