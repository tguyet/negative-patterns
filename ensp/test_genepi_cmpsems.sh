#!/bin/bash


##### Patterns extraction
fmin=1200
mlength=3
mgap_negpspan=0
ratio_ensp=7
miss=2

echo "-------------------------------------------------"
pattern_shape="(383).*(383)"
echo "==> Filtered patterns: $pattern_shape"
echo ""

echo "NegPSpan fmin= $fmin, m=$mlength, mg=$mgap_negpspan, miss=$miss"
./prefixspan -cneg -f $fmin -m $mlength -mg $mgap_negpspan -miss $miss db-pos.db 2>/dev/null > output_negpspan.pat
grep $pattern_shape output_negpspan.pat

echo "NegPSpan fmin= $fmin, m=$mlength, mg=$mgap_negpspan, miss=$miss, softinc"
./prefixspan -cneg -f $fmin -m $mlength -mg $mgap_negpspan -miss $miss -softinc db-pos.db 2>/dev/null > output_negpspan.pat
grep $pattern_shape output_negpspan.pat

echo "NegPSpan fmin= $fmin, m=$mlength, mg=$mgap_negpspan, miss=$miss, softinc/soft"
./prefixspan -cneg -f $fmin -m $mlength -mg $mgap_negpspan -miss $miss -softinc -soft db-pos.db 2>/dev/null > output_negpspan.pat
grep $pattern_shape output_negpspan.pat

echo "--------------------------------------------------"



