/**
Main program for sequential pattern mining
       - sequential patterns
       - ensp (negative sequential patterns)
       - neg_sp (another negative sequential pattern based on prefixspan)

 Author: Thomas Guyet <thomas.guyet@agrocampus-ouest.fr>

 Date (last update): 01/2018
 License: GPL2 (Gnu General Public License Version 2)
*/

#include "prefixspan.h"
#include "ensp.h"
#include "neg_ps.h"
#include <climits>
#include <cstdlib>
#include <sstream>
#include <algorithm>


int main(int argc, char **argv) {
    //list< pair<sequence,unsigned int> > patlist;
    bool closed=false, maximal=false, json=false;
    bool itemseq=false;
    long f=10, m=0, ns=0;
    char *fn=NULL;
    bool negatives=false;
    bool negonly=false;
    int max_negis_size=1;
    bool cneg=false;
    bool soft=false;
    bool is_inc_soft=false;
    char *fout=NULL;
    int mg=0; //maxgap constraint, 0=no constraint

    if(argc<=1) {
        cerr << "Error: a file must be provided" <<endl;
        return 0;
    }

    int i=1;
    while(i<argc) {
        if( argv[i][0]=='-' ) {
            if( !strcmp(argv[i],"-f" ) ) {
                i++;
                if( i>=argc ) {
                    cerr << "Error: missing value after -f" <<endl;
                    return 0;
                } else {
                    f = strtol (argv[i], NULL, 0);
                    if( f<=1 || f>=INT_MAX) {
                        cerr << "Error: wrong value for -f option (must be >1)" <<endl;
                        return 0;
                    }
                }
            } else if( !strcmp(argv[i],"-c" ) ) {
                closed=true;
            } else if( !strcmp(argv[i],"-no" ) ) {
                negonly=true;
            } else if( !strcmp(argv[i],"-cneg" ) ) {
                cneg=true;
            } else if( !strcmp(argv[i],"-softinc" ) ) {
                is_inc_soft=true;
            } else if( !strcmp(argv[i],"-soft" ) ) {
                soft=true;
            } else if( !strcmp(argv[i],"-miss" ) ) {
                i++;
                if( i>=argc ) {
                    cerr << "Error: missing value after -miss" <<endl;
                    return 0;
                } else {
                    max_negis_size = strtol (argv[i], NULL, 0);
                    if( max_negis_size<0 || max_negis_size>=INT_MAX) {
                        cerr << "Error: wrong value for -miss option (must be >=0)" <<endl;
                        return 0;
                    }
                }
            } else if( !strcmp(argv[i],"-I" ) ) {
                itemseq=true;
            } else if ( !strcmp(argv[i],"-o" ) )    {
                i++;
                if(fout==NULL) {
                    if( argv[i][0]=='-' ) {
                        cerr << "Error: output filename can not start with '-' symbol (possible missing filename)." <<endl;
                        return 0;
                    }
                    fout=argv[i];
                } else {
                    cerr << "Warning: a output file as already be given!" <<endl;
                }
            } else if( !strcmp(argv[i],"-n" ) ) {
                negatives=true;
                i++;
                if( i>=argc ) {
                    cerr << "Error: missing value after -n" <<endl;
                    return 0;
                } else {
                    ns = strtol (argv[i], NULL, 0);
                    if( ns<=0 || ns>=INT_MAX) {
                        cerr << "Error: wrong value for -n option (must be >1)" <<endl;
                        return 0;
                    }
                }
            } else if( !strcmp(argv[i],"-M" ) ) {
                maximal=true;
            } else if( !strcmp(argv[i],"-json" ) ) {
                json=true;
            } else if( !strcmp(argv[i],"-m" ) ) {
                i++;
                if( i>=argc ) {
                    cerr << "Error: missing value after -m" <<endl;
                    return 0;
                } else {
                    m = strtol (argv[i], NULL, 0);
                    if( m<=1 || m>=INT_MAX) {
                        cerr << "Error: wrong value for -m option (must be >1)" <<endl;
                        return 0;
                    }
                }
            } else if( !strcmp(argv[i],"-mg" ) ) {
                i++;
                if( i>=argc ) {
                    cerr << "Error: missing value after -mg" <<endl;
                    return 0;
                } else {
                    mg = strtol (argv[i], NULL, 0);
                    if( mg<0 ) { //|| ((long int)mg)>=UINT_MAX) {
                        cerr << "Error: wrong value for -mg option (must be >0)" <<endl;
                        return 0;
                    }
                }
            } else if( !strcmp(argv[i],"-h" ) ) {
                cout << "Usage: " << argv[0] << " [-f %d] [-n %d|-cneg] [-m %d] [-mg %d] [-miss %d] [-c] [-M] [-h] datafile" <<endl;
                cout << "\t -h: print this message" <<endl;
                cout << "\t -f: minimum support (number of transactions) / positive partner support (eNSP algorithm)" <<endl;
                cout << "\t -n: minimum support for eNSP negative patterns (eNSP algorithm)" <<endl;
                cout << "\t -m: maximum length of the patterns (not constrainted while 0, default 0)" <<endl;
                cout << "\t -mg: max gap constraint (default none, not compatible with -n option [ensp])" <<endl;
                cout << "\t -M: print only maximal length patterns" <<endl;
                cout << "\t -c: print only closed patterns" <<endl;
                cout << "\t -json: print patterns in JSON format" <<endl;
                cout << "\t -no: negative only (ensp option) " <<endl;
                cout << "\t -miss: maximal negative itemset size (if 0, all itemsets) (censp)" <<endl;
                cout << "\t -soft: activate soft embeddings (default, is strict embedding) (censp)" <<endl;
                cout << "\t -softinc: activate soft inclusion for itemsets (default, is strict) (censp)" <<endl;
                cout << "\t -I: load a sequence of items" <<endl;
                cout << "\t -cneg: use the complete version of negative patterns (does not require a negative support)" <<endl;
                cout << "\t datafile: transactions of sequences (IBM format)" <<endl;
                cout << "author: T. Guyet (AGROCAMPUS-OUEST)" <<endl;
            } else {
                cerr << "Warning: unknown option " << argv[i] << endl;
            }
        } else {
            if(fn==NULL) {
                fn=argv[i];
            } else {
                cerr << "Warning: unused datafile " << argv[i] << endl;
            }
        }
        i++;
    }
  
    if(fn==NULL) {
        cerr << "Error: missing datafile" << endl;
        return 0;
    }
    
    if( cneg && negatives ) {
        cerr << "Error: Impossible to choose which algorithm to use. cneg option does not require to define a negative support." << endl;
        return 0;
    }
   
    if( negatives ) {
        ProjDB data;
        if( itemseq ) {
            read(fn, data);
        } else {
            read_ibm(fn, data);
        }
        
        if( fout!=NULL) {
            ofstream fileout;
            fileout.open(fout);
            eNSP ensp((unsigned int)f, (unsigned int)m, (unsigned int)ns, fileout);
            
            if(negonly) ensp.include_positives(false);
            if( json ) {
                ensp.output_json();
                ensp.include_positives(true);
            }
            
            //run eNSP algorithm
            ensp.run(data);
            fileout.close();
        } else {
            //defaulty output in stdout
            eNSP ensp((unsigned int)f, (unsigned int)m, (unsigned int)ns);
            
            if(negonly) ensp.include_positives(false);
            if( json ) {
                ensp.output_json();
                ensp.include_positives(true);
            }
            
            //run eNSP algorithm
            ensp.run(data);
        }
    
        //clean transaction in the data structure
        for(vector<const Transaction*>::const_iterator tr=data.database.begin(); tr!=data.database.end();tr++) {
            delete(*tr);
        }
    } else if (cneg) {
        NegProjDB data;
        if( itemseq ) {
            read(fn, data);
        } else {
            read_ibm(fn, data);
        }
        
        if( is_inc_soft ) {
            itemset_noinc_mode=IS_NOINC_SOFT;
        }
        
        if( fout!=NULL) {
            ofstream fileout;
            fileout.open(fout);
            CeNSP censp((unsigned int)f, (unsigned int)m, fileout);
            
            if( json ) censp.output_json();
            if( soft ) censp.setStrictEmbedding(false);
            censp.setMaxItemsetSize(max_negis_size);
            censp.setMaxGap(mg);
            
            //run CeNSP algorithm
            censp.run(data);
            fileout.close();
        } else {
            //defaulty output in stdout
            CeNSP censp((unsigned int)f, (unsigned int)m);
            
            if( json ) censp.output_json();
            if( soft ) censp.setStrictEmbedding(false);
            censp.setMaxItemsetSize(max_negis_size);
            censp.setMaxGap(mg);
            
            //run CeNSP algorithm
            censp.run(data);
        }
    
        //clean transaction in the data structure
        for(vector<const Transaction*>::const_iterator tr=data.database.begin(); tr!=data.database.end();tr++) {
            delete(*tr);
        }
    } else {
        ProjDB data;
        if( itemseq ) {
            read(fn, data);
        } else {
            read_ibm(fn, data);
        }
        
        Prefixspan prefixspan((unsigned int)f, (unsigned int)m);// Do not save the pattern set (save memory) 
        prefixspan.show_closed(closed);
        prefixspan.show_maximal(maximal);
        prefixspan.setMaxGap(mg);
        if( json ) prefixspan.output_json();

        //run algorithm
        prefixspan.run(data);
    
        //clean transaction in the data structure
        for(vector<const Transaction*>::const_iterator tr=data.database.begin(); tr!=data.database.end();tr++) {
            delete(*tr);
        }
    } 

    return 0;
}
