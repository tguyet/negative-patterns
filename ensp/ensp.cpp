/**
 eNSP --- An Implementation of e-NSP algorithm for negative pattern mining

 Author: Thomas Guyet <thomas.guyet@agrocampus-ouest.fr>

 Date (last update): 01/2018

 License: GPL2 (Gnu General Public License Version 2)

 Reference:
  Longbing Cao, Xiangjun Dong, Zhigang Zheng,
  e-NSP: Efficient negative sequential pattern mining,
  Artif. Intell. 235: 156-182 (2016)
*/



#include "prefixspan.h"
#include "ensp.h"

#if COMPUTE_SID
//SID are required for this part !!

#include <map>
#include <stdexcept>
#include <algorithm>

using namespace std;

/**
\brief Compute maximal positive sequence from a Negative Pattern, ie the sequence of positive itemsets only
\param np a negative pattern

For instance the MPS of <(ab)-cd-(rt)g> is <(ab)dg>
*/
sequence MPS( const NegativePattern &np ) {
    sequence proj;
    sequence::const_iterator it= np.pp.begin();
    vector<bool>::const_iterator itn= np.negs.begin();
    while( it != np.pp.end() && itn != np.negs.end() ) {
        if( not *itn ) proj.push_back( *it );
        it++; itn++;
    }
    return proj;
}

/**
* \param negpat list of negative patterns that is filled by the recursive call of this function
* \param current current negative pattern in the recursion
* \param pos position at which the recursion is, it corresponds to the position for which previous itemsets have been setted up
* \param nb total amount of negation that has been added
*/
void generateNegatives(list< NegativePattern> &negpat, NegativePattern current, unsigned int len, unsigned int pos =0, unsigned int nb =0) {
    if( pos == len ) {
        if( nb>0 ) { //at least one negative to be included
            negpat.push_back(current);
        }
        return;
    }

    //Recursive generation of Negative Candidates
    generateNegatives(negpat, current, len, pos+1, nb);
    if( pos==0 || (pos>0 && !current.negs[pos-1]) ) {
        current.negs[pos]=1;
        current.nbn=nb+1;
        generateNegatives(negpat, current, len, pos+1, nb+1);
    }
}

unsigned int eNSP::run(ProjDB &data) {
    map<sequence, set<unsigned int> > _seqs; // sids of frequent positive sequences
    negpats.clear();
    
    unsigned int dbsize = data.database.size();
    
    // run PrefixSpan (it collects frequent patterns in pplist)
    prefixspan.run(data);
    
    // generate candidate negative patterns (collected in negpats)
    list< pair<sequence, unsigned int> >::const_iterator it= pplist.begin();
    while( it != pplist.end() ) {
        if( it->first.size()==0 ) { it++; continue; } //skip the empty pattern!
        NegativePattern current;
        current.pp = it->first;
        current.pp.sid.clear();
        current.ppsup = it->second;
        current.nbn = 0;
        current.negs = vector<bool>( it->first.size() );

        if( _include_positives ) {
            //add the pattern without negation as a potential negative pattern
            negpats.push_back( current );
        }

        generateNegatives( negpats, current, current.negs.size() );
        
        _seqs[ it->first ].insert( it->first.sid.begin(), it->first.sid.end()); //sids of frequent positive sequences
        it++;
    }

#if OUTPUT_SHOW
    if( print_json ) {
        _os << "{\"patterns\":[";
    }
#endif
    
    // compute the support of each negative patterns
    for(list<NegativePattern>::iterator npit=negpats.begin(); npit!=negpats.end(); ) {
        
#if TIMESTAMPS
        // Set of sids that must hold the pattern (according to the counting principle) 
        set<unsigned int> diff;
#endif
        
        //cout << *npit << ": " << _seqs[ npit->pp ].size() << "/" << dbsize << endl;
        try {
            
            if( npit->pp.size() == 0 ) { //empty sequence
                npit->sup = _seqs[ npit->pp ].size();
#if TIMESTAMPS
                // diff is practically not useful to compute
                //diff = _seqs[ npit->pp ];
#endif
            } else if( npit->pp.size() == 1 ) {
                if( npit->nbn==1 ) {
                    npit->sup = dbsize - _seqs[ npit->pp ].size();
                } else {
                    npit->sup = _seqs[ npit->pp ].size();
                }
#if TIMESTAMPS
                // theoretically make the set difference between allsids and _seqs[ npit->pp ]
                //      -> practically not useful to compute (because no temporal constraints between events can be extracted)
                /*
                set<unsigned int> dbsids;
                for(unsigned val=0;val<dbsize;val++) dbsids.insert(val);
                std::set_difference(dbsids.begin(), dbsids.end(), _seqs[ npit->pp ].begin(), _seqs[ npit->pp ].end(), std::inserter(diff, diff.begin()));
                */
#endif
                //cout << "\tsingleton: " << endl;
            } else if (npit->nbn==1) {
                sequence proj = MPS( *npit );
                    
                npit->sup = _seqs[ proj ].size() - _seqs[ npit->pp ].size();
                //cout << *npit << ": " << _seqs[ proj ].size()<< "-"<< _seqs[ npit->pp ].size() << "=" << npit->sup << "/" << dbsize << endl;
                
#if TIMESTAMPS
                // make difference between _seqs[ proj ] - _seqs[ npit->pp ]
                std::set_difference(_seqs[ proj ].begin(), _seqs[ proj ].end(), _seqs[ npit->pp ].begin(), _seqs[ npit->pp ].end(), std::inserter(diff, diff.begin()));
#endif
            } else {
                sequence proj = MPS( *npit );
                unsigned int supp = _seqs[ proj ].size();
                unsigned int ni=0;
                set<unsigned int> negsid; 
                while( ni< npit->negs.size() ) {
                    if( npit->negs[ni]==1 ) {
                        sequence mssproj; // positive partner without the "ni"-th element
                        for(unsigned int j=0; j< npit->pp.size(); j++) {
                            if( j==ni ) continue;
                            if( not npit->negs[j] ) mssproj.push_back( npit->pp[j] );
                        }
                        set<unsigned int>& lsid = _seqs[ mssproj ];
                        negsid.insert( lsid.begin(), lsid.end() );
                    }
                    ni++;
                }
                npit->sup = supp - negsid.size();
                
                
#if TIMESTAMPS
                // make difference between _seqs[ proj ] - negsid
                std::set_difference(_seqs[ proj ].begin(), _seqs[ proj ].end(), negsid.begin(), negsid.end(), std::inserter(diff, diff.begin()));
#endif
            }
        } catch(exception &e) {
            cerr << "unknown support for sequence!" << endl;
        }
        if( npit->sup < _negative_support ) {
            npit = negpats.erase(npit);
        } else {
#if OUTPUT_SHOW
            if( print_json ) {
                if( npit!=negpats.begin() ) _os<<",";
                npit->printJSON(_os);
            } else {
                _os << *npit << " [f=" << npit->sup <<"]"<< endl;
            }
#endif
#if TIMESTAMPS
            if( npit->pp.size()-npit->nbn>=2 ) { // Compute temporal constraints only for patterns that have more than 2 postive itemsets
                vector<vector<float> > temporal_projections;
                //HACK: iterate only over the set of sequences that actually holds the pattern : _seqs[proj] \ negsid !
                for(set<unsigned int>::const_iterator its=diff.begin();its!=diff.end();its++) {
                    const Transaction* T = data.database[ *its ];
                    
                    // find the first occurrence of the pattern in each sequence
                    NegativePattern::NegativePatternOccurrence cc=npit->find(T->second);
                    vector<float> projection;
                    if( cc.ppos.size()>0 ) {
                        //Construct a vector of positions corresponding to the MPS of the negative pattern (positive part)
                        for(vector<unsigned int>::iterator posit=cc.ppos.begin(); posit!=cc.ppos.end(); posit++) {
                            projection.push_back( T->second.timestamps[*posit] );
                        }
                        //cout << endl;
                        temporal_projections.push_back( projection );
                    }/* else if( npit->pp.size()!=npit->nbn ) {
                        cout << "Error: " << *npit << " not found in sequence " << *its << endl;
                    }*/
                }
                
                //Callback launch when a pattern is extracted with a temporal projection
                if( caller && caller->fp) {
                    //Here *npit is a NegativePattern, a kind of Generic sequential pattern
                    (*(caller->fp))(caller, *npit, temporal_projections);
                }
            }
#endif
            npit++;
        }
    }
    
#if OUTPUT_SHOW
    if( print_json ) {
        _os << "]}"<<endl;
    }
#endif
    return 0; //TODO: return the number of patterns
}

#endif
