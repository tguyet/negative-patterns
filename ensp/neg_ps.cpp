/**
 NegPS --- Implementation of a "depth-first" algorithm for mining negative sequential patterns

 Author: Thomas Guyet <thomas.guyet@agrocampus-ouest.fr>

 Date (last update): 01/2018
 
 License: GPL2 (Gnu General Public License Version 2)
*/

#include "prefixspan.h"
#include "neg_ps.h"
#include <set>
#include <sstream>
#include <algorithm>

//activate or desactive A2 hypothesis (remove surounding events from generated negative patterns)
#define A2 0


void CeNSP::output_pattern(const NegProjDB &projected) {
#if OUTPUT_SHOW>0
    if( print_json ) {
        current_pattern.printJSON( _os );
        if( current_pattern.pp.size()!=0) _os << ",";
        _os << endl;
    } else {
        _os << "pattern: " << current_pattern << ": " << projected.database.size() << endl;
    }
#endif
#if OUTPUT_SHOW>1
    //transaction list
    _os << endl << "( ";
    for (vector<const Transaction*>::const_iterator it = projected.database.begin(); it != projected.database.end(); it++) {
        _os << (*it)->first << " ";
    }
    _os << ") : " << projected.database.size() << endl;
#endif
    /*
    if(pfpatterns) {
        pfpatterns->push_back( pair<sequence, unsigned int >(pattern, projected.database.size()) );
    }
    */
}

unsigned int CeNSP::run(NegProjDB &projected) {
    //Evaluate the support of individual items
    for (unsigned int i = 0; i<projected.database.size(); i++) {
        const sequence &seq = projected.database[i]->second;
        //evaluating the possible sequential extensions
        set<unsigned int> items;
        for (unsigned int iter = 0; iter < seq.size(); iter++) {
            for (itemset::const_iterator item=seq[iter].begin(); item!=seq[iter].end(); item++) {
                items.insert( *item );
            }
        }
        for(set<unsigned int>::iterator itit = items.begin(); itit!=items.end();itit++){
            ++fitems[*itit];
        }
    }
    
    //prune unfrequent items
    list<unsigned int> infrequent;
    for(map<unsigned int,unsigned int>::iterator it=fitems.begin(); it!=fitems.end();) {
        if( it->second<min_sup ) {
            infrequent.push_back( it->first );
        }
        it++;
    }
    //remove infrequent items
    for(list<unsigned int>::iterator it= infrequent.begin();it!=infrequent.end();it++) {
        fitems.erase( *it );
    }
    
    #if OUTPUT_SHOW>0
    if( print_json ) {
        _os << "{\"patterns\":["<<endl;
    }
    unsigned int ret=project(projected);
    
    if( print_json ) {
        _os << "]}"<<endl;
    }
    return ret;
    #else
    return project(projected);
    #endif
}


void CeNSP::get_subsets(vector<itemset> &subsets, vector<unsigned int > &v, vector<unsigned int > currentpos, int max_size, int level, int pos) {
    if(max_size==0) return;

    if( level<max_size ) {
        //
        for(unsigned int i=pos+1; i<v.size(); i++) {
            currentpos.push_back( i );
            get_subsets(subsets, v, currentpos, max_size, level+1, i);
            itemset newsubset;
            for(vector<unsigned int >::iterator it=currentpos.begin(); it!=currentpos.end();it++) {
                newsubset.push_back(v[*it]);
            }
            subsets.push_back(newsubset);
            currentpos.pop_back();
        }
    }
}

/********************************************************************
 * Project database
 ********************************************************************/
unsigned int CeNSP::project(NegProjDB &projected) {
    if (projected.database.size() < min_sup) return 0;
    
    if (max_pat != 0 && current_pattern.size() == max_pat) {
        output_pattern(projected);
        return projected.database.size();
    }

    map<unsigned int, unsigned int> map_item;     // seqs
    map<unsigned int, unsigned int> map_item_sd;  // composition
    const vector<const Transaction*> &database = projected.database;
    for (unsigned int i = 0; i < database.size(); i++) {
        const sequence &seq = database[i]->second;

        //evaluating the possible compositional extensions
        unsigned int iter = projected.indexes[i].first;
        //Here iter is the position of the projection in the seq i
        if( current_pattern.size()!=0 ) {
            //Here, j is the position of the projection in the itemset at position iter of the seq i
            for (unsigned int j=projected.indexes[i].second; j<seq[iter].size(); j++) {
                map_item_sd[ seq[iter][j] ]++;
            }
            //HACK Warning TG: not complete !!!
            //      -> you may imagine that there exists further itemsets containing the last
            //         itemset of the current pattern and which hold alternative items !!
        }
        
        iter++; //next itemset
        //evaluating the possible sequential extensions
        for (; iter < seq.size(); iter++) {
            set<unsigned int> found_items;
            for (itemset::const_iterator item=seq[iter].begin(); item!=seq[iter].end(); item++) {
                found_items.insert( *item );
            }
            for( set<unsigned int>::iterator item=found_items.begin();item!=found_items.end();item++) {
                map_item[ *item ]++;
            }
        }
    }
    
    //HACK: filter out non-frequent items in map_item_sd and map_item
    //  -> the current enumeration in the map is not appropriate

    //creation of a new data on which project
    NegProjDB db_sd;
    db_sd.usedict=projected.usedict;
    vector<const Transaction*> &new_database_sd = db_sd.database;
    vector< triple > &new_indexes_sd = db_sd.indexes;
    
    bool singleton_extension=true;
    if( current_pattern.size()!=0 ) {// In case of non-empty pattern: composition and negative extensions
        singleton_extension=false;
        
        //Positive compositions
        for (map<unsigned int, unsigned int>::iterator it_1 = map_item_sd.begin(); it_1 != map_item_sd.end(); it_1++) {
        
            //complete the last pattern itemset with the new candidate item
            current_pattern.back().push_back(it_1->first);
                
                
            for (unsigned int i = 0; i < database.size(); i++) {
                const Transaction &transaction = *database[i];
                const sequence &seq = transaction.second;
                
                
                unsigned int iter = projected.indexes[i].first;
                itemset::const_iterator item=seq[iter].begin();
                bool extended=false;
                unsigned int j=projected.indexes[i].second;
                for (; j<seq[iter].size() && seq[iter][j] >= it_1->first; j++) {
                    if ( *item == it_1->first) {
                        new_database_sd.push_back( &transaction );
                        new_indexes_sd.push_back( triple(iter, j+1, projected.indexes[i].prev_is_id) );
                        extended=true;
                        break;
                    }
                }
                //the itemset can be later in the sequence !!
                if( !extended ) {
                    for(  ;iter<seq.size() &&                                                   // sequence length constraint
                           (max_gap==0 || (iter - projected.indexes[i].prev_is_id)<max_gap ) ;  // max_gap constraint
                           iter++) {
                        j=0;
                        itemset &is = current_pattern.back();
                        itemset::iterator itis=is.begin();
                        for (item=seq[iter].begin(); item!=seq[iter].end(); item++, j++) {
                            if( *item == *itis ) {
                                itis++;
                            } else if (*item > *itis ) {
                                //uses the item order in the itemsets
                                break;
                            }
                            if( itis==is.end() ) {
                                new_database_sd.push_back( &transaction );
                                new_indexes_sd.push_back( triple(iter, j+1, projected.indexes[i].prev_is_id) );
                                extended=true;
                                break;
                            }
                        }
                        if(extended) break;
                    }
                }
            }
            
            project(db_sd); // recursive call
            current_pattern.back().pop_back();
            db_sd.clear();
        } // end for it_1 (positive itemset extension)        
        
        
        //negative extensions only for 
        //  - pattern larger than 2 itemsets, and
        //  - last itemset of size 1 (to prevent redundancy) 
        if( current_pattern.size()>1 && current_pattern.pp.back().size()==1 ) {
        
            if( !current_pattern.negs[current_pattern.size()-2] ) { // the ante-itemsets is not a negative item -> insert a new negative item
                //Set of possible negative insertions (among frequent items)
                vector<itemset> candidate_negative;
                
                //TODO: make an option to ** use maximal frequent itemsets **
                if( itemset_noinc_mode==IS_NOINC_STRICT ) {
                    list<unsigned int> negis; //list of possible items (frequent, and that are not surounded by identical positive items)
                    
                    for(map<unsigned int,unsigned int>::iterator it= fitems.begin();it!= fitems.end();it++) negis.push_back(it->first);

                    // patterns of the shape "b-aa" or "a-ab?" are not generated -> removed from the itemset
                    //      * patterns "b-aa" may systematically be discarded to avoid generate equivalent patterns b-aac and ba-ac ! (but in this case, we will loose the pattern b-aa and its equivalent... because, the pattern ba-a will never be generated : thus the set of extracted patterns will not be equivalent.
                    //
                    //  -> more precisely, any item in the pattern before or after the insertion position in removed from the itemset
                    //  !!!! HACK !!!! does not seems to be 'complete' : (AC)-(AB)(BD) is potentially interesting patterns, but not generated ! (but (AC)-A(BD) or (AC)-B(BD) are not interesting)
                    #if A2
                    for(itemset::const_iterator itit=current_pattern.pp.back().begin(); itit!=current_pattern.pp.back().end(); itit++) {
                        negis.remove( *itit );
                    }
                    //current_pattern.pp[ current_pattern.size()-2] exists here !
                    for(itemset::const_iterator itit=current_pattern.pp[current_pattern.size()-2].begin(); itit!=current_pattern.pp[current_pattern.size()-2].end(); itit++) {
                        negis.remove( *itit );
                    }
                    #endif

                    // create one candidate per possible item: ie one negative itemset
                    for (list<unsigned int>::const_iterator negit = negis.begin(); negit != negis.end(); negit++) {
                        itemset is;
                        is.push_back( *negit );
                        candidate_negative.push_back( is );
                    }
                } else {
                    //itemset_noinc_mode==IS_NOINC_SOFT, support is not anti-monotone with extension of the negative itemsets)
                    // the algorithm does not extend recursively the negative itemsets!
                    // => generate all possible subpatterns of the itemset made of frequent items !!
                    vector<unsigned int> negis;
                    for(map<unsigned int,unsigned int>::iterator it= fitems.begin();it!= fitems.end();it++) negis.push_back(it->first);

                    unsigned int max_size = (max_negis_size==0?negis.size():(negis.size()<max_negis_size?negis.size():max_negis_size));
                    get_subsets(candidate_negative, negis, vector<unsigned int >(), max_size);
                }
                
                // Filter the database with transactions that does not hold negative itemsets
                for(vector<itemset>::const_iterator negis = candidate_negative.begin(); negis != candidate_negative.end(); negis++) { //composition with one of the frequent items (not necessarily in the projected database)
                    // Create an extention of the pattern with the negative 
                    NegProjDB db;
                    db.usedict=projected.usedict;
                    vector<const Transaction*> &new_database = db.database;
                    vector< triple > &new_indexes = db.indexes;
                    
                    itemset lastis = current_pattern.back();
                    current_pattern.pop_back(); //remove the last itemset
                    current_pattern.push_back( *negis, true ); //add a negative itemset
                    current_pattern.push_back( lastis ); //psuh back the last item (as a positive item)
                    
                    //map<unsigned int, triple> = old_indexes;
                    for (unsigned int i = 0; i < database.size(); i++) {
                        const Transaction &transaction = *database[i];
                        const sequence &seq = transaction.second;
                        
                        // find the itemset *negis in sequence seq !!
                        bool found=false;
                        if( current_pattern.strict_embedding ) {
                            for (unsigned int iter = projected.indexes[i].prev_is_id+1; iter!=projected.indexes[i].first; iter++) {
                                if( not noinc(*negis, seq[iter] ) ) { // HACK: here inc() is not equivalent to "not noinc()" !!!
                                    found=true;
                                    break;
                                }
                            }
                        } else {
                            //itemset cumul
                            itemset cumulated_items;
                            for (unsigned int iter = projected.indexes[i].prev_is_id+1; iter!=projected.indexes[i].first; iter++) {
                                merge(cumulated_items, seq[iter]);
                                /*cout << "\tadd: ";
                                for(itemset::const_iterator itis=seq[iter].begin();itis!=seq[iter].end();itis++) cout << *itis << ",";
                                cout << endl;
                                cout << "\tcumulative: ";
                                for(itemset::iterator itis=cumulated_items.begin();itis!=cumulated_items.end();itis++) cout << *itis << ",";
                                cout << endl;
                                */
                                if( not noinc(*negis, cumulated_items ) ) { // HACK: here inc() is not equivalent to "not noinc()" !!!
                                    found=true;
                                    //cout << "\t\tfound"<<endl;
                                    break;
                                }
                            }
                        }
                        
                        if( found ) {
                            // The considered occurrence for positive itemsets is not suitable! Another combination may be more convinent for the negative constraint
                            /*
                            NegativePattern negpat = current_pattern;
                            itemset lastis = negpat.back();
                            negpat.pop_back(); //remove the last itemset
                            negpat.push_back( *negis, true ); //add a negative itemset
                            negpat.push_back( lastis ); //push back the last item (as a positive item)
                            negpat.mg=max_gap;
                            */
                            NegativePattern::NegativePatternOccurrence occ=current_pattern.find(seq);
                            if( occ.ppos.size()>0 ) {
                                triple newocc(occ.ppos[occ.ppos.size()-1], 0, occ.ppos[occ.ppos.size()-2]);
                                
                                //add the transaction with the new found index
                                new_database.push_back( &transaction );
                                new_indexes.push_back( newocc );
                            }
                        } else {
                            // The sequence occurrence matches the negation: add the transaction to the database 
                            new_database.push_back( &transaction );
                            // new triple didn't changed by adding a negative item!
                            //      - starting position to look for next 
                            new_indexes.push_back( projected.indexes[i] );
                        }
                    }

                    project(db);//recursive call
                    lastis = current_pattern.back();
                    current_pattern.pop_back(); //remove the last (positive itemset)
                    current_pattern.pop_back(); //remove the negative itemset
                    current_pattern.push_back( lastis ); //push back the last positive item (as a positive item)
                    db.clear();
                } //end for negative items 
            } else if(itemset_noinc_mode==IS_NOINC_STRICT && current_pattern.pp[current_pattern.size()-2].size()<max_negis_size) {
                // Composition of last negative items (at position [last-2]), within the itemset size limit
                //  -> extension of negative patterns is possible only for strict-itemset inclusion because soft-itemset inclusion is not anti-monotone, but monotone (with IS_NOINC_SOFT)
                //  -> here, soft- and strict-embeddings are equivalent !!
                

                //Compute the list of possible items :
                //  <li> frequent ones, 
                //  <li> greater than the last current item and
                //  <li> that are not surounded by identical positive items
                list<unsigned int> negis; 
                for(map<unsigned int,unsigned int>::iterator it= fitems.begin();it!= fitems.end(); it++) {
                    if( it->first > current_pattern.pp[current_pattern.size()-2].back() ) negis.push_back(it->first); //add only items that are greater than the last item
                }
                #if A2
                for(itemset::const_iterator itit=current_pattern.pp.back().begin(); itit!=current_pattern.pp.back().end(); itit++) {
                    negis.remove( *itit );
                }
                for(itemset::const_iterator itit=current_pattern.pp[current_pattern.size()-3].begin(); itit!=current_pattern.pp[current_pattern.size()-3].end(); itit++) {
                    negis.remove( *itit );
                }
                #endif

                // compose the negative with another item *negit
                for (list<unsigned int>::const_iterator negit = negis.begin(); negit != negis.end(); negit++) {
                    
                    // Create an composition of the pattern with the negative 
                    NegProjDB db;
                    db.usedict=projected.usedict;
                    vector<const Transaction*> &new_database = db.database;
                    vector< triple > &new_indexes = db.indexes;
                    
                    // Extend pattern
                    current_pattern.pp[current_pattern.size()-2].push_back( *negit ); //<- extend the pattern here [used for soft-embeddings]
                    
                    // DB projection
                    for (unsigned int i = 0; i < database.size(); i++) {
                        const Transaction &transaction = *database[i];
                        const sequence &seq = transaction.second;
                        
                        // find the additional item *negis in sequence seq (between occurrences of positive itemsets) !!
                        bool found=false;
                        for (unsigned int iter = projected.indexes[i].prev_is_id+1; iter!=projected.indexes[i].first; iter++) {
                            found=false;
                            if( inc(*negit, seq[iter] ) ) { //HACK: here, the inc() operator just tests the **item** presence (not itemset), 
                                // same test for strict and soft embeddings with IS_NOINC_STRICT!
                                found=true;
                                break;
                            }
                        }
                        
                        if( found ) {
                            // The considered occurrence for positive itemsets is not suitable! Another combination may be more convinent for negative constraints
                            NegativePattern negpat = current_pattern;
                            //negpat.pp[negpat.size()-2].push_back( *negit ); //<- already in the current_pattern
                            negpat.mg=max_gap;
                            NegativePattern::NegativePatternOccurrence occ=negpat.find(seq);
                            if( occ.ppos.size()>0 ) {
                                triple newocc(occ.ppos[occ.ppos.size()-1], 0, occ.ppos[occ.ppos.size()-2]);
                                
                                //add the transaction with the new found index
                                new_database.push_back( &transaction );
                                new_indexes.push_back( newocc );
                            }
                        } else {
                            // The sequence occurrence matches the negation: add the transaction to the database 
                            new_database.push_back( &transaction );
                            // new triple didn't changed by adding a negative item!
                            //      - starting position to look for next 
                            new_indexes.push_back( projected.indexes[i] );
                        }
                    }

                    //current_pattern.pp[current_pattern.size()-2].push_back( *negit ); //has been done earlier !
                    project(db);//recursive call
                    current_pattern.pp[current_pattern.size()-2].pop_back();
                    db.clear();
                
                }
            }// end ante-itemset is not negative
        }// end current_pattern size >1
    }// end current_pattern size >0
    
    
    //sequential extension of the pattern
    NegProjDB db;
    db.usedict=projected.usedict;
    vector<const Transaction*> &new_database = db.database;
    vector< triple > &new_indexes = db.indexes;
    for (map<unsigned int, unsigned int>::iterator it_1 = map_item.begin(); it_1 != map_item.end(); it_1++) {
        //if(singleton_extension) cout << "try " << it_1->first <<": " << it_1->second << endl;
        if(it_1->second<min_sup) continue;
        
        for (unsigned int i = 0; i < database.size(); i++) {
            const Transaction &transaction = *database[i];
            const sequence &seq = transaction.second;
            bool extended=false;
            unsigned int previous_id = (projected.indexes[i].first==(unsigned int)(-1)?0:projected.indexes[i].first);
            for (unsigned int iter = projected.indexes[i].first+1;
                           iter < seq.size() &&                                                // sequence length constraint
                           (max_gap==0 || singleton_extension || (iter - projected.indexes[i].prev_is_id)<max_gap ) ; // max_gap constraint, only if it exists and that it is not the first item of a pattern (singleton_extension)!
                iter++) {
            
                extended=false;
                for (unsigned int j=0; j<seq[iter].size() /*itemset size*/ && seq[iter][j]<=it_1->first /*ordered itemsets*/; j++) {
                    if( seq[iter][j] == it_1->first ) {
                        new_database.push_back( &transaction );
                        new_indexes.push_back( triple(iter, j+1, previous_id) );
                        extended=true;
                        break;
                    }
                }
                if(extended) break;
            }
        }

        itemset is;
        is.push_back( it_1->first );
        current_pattern.push_back( is );
        project(db);//recursive call
        current_pattern.pop_back();
        if( current_pattern.size()>0 && current_pattern.negs[ current_pattern.size()-1 ] ) {
            //backtrack the negative itemset at the same time !!
            current_pattern.pop_back();
        }
        db.clear();
    }

    output_pattern(projected);
    
    return projected.database.size();
}


////////////////  FILE READ FUNCTIONS ////////////////////

void read_ibm(const string &_filename, NegProjDB &pairdata) {
    string       line;
    int          item;
    unsigned int id = 0, nb =0, pid=0;
    unsigned int timestamp = 0;

    ifstream is( _filename.c_str() );
    Transaction *transaction = NULL;
    sequence *seq=NULL;
    
    
    pairdata.usedict=false;
    
    while( getline(is, line) ) {
        stringstream ss(line.c_str());
    
        //sequence id
        if( not (ss >> id) ) {
            continue;
        }
        
        //itemset timestamp
        if( not (ss >> timestamp) ) {
            continue;
        }
        
        //itemset size
        if( not (ss >> nb) ) {
            continue;
        }
    
        if( !transaction ) {
            //create new transaction
            transaction = new Transaction();
            pid = 0;
            seq = &(transaction->second);
            if(id!=0) {
                cerr << "warning: sequence ids must start with 0" << endl;
            }
        } else if (id != pid ) {
            if( transaction->second.size()>0 ) {
                transaction->first = pairdata.database.size();
                pairdata.database.push_back(transaction);
                pairdata.indexes.push_back( triple(-1,0,0) );
            } else {
                delete(transaction);
            }
            
            if( (id-1)!=pid ) {
                cerr << "warning: sequence ids must be contigues" << endl;
            }
            
            //create new transaction
            transaction = new Transaction();
            pid = id;
            seq = &(transaction->second);
        }
        
        //read the itemset
        itemset is;
        while (ss >> item) {
            is.push_back( item );
        }
        
        if( nb!=is.size() ) {
            cerr << "warning: itemset size invalid" << endl;
        }
        
        sort( is.begin(), is.end() );
        unique( is.begin(), is.end() ); //ensure itemset without repetitions
        seq->push_back( is );
#if TIMESTAMPS
        seq->timestamps.push_back( timestamp );
#endif
    }
    //add the last sequence
    if( transaction && transaction->second.size()>0 ) {
        pairdata.database.push_back(transaction);
        pairdata.indexes.push_back( triple(-1,0,0) );
    }
}

void read(const string &_filename, NegProjDB &pairdata) {
    string       line;
    int          item;
    unsigned int id = 0;
#if TIMESTAMPS
    unsigned int timestamp = 0;
#endif

    ifstream is(_filename.c_str());
    Transaction *transaction = new Transaction();
    while (getline (is, line)) {
        transaction->second.clear();
        sequence &seq= transaction->second;
        stringstream ss(line.c_str());

        while (ss >> item) {
            itemset is;
            is.push_back( item );
            seq.push_back( is );
#if TIMESTAMPS
            seq.timestamps.push_back( ++timestamp );
#endif
        }
            
        transaction->first = id++;
        pairdata.usedict=false;
        pairdata.database.push_back(transaction);
        pairdata.indexes.push_back( triple(-1,0,0) );
        transaction = new Transaction();
#if TIMESTAMPS
        timestamp = 0;
#endif
    }
}

