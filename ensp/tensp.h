/**
* Implementation of a TGSP-like algorithm for mining temporal patterns.
* The TGSP algorithm extract Time-Gap Sequential Patterns, ie a pattern with the shape A [34,45] B [56,67] C, where intervals specifies the 
* delays between successive events (A, B and C).
*
* date (last update): 01/2018
* author: T. Guyet <thomas.guyet@agrocampus-ouest.fr>,
* institution: AGROCAMPUS-OUEST/IRISA
* 
* License: GPL2 (Gnu General Public License Version 2)
*
* References:
*   - Yen, S.-J. and Y.-S. Lee (2013). Mining non-redundant time-gap sequential patterns. Applied intelligence 39(4), 727–738.
*   - Goil, S., Nagesh, H., & Choudhary, A. (1999). MAFIA: Efficient and scalable subspace clustering for very large data sets. In Proceedings of the 5th ACM SIGKDD International Conference on Knowledge Discovery and Data Mining, pp. 443-452.
*/

#ifndef TGSP_H
#define TGSP_H

#include "ensp.h"
#include "cppmafia/utils.h"
#include "cppmafia/options.h"
#include "cppmafia/mafia-solver.h"
#include <list>
#include <vector>
#include <climits>
#include <cstdlib>
#include <sstream>
#include <iostream>

using namespace std;

class TemporalNegativePattern {
public:
    NegativePattern symbolic_signature;
    vector< pair<float,float> > temporal_projection;
};

ostream &operator<<(ostream &os, const TemporalNegativePattern &);


/**
* \class TeNSP
* \brief TeNSP class implements the mining of frequent negative temporal patterns using the eNSP strategy
* 
* This class is derived from the Prefixspan class which implement the prefixspan algorithm. It connects the prefixspan 
* frequent sequential pattern extraction to the MAFIA algorithm.
* The original algorithm of TGSP was based on the CLIQUE clustering algorithm, the MAFIA clustering is also a density based
* clustering algorithm, but with adaptative boundaries
*
* 
* \warning with small dataset, I noticed that MAFIA does not correctly evaluate the support of the pattern.
*/
class TeNSP {
private:
    eNSP ensp;
    
#if TIMESTAMPS
    /**
    \brief functional-object which enables to adaptively define the parameters to transmit to the callable function pointer within PrefixSpan/eNSP
    */
    class TeNSPCallable : public Callable {
    public:
        TeNSPCallable( void (*f)(const Callable *, const GenericPattern &, const vector<vector<float> >&) );
        unsigned int threshold;
        bool print_patterns;
        list<TemporalNegativePattern> *tpatterns=NULL;
    };

    /**
    \brief Callable function for specifying temporal patterns for each sequential pattern
    \warning The caller can be specilized to transmis values from the main program to this function. Be careful to be consistent with reverse-casting and definition of the caller transmitted to the PrefixSpan object
    */
    static void process_temporal_pattern(const Callable* caller, const GenericPattern &symbolic_signature, const vector<vector<float> >&temporal_projection);
    
    TeNSPCallable caller;
#endif

public:
    TeNSP(unsigned int _min_sup, unsigned int _max_pat, unsigned int _ns, list< TemporalNegativePattern > *patlist =NULL);
    unsigned int run(ProjDB &projected);
    
    void setAlpha(float alpha);
    void setBeta(float beta);
    void setMinWindows(unsigned int mW);
    void setMaxWindows(unsigned int MW);
    void setNbBins(unsigned int n);
};

#endif



