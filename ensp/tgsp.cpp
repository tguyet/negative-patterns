/**
* Implementation of a TGSP-like algorithm for mining temporal patterns
*
* date (last update): 01/2018
* author: T. Guyet <thomas.guyet@agrocampus-ouest.fr>,
* institution: AGROCAMPUS-OUEST/IRISA
* 
* License: GPL2 (Gnu General Public License Version 2)
*/

#include "tgsp.h"

ostream &operator<<(ostream &os, const TemporalPattern &pattern) {
    os << "<";
    for(unsigned int j=0; j< pattern.temporal_projection.size(); j++) {
        os << "(" << pattern.symbolic_signature[j] << ") [" << pattern.temporal_projection[j].first << "," << pattern.temporal_projection[j].second << "] ";
    }
    os << "("<<pattern.symbolic_signature[pattern.temporal_projection.size()] << ")>";
    return os;
}

#if TIMESTAMPS
TGSP::TGSPCallable::TGSPCallable( void (*f)(const Callable *, const GenericPattern &, const vector<vector<float> >&) ) {
    fp=f;
}
#endif

TGSP::TGSP(unsigned int _min_sup, unsigned int _max_pat, list< TemporalPattern > *patlist) :
    prefixspan(_min_sup, _max_pat)
#if TIMESTAMPS
    , caller(&TGSP::process_temporal_pattern)
#endif
    {
#if TIMESTAMPS
    //MAFIA options
    Options::init();
    
    //Branching the callback
    prefixspan.setCallback( &caller );
    caller.threshold = (unsigned int)_min_sup;
    caller.print_patterns = true;
    caller.tpatterns=patlist;
#endif
}

unsigned int TGSP::run(ProjDB &data) {
    //run algorithm
    return prefixspan.run(data);
}

#if TIMESTAMPS

/**
* @param temporal_projection list of current prefixspan temporal projections: the valid temporal projections are only those corresponding to the sid saved in the pattern
*/
void TGSP::process_temporal_pattern(const Callable* caller, const GenericPattern &pattern, const vector<vector<float> >&temporal_projection) {
    // cast the parameters of the generic caller corresponding to the specific values of TGSP
    TGSP::TGSPCallable *tgspcaller = (TGSP::TGSPCallable*)(caller); //Warning reverse-casting
    sequence & symbolic_signature = (sequence &)pattern; //warning reverse-casting
    
    //NB: in the following variable name "n", "d" and "ps" must not be changed cause of PS macro (in the MAFIA algorithm)
    unsigned int n=symbolic_signature.sid.size(); 
    if( n==0 ) return;
    int d=symbolic_signature.size()-1;
    if( d<=0 ) return; //case of empty pattern
    
    //Time-gap table construction
    float *ps = (float*)bulk_alloc( sizeof(float)*n*d );
    
    unsigned int i = 0;
    set<unsigned int>::const_iterator trans_it = symbolic_signature.sid.begin();
    while(trans_it!= symbolic_signature.sid.end() ) {
        for(unsigned int idim=0; idim<(unsigned int)d; idim++) {
            PS(i, idim) = temporal_projection[*trans_it][idim+1]-temporal_projection[*trans_it][idim];
            //cout << PS(i, idim) <<",";
        }
        trans_it++;
        i++;
        //cout << endl;
    }
    
	const Options &opts = Options::options();
	vector< cluster_rule<float> > du_clusters= mafia_solve(ps, n, d, opts);
    
    vector< cluster_rule<float> >::iterator itclust = du_clusters.begin();
    while( itclust != du_clusters.end() ) {
        if( itclust->nbpoints < tgspcaller->threshold ) {
            itclust = du_clusters.erase(itclust); 
        } else {
            //The cluster yields a new temporal pattern
            if( tgspcaller->tpatterns ) {
                TemporalPattern tpattern;
                tpattern.symbolic_signature=symbolic_signature;
                tpattern.temporal_projection = itclust->intervals;
                tgspcaller->tpatterns->push_back( tpattern );
                if( tgspcaller->print_patterns ) {
                    cout << "TP: " << tpattern << ": "<< itclust->nbpoints << endl;
                }
            } else if ( tgspcaller->print_patterns ) {
                TemporalPattern tpattern;
                tpattern.symbolic_signature=symbolic_signature;
                tpattern.temporal_projection = itclust->intervals;
                cout << "TP: " << tpattern << ": "<< itclust->nbpoints << endl;
            }
            itclust++;
        }
    }

    bulk_free(ps);
}
#endif


