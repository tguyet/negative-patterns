from prefixspan import *

print("Construct a dataset (a list of lists) from the 'data' file")
fin = open("data", "r")
data=[]
for l in fin:
    trs = l.rstrip('\n').strip(' ').split(' ')
    tr = [int(e) for e in trs ]
    data.append(tr)
fin.close()
# data can be integers or string !
#   -> in case of strings, the values will be indexed
#   -> avoid mixing str and int !

th=50
print("run prefix span with threshold=",th)
ret = prefixspan(data, th)
print("patterns list:", ret)
print("#patterns:", len(ret))
print("10th pattern:", ret[10])

patterns = [p[0] for p in ret]
sup = [p[1] for p in ret]

print("=========================================")
print("test with sequences of itemsets")
data=[  [6],
        [1, 2, 3],
        [ [1,3,5], [2,4]],
        [ [1,4], 3, [2,4] ],
        [ [2,3,4], [1], [1,3], [2,4], [4]],
        [ [1,3,5] ]]
print("data:", data)

pat = prefixspan(data, 2)
print("patterns (threshold 2):", pat)

pat = prefixspan(data, 3)
print("patterns (threshold 3)", pat)

pat = prefixspan(data, 3, CLOSED)
print("CLOSED patterns (threshold 3)", pat)

pat = prefixspan(data, 3, MAXIMAL)
print("MAXIMAL patterns (threshold 3)", pat)


pat = ensp(data, 3, 1)
print("negative patterns (threshold 3, 1):", pat)
