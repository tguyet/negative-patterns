#!/bin/bash

##### Example of negative patterns that is not extracted due to non-frequent positive pattern
## pattern (383),(114),(383) represents a switch G-R-G of valproic acids
##  -> it occurs 206 times in the dataset
##  -> its negative version (383),-(114),(383) representing that non-switches occurred 1417 times (with mgap=4)
##      => the later is extracted by NegPSpan, but not by eNSP

pattern_shape="(383),[-]*(114),(383)"
echo "-------------------------------------------------"
echo "switch pattern G-R-G extracted for valproic acids:"
fmin=300
mlength=3
mgap_negpspan=0
ratio_ensp=8
# Extraction des motifs avec NegPSpan
./prefixspan -cneg -f $fmin -m $mlength -mg $mgap_negpspan -miss 2 db-pos.db > output_negpspan.pat

# Extraction des motifs avec eNSP
fpos=$(echo "scale=4;$fmin*$ratio_ensp/10" | bc)
./prefixspan -n $fmin -f $fpos -m $mlength -miss 1 db-pos.db > output_ensp.pat
echo "by eNSP"
grep $pattern_shape output_ensp.pat
echo "by NegPSpan"
grep $pattern_shape output_negpspan.pat
echo "-------------------------------------------------"

