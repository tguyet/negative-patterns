/**
 PrefiSpan --- An Implementation of Prefix-projected Sequential Pattern mining
           --- A Python Binding

 Author: Thomas Guyet <thomas.guyet@agrocampus-ouest.fr>
 
 date (last update): 01/2018

 License: GPL2 (Gnu General Public License Version 2)
*/
#if defined(_WIN32) || defined(WIN32) || defined(__APPLE__)
#include "Python.h"
#else
#include "python3.5m/Python.h"
#endif

#include "prefixspan.h"
#include "ensp.h"
#include <climits>
#include <list>
#include <vector>
#include <algorithm>

extern "C" {

unsigned int getValue(PyObject *item, ProjDB *pairdata) {
    if( PyLong_Check(item) )  { //integer -> add a new itemset with one integer value
        long l=PyLong_AsLong(item);
        if( l<0) return -1;         // skip negative integers
        if( l>INT_MAX ) return -1;  // skip too huge values
        
        return (unsigned int)l;
    } else if (PyUnicode_Check(item) ) { //characters
        pairdata->usedict=true;
        
        string s( PyUnicode_AsUTF8(item) );
        std::map<string,unsigned int>::iterator it;
        it=pairdata->dictionnary.find( s );
        if( it!= pairdata->dictionnary.end() ) {
            return (*it).second;
        } else {
            unsigned int val = pairdata->dictionnary.size();
            pairdata->dictionnary.insert( std::pair<string,int>(s,val) );
            pairdata->revdictionnary.insert( std::pair<int, string>(val,s) );
            return val;
        }
    }
    return -1;
}


void addTransaction(PyObject *list, ProjDB *pairdata) {
	if (!PyList_Check(list)) {
		return;
	}

    int n = PyList_Size(list);
    PyObject *item;
    int i,j;
        
    Transaction *transaction = new Transaction();
    sequence &seq = transaction->second;
    
    //read the list
    for (i = 0; i < n; i++) {
        item = PyList_GetItem(list, i); // Can't fail
        if (PyList_Check(item)) { //list of items
			int nit = PyList_Size(item);
            itemset is;
            for(j=0; j< nit; j++) {
                unsigned int val = getValue( PyList_GetItem(item, j), pairdata);
                if (val>=0) is.push_back( val );
            }
            sort( is.begin(), is.end() );
            unique( is.begin(), is.end() ); //ensure itemset without repetitions
            seq.push_back(is);
        } else {
            //single value : add a new itemset with one integer value
            unsigned int val=getValue(item, pairdata);
            itemset is;
            is.push_back( val );
            seq.push_back(is);
        }
    }
    
    transaction->first = pairdata->database.size();
    pairdata->database.push_back( transaction );
    pairdata->indexes.push_back( projection_pointer(-1,0,transaction->second.size()) );
}

/**
* Create a list of long describing the pattern pat
*/
PyObject* createitemset(itemset is, ProjDB &data) {
    PyObject* PP = PyList_New( is.size() );
    unsigned int i=0;
    itemset::iterator it=is.begin();
    while( it!=is.end() ) {
        if( !data.usedict ) {
            PyList_SET_ITEM(PP, i, Py_BuildValue("l",*it) );
        } else {
            //utilisation du dictionnaire
            std::map<unsigned int,string>::iterator ct;
            ct=data.revdictionnary.find( *it );
            if( ct!= data.revdictionnary.end() ) {
                PyList_SET_ITEM(PP, i, Py_BuildValue("s", (*ct).second.c_str()) );
            } else {
                PyList_SET_ITEM(PP, i, Py_BuildValue("l",*it) );
            }
        }
        it++;
        i++;
    }
    return PP;
}

PyObject* createpattern(sequence pat, unsigned int support, ProjDB &data) {
    //creation du motif
    PyObject* PP = PyList_New( pat.size() );
    unsigned int i=0;
    for( i=0; i< pat.size(); i++ ) {
        PyList_SET_ITEM(PP, i, createitemset( pat[i], data ) );
    }
    PyObject* PySupport=Py_BuildValue("l",support);
    PyObject* Pat=PyTuple_New(2);
    PyTuple_SET_ITEM(Pat, 0, PP );
    PyTuple_SET_ITEM(Pat, 1, PySupport );
    return Pat;
}

/**
* Create a list of pair with (the patterns and its support)
*/
PyObject* createpatternslist(list< pair<sequence,unsigned int> > patterns, ProjDB &data) {
    PyObject* pats = PyList_New( patterns.size() ); //-1 because the first pattern is empty !
    int i=0;
    list< pair<sequence,unsigned int> >::iterator it= patterns.begin();
    while( it != patterns.end() ) {
        PyList_SET_ITEM(pats, i, createpattern( it->first, it->second, data ) );
        i++;
        it++;
    }
    return pats;
}

PyObject* createnegativepattern(NegativePattern pat, ProjDB &data) {
    //create the positive pattern and negation
    PyObject* PP = PyList_New( pat.pp.size() );
    PyObject* negs = PyList_New( pat.pp.size() );
    unsigned int i=0;
    for( i=0; i< pat.pp.size(); i++ ) {
        PyList_SET_ITEM(PP, i, createitemset( pat.pp[i], data ) );
        if( pat.negs[i] ) {
            PyList_SET_ITEM(negs, i, Py_BuildValue("l",1) );
        } else {
            PyList_SET_ITEM(negs, i, Py_BuildValue("l",0) );
        }
    }
    
    PyObject* PySupport=Py_BuildValue("l", pat.sup);
    PyObject* Pat=PyTuple_New(3);
    PyTuple_SET_ITEM(Pat, 0, PP );
    PyTuple_SET_ITEM(Pat, 1, negs );
    PyTuple_SET_ITEM(Pat, 2, PySupport );
    return Pat;
}


/**
* Create a list of triples with (the positive pattern, the negation and its support)
*/
PyObject* createnegativepatternslist(const list< NegativePattern>& patterns, ProjDB &data) {
    PyObject* pats = PyList_New( patterns.size() ); //-1 because the first pattern is empty !
    int i=0;
    list< NegativePattern >::const_iterator it= patterns.begin();
    while( it != patterns.end() ) {
        PyList_SET_ITEM(pats, i, createnegativepattern( *it, data ) );
        i++;
        it++;
    }
    
    return pats;
}


/*  wrapped prefix-span function */
static PyObject* prefixspan(PyObject* self, PyObject* args) {
	int n = 0, i;
	PyObject *list;
	int th = 0; //threshold
	short int options = 0;
	std::list< pair<sequence, unsigned int> > patlist; //the pattern list

	/*  parse the input, from python type to c type */
	if (!PyArg_ParseTuple(args, "O!i|b", &PyList_Type, &list, &th, &options)) {
		return NULL;
	}
	
	
	if (!PyList_Check(list)) {
		return NULL;
	} else {
		n = PyList_Size(list);
	}
    if(th <=0 )
        return NULL;
    
    ProjDB data; //dataset representation
    data.usedict = false;
    //each element of the list represents a transaction
    for (i = 0; i < n; i++) {
        addTransaction( PyList_GetItem(list, i), &data );
    }
    
    //create a prefixspan instance
    Prefixspan ps(/*threshold*/ th, /*max pat size*/0, &patlist);
	
    ps.show_closed(options&1);
    ps.show_maximal(options&2);
    //run the algorithm
    ps.run(data);
	PyObject *result = createpatternslist( patlist, data );

    return result;
}



/*  wrapped eNSP function */
static PyObject* ensp(PyObject* self, PyObject* args) {
	int n = 0, i;
	PyObject *list;
	int th = 0; //threshold
    int negs=0;      //negatives threshold
	std::list< pair<sequence, unsigned int> > patlist; //the pattern list

	/*  parse the input, from python type to c type */
	if (!PyArg_ParseTuple(args, "O!ii", &PyList_Type, &list, &th, &negs)) {
		return NULL;
	}
	
	
	if (!PyList_Check(list)) {
		return NULL;
	} else {
		n = PyList_Size(list);
	}
    if(th <=0 )
        return NULL;
    
    ProjDB data; //dataset representation
    data.usedict = false;
    //each element of the list represents a transaction
    for (i = 0; i < n; i++) {
        addTransaction( PyList_GetItem(list, i), &data );
    }
    
    //create a prefixspan instance
    eNSP ps(/*threshold*/ th, /*max pat size*/0, /*negative support*/negs);
	
    //run the algorithm
    ps.run(data);
	PyObject *result = createnegativepatternslist( ps.patterns(), data );

    return result;
}


/*  wrapped CeNSP function */
// \TODO
/*
static PyObject* censp(PyObject* self, PyObject* args) {
	int n = 0, i;
	PyObject *list;
	int th = 0; //threshold
    int negs=0;      //negatives threshold
	std::list< pair<sequence, unsigned int> > patlist; //the pattern list

	//  parse the input, from python type to c type
	if (!PyArg_ParseTuple(args, "O!i", &PyList_Type, &list, &th)) {
		return NULL;
	}
	
	if (!PyList_Check(list)) {
		return NULL;
	} else {
		n = PyList_Size(list);
	}
    if(th <=0 )
        return NULL;
    
    NegProjDB data; //dataset representation
    data.usedict = false;
    //each element of the list represents a transaction
    for (i = 0; i < n; i++) {
        addTransaction( PyList_GetItem(list, i), &data );
    }
    
    //create a prefixspan instance
    CeNSP ps(th, 0);
	
    //run the algorithm
    ps.run(data);
	PyObject *result = createnegativepatternslist( ps.patterns(), data );

    return result;
}
//*/

/*
* Implements an example function.
*/
PyDoc_STRVAR(prefixspan_example_doc, "example(obj, number) Example function");
PyDoc_STRVAR(ensp_example_doc, "example(obj, number) Example function");

/*
* List of functions to add to test_module in exec_test_module().
*/
static PyMethodDef prefixspan_functions[] = {
	{ "prefixspan", (PyCFunction)prefixspan, METH_VARARGS, prefixspan_example_doc },
	{ "ensp", (PyCFunction)ensp, METH_VARARGS, ensp_example_doc },
	{ NULL, NULL, 0, NULL } /* marks end of array */
};

/*
* Initialize test_module. May be called multiple times, so avoid
* using static state.
*/
int exec_prefixspan(PyObject *module) {
	PyModule_AddFunctions(module, prefixspan_functions);

	PyModule_AddStringConstant(module, "__author__", "T. Guyet");
	PyModule_AddStringConstant(module, "__version__", "1.0.0");
	PyModule_AddIntConstant(module, "year", 2017);

	PyModule_AddIntConstant(module, "ALL", 0);
	PyModule_AddIntConstant(module, "CLOSED", 1);
	PyModule_AddIntConstant(module, "MAXIMAL", 2);

	return 0; /* success */
}

/*
* Documentation for prefixspan module.
*/
PyDoc_STRVAR(prefixspan_doc, "The prefixspan module");


static PyModuleDef_Slot prefixspan_slots[] = {
	{ Py_mod_exec, (void*)exec_prefixspan },
	{ 0, NULL }
};

static PyModuleDef moduledef = {
	PyModuleDef_HEAD_INIT,
	"prefixspan",
	prefixspan_doc,
	0,              /* m_size */
	NULL,           /* m_methods */
	prefixspan_slots,
	NULL,           /* m_traverse */
	NULL,           /* m_clear */
	NULL,           /* m_free */
};

/* module initialization */
PyMODINIT_FUNC
PyInit_prefixspan(void)
{
    return PyModuleDef_Init(&moduledef);
}

} //extern "C"


